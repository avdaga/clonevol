#ifndef WORKERTHREAD_H
#define WORKERTHREAD_H

#include <QThread>

/*! ***********************************************************************************************
 * \author  Avdo Hanjalic
 * \date    15.07.2013
 * \class   WorkerThread
 *
 * \brief   This class can be used to move objects to a separate thread; The exec() method is
 *          called when the thread is started, so that it starts processing events of the child
 *          objects.
 **************************************************************************************************/
class WorkerThread : public QThread
{
    Q_OBJECT

public:
    /* ****************************************************************************************
     * Constructor & Destructor
     ******************************************************************************************/
    WorkerThread(QObject *parent = 0) : QThread(parent) {}

protected:
    /* ****************************************************************************************
     * Protected Functions
     ******************************************************************************************/
    virtual void run() {
        QThread::exec();
    }
};

#endif // WORKERTHREAD_H
