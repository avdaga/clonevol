#include "logpipe.h"

#include <QTextCursor>

LogPipe* LogPipe::m_instance = 0;

/* ************************************************************************************************
 * Getters & Setters
 **************************************************************************************************/
LogPipe* LogPipe::Instance()
{
    if (m_instance == 0) {
        m_instance = new LogPipe();
    }

    return m_instance;
}

/* ************************************************************************************************
 * Constructor & Destructor
 **************************************************************************************************/
LogPipe::LogPipe() :
    QObject()
{
    qRegisterMetaType<QTextCursor>("QTextCursor");
    qInstallMessageHandler(myMessageOutput);
}

/* ************************************************************************************************
 * Private Functions
 **************************************************************************************************/
void LogPipe::writeLog(const QString &msg)
{
    emit sigWriteLog(msg);
}

void LogPipe::myMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    Q_UNUSED(context);
    Q_UNUSED(type);

    LogPipe::Instance()->writeLog(msg);
}
