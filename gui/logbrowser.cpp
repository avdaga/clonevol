#include "logbrowser.h"
#include "ui_logbrowser.h"

#include "util/logpipe.h"

LogBrowser* LogBrowser::m_instance = 0;

/* ************************************************************************************************
 * Getters & Setters
 **************************************************************************************************/
LogBrowser* LogBrowser::Instance()
{
    if (m_instance == 0) {
        m_instance = new LogBrowser();
    }

    return m_instance;
}

/* ************************************************************************************************
 * Constructor & Destructor
 **************************************************************************************************/
LogBrowser::LogBrowser(QWidget *parent) :
    QMainWindow     (parent)
  , ui              (new Ui::LogBrowser)
  , m_bufferLength  (0)
{
    ui->setupUi(this);
    connect(LogPipe::Instance(), SIGNAL(sigWriteLog(QString)),
            this,                SLOT(writeLog(QString)));
}

LogBrowser::~LogBrowser()
{
    delete ui;
}

/* ************************************************************************************************
 * Private Slots
 **************************************************************************************************/
void LogBrowser::writeLog(const QString &msg)
{
    m_logMutex.lock();

    if (m_bufferLength > 1024000) {
        ui->textBrowser->clear();
        m_bufferLength = 0;
    }

    ui->textBrowser->append(msg);
    m_bufferLength += msg.length();

    m_logMutex.unlock();
}

void LogBrowser::on_clearButton_clicked()
{
    ui->textBrowser->clear();
}
