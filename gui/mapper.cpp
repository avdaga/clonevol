#include "mapper.h"

#include <QDebug>
#include <QElapsedTimer>

#include "datastore/nodetype.h"

/* ************************************************************************************************
 * Constructor & Destructor
 **************************************************************************************************/
Mapper::Mapper(QString fileName) :
    QObject                 ()
  , m_fileName              (fileName)
  , m_processCalled         (0)
  , m_nFilesTotal           (0)
  , m_nScopesTotal          (0)
  , m_colorMap              (Color_Structure)
  , m_currentRoot           ("/")
  , m_edgeMetric            (Metric_None)
  , m_maxTreeDepth          (0)
  , m_minRevision           (1)
  , m_maxRevision           (1)
  , m_nodeFilter            (7)
  , m_showInterClones       (true)
  , m_showIntraClones       (true)
  , m_showOldIntraClones    (true)
  , m_edgeColorMap          (new QMultiHash<QString,QColor>())
  , m_edgeColorMapBuffer    (new QMultiHash<QString,QColor>())
  , m_ringColorMap          (new QHash<QString,QColor>())
  , m_ringColorMapBuffer    (new QHash<QString,QColor>())
{
    reloadDatabase();
}

Mapper::~Mapper() {
    m_dataBase.close();
    QSqlDatabase::removeDatabase("QSQLITE");
    delete m_ringColorMap;
    delete m_ringColorMapBuffer;
}

/* ************************************************************************************************
 * Public Functions
 **************************************************************************************************/
void Mapper::reloadDatabase()
{
    // Open the database
    m_dataBase = QSqlDatabase::addDatabase("QSQLITE", m_fileName);
    m_dataBase.setDatabaseName(m_fileName);
    m_dataBase.open();

    // Improve database speed
    m_dataBase.exec("PRAGMA synchronous = OFF");
    m_dataBase.exec("PRAGMA journal_mode = MEMORY");
    m_dataBase.exec("PRAGMA cache_size = -512000");

    QSqlQuery query(m_dataBase);
    query.prepare("SELECT dataStoreName, URL FROM DataStore");
    if (query.exec()) {
        query.first();

        m_projectName = query.value(0).toString();
        m_projectUrl  = query.value(1).toString();

        qDebug () << m_projectName << m_projectUrl;

        // Get total file count
        query.prepare("SELECT Count() FROM File");
        if (query.exec() && query.first()) m_nFilesTotal = query.value(0).toInt();

        // Get total scope count
        query.prepare("SELECT Count() FROM Scope");
        if (query.exec() && query.first()) m_nScopesTotal = query.value(0).toInt();

        // Read revisions
        query.prepare("SELECT revisionNr, isAcquired FROM Revision "
                      "WHERE revisionNr > 0 "
                      "ORDER BY revisionNr");
        if (query.exec()) {
            m_revisions.clear();
            m_revisionsAcq.clear();
            while(query.next()) {
                m_revisions     << query.value(0).toInt();
                m_revisionsAcq  << query.value(1).toBool();
            }
        }

        // Read the unions
        setRootNode(m_currentRoot, true);
    }
}

/* ************************************************************************************************
 * Private Functions
 **************************************************************************************************/
void Mapper::readClones()
{
    QSqlQuery query(m_dataBase);
    //! \todo: Filter out clones that are out of m_currentRoot
    query.prepare("SELECT sourceName, targetName FROM ScopeClone "
                  "GROUP BY sourceName, targetName");
    query.exec();

    m_cloneTargets.clear();
    m_cloneSources.clear();
    while(query.next()) {
        m_cloneTargets.insert(query.value(0).toString(), query.value(1).toString());
        m_cloneSources.insert(query.value(1).toString(), query.value(0).toString());
    }

    qDebug() << "Clones READ: " << m_cloneTargets.count();
}

void Mapper::readFiles()
{
    QSqlQuery query(m_dataBase);
    query.prepare("SELECT parentName, fileName FROM File "
                  "WHERE FNType & ? AND fileName LIKE \'" + m_currentRoot + "%\' "
                  "GROUP BY parentName, fileName "
                  "ORDER BY FNTYpe, fileName ");
    query.bindValue(0, m_nodeFilter);
    query.exec();

    m_fileChildren.clear();
    m_fileParent.clear();
    while(query.next()) {
        m_fileChildren.insert(query.value(0).toString(), query.value(1).toString());
        m_fileParent.insert(query.value(1).toString(), query.value(0).toString());
    }

    qDebug() << "Files READ: " << m_fileParent.count();
}

void Mapper::readScopes()
{
    QSqlQuery query(m_dataBase);
    query.prepare("SELECT fileName, parentName, scopeName FROM Scope "
                  "WHERE fileName LIKE \'" + m_currentRoot + "%\' AND SNType & ? "
                  "GROUP BY fileName, scopeName "
                  "ORDER BY SNType, scopeName ");  //! \todo: make conditional
    query.bindValue(0, m_nodeFilter);
    query.exec();

    m_fileScopes.clear();
    m_scopeChildren.clear();
    m_scopeParent.clear();
    while(query.next()) {
        m_fileScopes.insert(query.value(0).toString(),    query.value(2).toString());
        m_scopeChildren.insert(query.value(1).toString(), query.value(2).toString());
        m_scopeParent.insert(query.value(2).toString(),   query.value(1).toString());
    }

    QSqlQuery queryF(m_dataBase);
    queryF.prepare("SELECT scopeName, fileName FROM Scope "
                  "WHERE fileName <> \"\" "
                  "GROUP BY scopeName");
    queryF.exec();
    m_scopeFiles.clear();
    while(queryF.next()) {
        m_scopeFiles.insert(queryF.value(0).toString(),    queryF.value(1).toString());
    }

    qDebug() << "Scopes READ: " << m_scopeParent.count();
}

void Mapper::makeEdgeColorMap()
{
    unsigned int startTime = m_processCalled;

    // Set buffer
    QMultiHash <QString,QColor> *buffer = m_edgeColorMapBuffer;
    buffer->clear();

    // Start time measurement
    QElapsedTimer myTimer;
    myTimer.start();
//    qDebug() << "Generating edge colormap for revision" << m_minRevision << m_maxRevision;

    switch(m_colorMap) {
        case Color_Activity:    edgeColorMapActivity(buffer);   break;
        case Color_Differences: edgeColorMapDiffs(buffer);      break;
        case Color_Structure:   edgeColorMapStructure(buffer);  break;
    }

//    qDebug() << "Generated edge colormap amount " << buffer->count();
    qDebug() << "Generated edge colormap for revision " << m_minRevision << m_maxRevision
             << "in" << myTimer.elapsed() << "ms" << Qt::endl;

    if (startTime < m_processCalled) return;

    // Swap buffers
    m_edgeColorMapBuffer = m_edgeColorMap;
    m_edgeColorMap       = buffer;

    emit updateEdgeColors();
}

void Mapper::makeRingColorMap()
{
    unsigned int startTime = m_processCalled;

    // Set buffer
    QHash <QString,QColor> *buffer = m_ringColorMapBuffer;
    buffer->clear();

    // Start time measurement
    QElapsedTimer myTimer;
    myTimer.start();
//    qDebug() << "Generating ring colormap for revision" << m_minRevision << m_maxRevision;

    switch(m_colorMap) {
        case Color_Activity:    ringColorMapActivity(buffer);   break;
        case Color_Differences: ringColorMapDiffs(buffer);      break;
        case Color_Structure:   ringColorMapStructure(buffer);  break;
    }

    qDebug() << "Generated ring colormap for revision " << m_minRevision << m_maxRevision << "in" << myTimer.elapsed() << "ms";

    if (startTime < m_processCalled) return;

    // Swap buffers
    m_ringColorMapBuffer = m_ringColorMap;
    m_ringColorMap       = buffer;

    emit updateRingColors();
}

void Mapper::makeRingElements()
{
    m_ringElements.clear();
    m_ringElementLeafs.clear();
    m_ringElementDepth.clear();
    m_ringElementInvDepth.clear();
    m_maxTreeDepth = 0;
    makeRingElements(m_currentRoot, 0);
}

int  Mapper::makeRingElements(QString node, int depth)
{
    int leafs = 0, invDepth = 0;

    foreach(QString child, m_fileChildren.values(node)) {
        m_ringElements.insert(node, child);
        leafs += makeRingElements(child, depth+1);

        if (m_ringElementInvDepth[child] >= invDepth)
            invDepth = 1 + m_ringElementInvDepth[child];
    }

    foreach(QString child, m_fileScopes.values(node)) {
        if (m_fileScopes.values(node).contains(m_scopeParent[child])) {
            continue;
        }

        m_ringElements.insert(node, child);
        leafs += makeRingElements(child, depth+1);

        if (m_ringElementInvDepth[child] >= invDepth)
            invDepth = 1 + m_ringElementInvDepth[child];
    }

    foreach(QString child, m_scopeChildren.values(node)) {
        if (m_scopeFiles[child] != m_scopeFiles[node]) continue;

        m_ringElements.insert(node, child);
        leafs += makeRingElements(child, depth+1);

        if (m_ringElementInvDepth[child] >= invDepth)
            invDepth = 1 + m_ringElementInvDepth[child];
    }

    if (leafs == 0) leafs = 1;

    m_ringElementDepth.insert(node, depth);
    m_ringElementLeafs.insert(node, leafs);
    m_ringElementInvDepth.insert(node, invDepth);

    if (m_maxTreeDepth < depth) m_maxTreeDepth = depth;

    return leafs;
}

/*** COLORMAP GENERATION **************************************************************************/
void Mapper::ringColorMapActivity(QHash<QString,QColor> *buffer)
{
    QSqlQuery query(m_dataBase);
    query.prepare("SELECT  Max(amount) FROM ("
                  "SELECT File.fileName AS name, Count() as amount FROM File "
                  "WHERE operation <> 0 AND revisionNr >= ? AND revisionNr <= ? AND fileName <> ? "
                  "GROUP BY name "
                  "UNION "
                  "SELECT Scope.scopeName AS name, Count() as amount FROM Scope "
                  "WHERE operation <> 0 AND revisionNr >= ? AND revisionNr <= ? AND scopeName <> \":::: (root)\""
                  "GROUP BY name "
                  "ORDER BY Count() DESC "
                  ")");

    query.bindValue(0, m_minRevision);
    query.bindValue(1, m_maxRevision);
    query.bindValue(2, m_currentRoot);
    query.bindValue(3, m_minRevision);
    query.bindValue(4, m_maxRevision);
    query.exec();

    query.first();
    double maxCount = query.value(0).toDouble();

    //! \todo: Make proper query for this without hardcoded node names
    query.prepare("SELECT File.fileName AS name, Count() as amount  FROM File "
                  "WHERE revisionNr >= ? AND revisionNr <= ? AND fileName <> ? "
                  "GROUP BY name "
                  "UNION "
                  "SELECT Scope.scopeName AS name, Count() as amount FROM Scope "
                  "WHERE revisionNr >= ? AND revisionNr <= ? AND scopeName <> \":::: (root)\""
                  "GROUP BY name "
                  "ORDER BY Count() DESC ");

    query.bindValue(0, m_minRevision);
    query.bindValue(1, m_maxRevision);
    query.bindValue(2, m_currentRoot);
    query.bindValue(3, m_minRevision);
    query.bindValue(4, m_maxRevision);
    query.exec();

    double count = 0;
    while(query.next()) {
        count = query.value(1).toDouble();
        buffer->insert(query.value(0).toString(), colorRainbow(sqrt(count/maxCount)));
    }
}

void Mapper::ringColorMapDiffs(QHash<QString, QColor> *buffer)
{
    // Do Diff ColorMap
    static QColor colA("green");
    static QColor colD("red");
    static QColor colM("orange");

    // The query orders the data by operation, before grouping it.
    // D > A > M, which makes sure that the relevant data is shown
    QSqlQuery query(m_dataBase);
    query.prepare("SELECT * FROM ( "
                    "SELECT File.fileName AS name, operation, revisionNr FROM File "
                        "WHERE revisionNr >= ? AND revisionNr <= ? AND operation <> 0 "
                    "UNION "
                    "SELECT Scope.scopeName AS name, operation, revisionNr FROM Scope "
                        "WHERE revisionNr >= ? AND revisionNr <= ? AND operation <> 0 "
                    "ORDER BY revisionNr DESC "
                  ") GROUP BY name "
                  );

    query.bindValue(0, m_minRevision);
    query.bindValue(1, m_maxRevision);
    query.bindValue(2, m_minRevision);
    query.bindValue(3, m_maxRevision);
    query.exec();

    while(query.next()) {
        switch (query.value(1).toInt()) {
            case 1: buffer->insert(query.value(0).toString(), colM); break;
            case 2: buffer->insert(query.value(0).toString(), colA); break;
            case 3: buffer->insert(query.value(0).toString(), colD); break;
        }
    }
}

void Mapper::ringColorMapStructure(QHash<QString, QColor> *buffer)
{
    QSqlQuery query(m_dataBase);
    query.prepare("SELECT * FROM ("
                    "SELECT File.fileName AS name, File.FNType AS type, operation FROM File "
                    "WHERE revisionNr >= ? AND revisionNr <= ? "
                    "GROUP BY name "
                    "UNION "
                    "SELECT Scope.scopeName AS name, Scope.SNType AS type, operation FROM Scope "
                    "WHERE revisionNr >= ? AND revisionNr <= ? "
                    "GROUP BY name "
                  ") WHERE operation <> 3 ");   // Do not show deleted nodes

    query.bindValue(0, m_minRevision);
    query.bindValue(1, m_maxRevision);
    query.bindValue(2, m_minRevision);
    query.bindValue(3, m_maxRevision);
    query.exec();

    QColor col;
    while(query.next()) {
        switch ((NodeType)query.value(1).toInt()) {
            case TYPE_NAMESPACE:    col = QColor(Qt::darkMagenta);  break;
            case TYPE_CLASS:        col = QColor(Qt::magenta);      break;
            case TYPE_FUNCTION:     col = QColor(Qt::darkRed);      break;
            case TYPE_SLOT:         col = QColor(Qt::red);          break;
            case TYPE_ATTRIBUTE:    col = QColor(Qt::darkCyan);     break;
            case TYPE_SIGNAL:       col = QColor(Qt::cyan);         break;
            case TYPE_ENUM:         col = QColor(Qt::darkGreen);    break;
            case TYPE_DEFINE:       col = QColor(Qt::green);        break;
            case TYPE_FILE:         col = QColor(Qt::yellow);       break;
            case TYPE_DIR:          col = QColor(Qt::darkYellow);   break;
            default:                col = QColor(Qt::lightGray);    break;
        }
        buffer->insert(query.value(0).toString(), col);
    }
}

void Mapper::edgeColorMapActivity(QMultiHash<QString, QColor> *buffer)
{
    // Get the maximum value
    QSqlQuery query(m_dataBase);
    query.prepare("SELECT  Max(amount) FROM ( "
                    "SELECT sourceName, targetName, Count() as amount FROM ScopeClone "
                    "WHERE targetRevisionNr >= ? AND targetRevisionNr <= ? "
                    " AND isIntra = 1 AND intraType <> 68 "
                    "GROUP BY sourceName, targetName"
                  ")"
                  );
    query.bindValue(0, m_minRevision);
    query.bindValue(1, m_maxRevision);
    query.exec();

    query.first();
    double maxCount = query.value(0).toDouble();

    // Get the list of values
    query.prepare("SELECT sourceName, targetName, Count() as amount, targetRevisionNr FROM ScopeClone "
                    "WHERE targetRevisionNr >= ? AND targetRevisionNr <= ? "
                    " AND isIntra = 1 "
                    "GROUP BY sourceName, targetName"
                  );
    query.bindValue(0, m_minRevision);
    query.bindValue(1, m_maxRevision);
    query.exec();

    double  count = 0.0;
    double  value = 0.0;
    QColor  col;
    QString name;
    while(query.next()) {
        name  = query.value(0).toString() + " <==> " + query.value(1).toString();
        count = query.value(2).toDouble();
        value = sqrt(count/maxCount);
        col   = colorRainbow(value);
        col.setAlphaF(0.1 + 0.9 * value);
        buffer->insert(name, col);
    }
}

void Mapper::edgeColorMapDiffs(QMultiHash<QString, QColor> *buffer)
{
    QString cloneFilter;

    cloneFilter += (m_showIntraClones || m_showInterClones ? "AND ( " : "");
    cloneFilter += (m_showIntraClones ? "intraType > 0 " : "");
    cloneFilter += (m_showIntraClones && m_showInterClones ? " OR " : "");
    cloneFilter += (m_showInterClones ? "driftType > 0 " : "");
    cloneFilter += (m_showIntraClones || m_showInterClones ? ") " : "");

    double maxLines = 0;

    // Get the maximum value
    QSqlQuery query(m_dataBase);
    if (m_edgeMetric == Metric_Size) {
        query.prepare("SELECT Max(matchLines) FROM ( "
                        "SELECT * FROM ScopeClone "
                        "WHERE targetRevisionNr >= ? AND targetRevisionNr <= ? "
                        + cloneFilter +
                        "GROUP BY sourceName, targetName "
                      ") WHERE intraType <> 68");
        query.bindValue(0, m_minRevision);
        query.bindValue(1, m_maxRevision);
        query.exec();

        query.first();
        maxLines = qMax(1.0,query.value(0).toDouble());
    }

    // Will remove deleted nodes from the structure overview
    query.prepare("SELECT sourceName, targetName, driftType, intraType, targetRevisionNr, matchLines FROM ScopeClone "
                  "WHERE targetRevisionNr >= ? AND targetRevisionNr <= ? "
                  + cloneFilter +
                  "GROUP BY sourceName, targetName");
    query.bindValue(0, m_minRevision);
    query.bindValue(1, m_maxRevision);
    query.exec();

    QColor  col;
    QString name;
    int     alpha = 0;
    double  value = 0.0;
    double  range = m_maxRevision - m_minRevision + 1;
    while(query.next()) {
        name  = query.value(0).toString() + " <==> " + query.value(1).toString();

        switch(m_edgeMetric) {
            case Metric_None:
                alpha = 64.0;
                break;
            case Metric_Age:
                value = (query.value(4).toDouble() - m_minRevision + 1) / range;
                alpha = 128.0 * (0.1 + 0.9 * value);
                break;
            case Metric_Size:
                value = query.value(5).toDouble() / maxLines;
                alpha = 128.0 * (0.1 + 0.9 * value);
                break;
        }

        if (query.value(2).toInt() != 0) { // Inter
            // First target color, then source color
            buffer->insert(name, QColor(0,255,0,alpha)); // Green
            buffer->insert(name, QColor(255,0,0,alpha)); // Red
        } else { // Intra
            switch (query.value(3).toInt()) {
                case 65:    col = QColor(0,0,255,alpha);      break; // Blue
                case 68:    col = QColor(255,255,0,alpha);    break; // Yellow
                case 0:     col = QColor(224,224,224,alpha);  break; // Grey
                default:    continue;
            }
            buffer->insert(name, col);
        }
    }
}

void Mapper::edgeColorMapStructure(QMultiHash<QString, QColor> *buffer)
{
    QSqlQuery query(m_dataBase);
    double maxLines = 0;

    // Get the maximum value
    if (m_edgeMetric == Metric_Size) {
        query.prepare("SELECT Max(matchLines) FROM ( "
                        "SELECT * FROM ScopeClone "
                        "WHERE targetRevisionNr >= ? AND targetRevisionNr <= ? "
                        "AND isIntra = 1 "
                        "GROUP BY sourceName, targetName "
                      ") WHERE intraType <> 68");
        query.bindValue(0, m_minRevision);
        query.bindValue(1, m_maxRevision);
        query.exec();

        query.first();
        maxLines = qMax(1.0,query.value(0).toDouble());
    }

    // Show all clones that still exist in this revision
    query.prepare("SELECT sourceName, targetName, targetRevisionNr, matchLines FROM ( "
                    "SELECT * FROM ScopeClone "
                    "WHERE targetRevisionNr >= ? AND targetRevisionNr <= ? "
                    "AND isIntra = 1 "
                    "GROUP BY sourceName, targetName "
                  ") WHERE intraType <> 68");
    query.bindValue(0, m_minRevision);
    query.bindValue(1, m_maxRevision);
    query.exec();

    int     alpha = 0;
    double  value = 0.0;
    double  range = m_maxRevision - m_minRevision + 1;
    QString name;
    while(query.next()) {
        name  = query.value(0).toString() + " <==> " + query.value(1).toString();

        switch(m_edgeMetric) {
            case Metric_None:
                alpha = 64.0;
                break;
            case Metric_Age:
                value = (query.value(2).toDouble() - m_minRevision + 1) / range;
                alpha = 128.0 * (0.1 + 0.9 * value);
                break;
            case Metric_Size:
                value = query.value(3).toDouble() / maxLines;
                alpha = 128.0 * (0.1 + 0.9 * value);
                break;
        }

        buffer->insert(name, QColor(128,128,128,alpha));
    }
}

QColor Mapper::colorRainbow(double value)
{
    const double dx=0.8;
    value = qBound(0.0, value, 1.0);
    value = (6-2*dx)*value+dx;
    return QColor(255 * qMax(0.0,(3-qAbs(value-4)-qAbs(value-5))/2),
                  255 * qMax(0.0,(4-qAbs(value-2)-qAbs(value-4))/2),
                  255 * qMax(0.0,(3-qAbs(value-1)-qAbs(value-2))/2));
}

QColor Mapper::colorRedWhite(double value)
{
    return QColor(255 * 2 * qMin(value, 0.5),
                  255 * 2 * qMax(0.0 , value - 0.5),
                  255 * 2 * qMax(0.0 , value - 0.5));
}
