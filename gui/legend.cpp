#include "legend.h"
#include "ui_legend.h"

/* ************************************************************************************************
 * Constructor & Destructor
 **************************************************************************************************/
Legend::Legend(QWidget *parent) :
    QWidget (parent)
  , ui      (new Ui::legend)
{
    ui->setupUi(this);
}

Legend::~Legend()
{
    delete ui;
}

/* ************************************************************************************************
 * Public Functions
 **************************************************************************************************/
void Legend::setColorMap(Mapper::ColorMap colorMap)
{
    ui->ActFrame->setVisible(false);
    ui->DiffFrame->setVisible(false);
    ui->StructFrame->setVisible(false);

    switch(colorMap) {
        case Mapper::Color_Activity:
            ui->ActFrame->setVisible(true);
            break;
        case Mapper::Color_Differences:
            ui->DiffFrame->setVisible(true);
            break;
        case Mapper::Color_Structure:
            ui->StructFrame->setVisible(true);
            break;
    }
}
