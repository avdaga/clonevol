#ifndef MAPPER_H
#define MAPPER_H

#include <QColor>
#include <QMultiHash>
#include <QMutex>
#include <QObject>
#include <QString>
#include <QtSql>

/*! ***********************************************************************************************
 * \author  Avdo Hanjalic
 * \date    30.07.2013
 * \class   Mapper
 *
 * \brief   This class queries the database, based on user input and generates the Ring elements
 *          and Edges to be drawn in visualization.
 **************************************************************************************************/
class Mapper : public QObject
{
    Q_OBJECT

public:
    /* ****************************************************************************************
     * Enums
     ******************************************************************************************/
    enum ColorMap {
        Color_Structure,
        Color_Differences,
        Color_Activity
    };

    enum EdgeMetric {
        Metric_None,
        Metric_Age,
        Metric_Size
    };

    /* ****************************************************************************************
     * Constructor & Destructor
     ******************************************************************************************/
    Mapper(QString fileName);
    ~Mapper();

    /* ****************************************************************************************
     * Getters & Setters
     ******************************************************************************************/
    // ** Stuff that is read once
    int                             FileCount()                     { return m_fileParent.count(); }
    int                             ScopeCount()                    { return m_scopeParent.count(); }
    int                             &FileCountTot()                 { return m_nFilesTotal; }
    int                             &ScopeCountTot()                { return m_nScopesTotal; }
    QString                         &ProjectName()                  { return m_projectName; }
    QString                         &ProjectUrl()                   { return m_projectUrl; }
    QList<int>                      &Revisions()                    { return m_revisions; }
    QList<bool>                     &RevisionsAcq()                 { return m_revisionsAcq; }
    QString                         &RootElement()                  { return m_currentRoot; }

    // ** Stuff that is regenerated based on user input
    QMultiHash<QString, QColor>     *EdgeColorMap()                 { return m_edgeColorMap; }
    QMultiHash<QString, QString>    &EdgeElements()                 { return m_cloneTargets; }

    int                             MaxTreeDepth()                  { return m_maxTreeDepth; }
    QHash<QString, QColor>          *RingColorMap()                 { return m_ringColorMap; }
    QMultiHash<QString, QString>    &RingElements()                 { return m_ringElements; }
    QHash<QString, int>             &RingElementLeafs()             { return m_ringElementLeafs; }
    QHash<QString, int>             &RingElementInvDepth()          { return m_ringElementInvDepth; }
    QHash<QString, int>             &RingElementDepth()             { return m_ringElementDepth; }

    // ** General Getters
    unsigned int                    &NodeFilter()                   { return m_nodeFilter; }

    int                             &MaxRevision()                  { return m_maxRevision; }
    int                             &MinRevision()                  { return m_minRevision; }

    /* ****************************************************************************************
     * Public Functions
     ******************************************************************************************/
    void                reloadDatabase();

public slots:
    /* ****************************************************************************************
     * Public Slots
     ******************************************************************************************/
    void                setColorMap         (ColorMap c);
    void                setEdgeMetric       (EdgeMetric m);
    void                setMinRevision      (int n);
    void                setMinMaxRevision   (int n, int m);
    void                setMaxRevision      (int n);
    void                setNodeFilter       (unsigned int filter);
    void                setRootNode         (const QString &nodeName, bool force=false);
    void                setRootNodeLevelUp  ();
    void                setShowClones       (bool old, bool intra, bool inter);

signals:
    /* ****************************************************************************************
     * Signals
     ******************************************************************************************/
    void                updateEdgeColors();
    void                updateRing();
    void                updateRingColors();

private:
    /* ****************************************************************************************
     * Private Functions
     ******************************************************************************************/
    void                readClones();
    void                readFiles();
    void                readScopes();

    void                makeEdgeColorMap();
    void                makeRingColorMap();
    void                makeRingElements();
    int                 makeRingElements(QString node, int depth);    // Returns nLeafs of node

    void                ringColorMapActivity   (QHash<QString, QColor> *buffer);
    void                ringColorMapDiffs      (QHash<QString, QColor> *buffer);
    void                ringColorMapStructure  (QHash<QString, QColor> *buffer);

    void                edgeColorMapActivity   (QMultiHash<QString, QColor> *buffer);
    void                edgeColorMapDiffs      (QMultiHash<QString, QColor> *buffer);
    void                edgeColorMapStructure  (QMultiHash<QString, QColor> *buffer);

    QColor              colorRainbow (double value);
    QColor              colorRedWhite(double value);

    /* ****************************************************************************************
     * Attrributes
     ******************************************************************************************/
    // ** Project properties (loaded only once)
    QString             m_fileName;
    QSqlDatabase        m_dataBase;
    QMutex              m_processing;
    unsigned int        m_processCalled;

    QString             m_projectName;
    QString             m_projectUrl;

    QList<int>          m_revisions;
    QList<bool>         m_revisionsAcq;

    int                 m_nFilesTotal;
    int                 m_nScopesTotal;

    // ** Filtering parameters (set by user)
    ColorMap            m_colorMap;
    QString             m_currentRoot;
    EdgeMetric          m_edgeMetric;
    int                 m_maxTreeDepth;
    int                 m_minRevision;
    int                 m_maxRevision;
    unsigned int        m_nodeFilter;

    bool                m_showInterClones;
    bool                m_showIntraClones;
    bool                m_showOldIntraClones;

    // ** Current Filter Result (loaded on each user input)
    QMultiHash  <QString, QString>  m_cloneTargets;
    QMultiHash  <QString, QString>  m_cloneSources;
    QMultiHash  <QString, QString>  m_fileChildren;                 // <File,  ChildFile>
    QHash       <QString, QString>  m_fileParent;                   // <File,  ParentFile>
    QMultiHash  <QString, QString>  m_fileScopes;                   // <File,  Scopes>
    QMultiHash  <QString, QString>  m_scopeChildren;                // <Scope, ChildScope>
    QHash       <QString, QString>  m_scopeFiles;                   // <Scope, File>
    QHash       <QString, QString>  m_scopeParent;                  // <Scope, ParentScope>

    // ** Visualization elements (publicly accessible by view)
    QMultiHash  <QString, QColor>   *m_edgeColorMap;
    QMultiHash  <QString, QColor>   *m_edgeColorMapBuffer;
//    QMultiHash  <QString, QString>  m_edgeElements;

    QHash       <QString, QColor>   *m_ringColorMap;
    QHash       <QString, QColor>   *m_ringColorMapBuffer;
    QMultiHash  <QString, QString>  m_ringElements;
    QHash       <QString, int>      m_ringElementLeafs;
    QHash       <QString, int>      m_ringElementDepth;
    QHash       <QString, int>      m_ringElementInvDepth;

    //! \todo: Remove friend classes
    friend class RadialGraphicsTreeView;
};

#endif // MAPPER_H
