#ifndef EXPORTDIALOG_H
#define EXPORTDIALOG_H

#include <QDialog>

namespace Ui {
class ExportDialog;
}

/*! ***********************************************************************************************
 * \author  Avdo Hanjalic
 * \date    2013
 * \struct  ExportConfig
 **************************************************************************************************/
struct ExportConfig
{
    int  minRev;
    int  maxRev;
    int  step;
    bool stretch;
    bool slide;
};

/*! ***********************************************************************************************
 * \author  Avdo Hanjalic
 * \date    2013
 * \class   ExportDialog
 *
 * Dialog that allows the user to set up an ExportConfig.
 **************************************************************************************************/
class ExportDialog : public QDialog
{
    Q_OBJECT

public:
    /* ****************************************************************************************
     * Public Functions
     ******************************************************************************************/
    static ExportConfig getConfig(int max, bool *ok);

private:
    /* ****************************************************************************************
     * Constructor & Destructor
     ******************************************************************************************/
    explicit ExportDialog(QWidget *parent = 0);
    ~ExportDialog();

private slots:
    /* ****************************************************************************************
     * Private Slots
     ******************************************************************************************/
    void                on_spinBox_valueChanged(int arg1);
    void                on_radioButton_7_toggled(bool checked);
    void                on_radioButton_9_toggled(bool checked);

private:
    /* ****************************************************************************************
     * Attrributes
     ******************************************************************************************/
    static ExportDialog *m_instance;

    Ui::ExportDialog    *ui;
    ExportConfig        m_config;
};

#endif // EXPORTDIALOG_H
