#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStandardItemModel>

#include "gui/exportdialog.h"
#include "mining/progressinfo.h"

class DataStore;
class FileNode;
class Mapper;
class Miner;
class ScopeNode;
class RevisionNode;

namespace Ui { class MainWindow; }

/*! ***********************************************************************************************
 * \author  Avdo Hanjalic
 * \date    01.01.2013
 * \class   MainWindow
 *
 * \brief   Main GUI Window
 **************************************************************************************************/
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    /* ****************************************************************************************
     * Getters & Setters
     ******************************************************************************************/
    static MainWindow*  Instance();

signals:
    /* ****************************************************************************************
     * Signals
     ******************************************************************************************/
    void                sigMinMaxRevision   (int minRev, int maxRev);
    void                sigMineDetails      (int minRev, int maxRev);
    void                sigMineLogs();

private:
    /* ****************************************************************************************
     * Constructor & Destructor
     ******************************************************************************************/
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

    /* ****************************************************************************************
     * Private Functions
     ******************************************************************************************/
    QString             getApplicationName();

    void                initDataStore();
    void                initMapper(QString fileName);
    void                initMiner();

    void                loadFile(QString fileName);
    void                makeRevisionGradient();

    void                exportSeq();

private slots:
    /* ****************************************************************************************
     * Private Slots
     ******************************************************************************************/
    // Internal callbacks
    void                onProgressInfo(ProgressInfo info);
    void                onRenderingDone();
    void                onStepProgressInfo(ProgressInfo info);

    // Buttons
    void                on_levelUpButton_clicked();
    void                on_logButton_clicked();
    void                on_ExportSvgButton_clicked();
    void                on_ExportPngButton_clicked();
    void                on_ExportSequence_clicked();

    void                on_startButton_clicked();
    void                on_openFileButton_clicked();
    void                on_getRevisionsButton_clicked();
    void                on_cancelRevisionsButton_clicked();

    // Filter (re)set buttons
    void                on_setNodesFilterAllButton_clicked();
    void                on_setNodesFilterButton_clicked();
    void                on_resetNodesFilterButton_clicked();

    // Sliders
    void                on_maxRevSlider_valueChanged          (int value);
    void                on_minRevSlider_valueChanged          (int value);
    void                on_bundlingStrengthSlider_valueChanged(int value);
    void                on_splineFragmentSlider_valueChanged  (int value);

    // Checkboxes
    void                on_flattenRingCheck_toggled     (bool checked);
    void                on_edgesCheck_toggled           (bool checked);
    void                on_nodesCheck_toggled           (bool checked);
    void                on_intraClonesCheck_toggled     (bool checked);
    void                on_oldIntraClonesCheck_toggled  (bool checked);
    void                on_InterClonesCheck_toggled     (bool checked);

    // Colormap Radio buttons
    void                on_structureColorButton_toggled (bool checked);
    void                on_diffColorButton_toggled      (bool checked);
    void                on_activityColorButton_toggled  (bool checked);

    void                on_checkBox_toggled             (bool checked);
    void                on_edgePathXOR_toggled          (bool checked);
    void                on_conrolPointsCheck_toggled    (bool checked);

    // Combo Boxes
    void                on_edgeMetricCombo_currentIndexChanged(const QString &arg1);

private:
    /* ****************************************************************************************
     * Attrributes
     ******************************************************************************************/
    static MainWindow   *m_instance;

    Ui::MainWindow      *ui;

    DataStore           *m_dataStore;
    Mapper              *m_mapper;
    QThread             *m_mapperThread;
    Miner               *m_miner;
    QThread             *m_minerThread;

    bool                m_viewFileTree;
    unsigned int        m_nodeFilter;

    ExportConfig        m_exportConfig;
    int                 m_exportCurRevMax;
    int                 m_exportCurRevMin;
    QString             m_exportDir;
};

#endif // MAINWINDOW_H
