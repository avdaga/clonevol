#ifndef LOGBROWSER_H
#define LOGBROWSER_H

#include <QMainWindow>
#include <QMutex>

namespace Ui { class LogBrowser; }

/*! ***********************************************************************************************
 * \author  Avdo Hanjalic
 * \date    2013
 * \struct  LogBrowser
 *
 * \brief   Forwards all qDebug() to sigWriteLog()
 **************************************************************************************************/
class LogBrowser : public QMainWindow
{
    Q_OBJECT

public:
    /* ****************************************************************************************
     * Getters & Setters
     ******************************************************************************************/
    static LogBrowser   *Instance();

private:
    /* ****************************************************************************************
     * Constructor & Destructor
     ******************************************************************************************/
    LogBrowser(QWidget *parent = 0);
    ~LogBrowser();

private slots:
    /* ****************************************************************************************
     * Private Slots
     ******************************************************************************************/
    void                writeLog(const QString &msg);

    void                on_clearButton_clicked();

private:
    /* ****************************************************************************************
     * Attrributes
     ******************************************************************************************/
    static LogBrowser   *m_instance;

    Ui::LogBrowser      *ui;
    unsigned long       m_bufferLength;
    QMutex              m_logMutex;
};

#endif // LOGBROWSER_H
