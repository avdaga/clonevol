#include "radialgraphicstreeview.h"

#include "gui/mapper.h"

/*******************************************************************************
*   Setters
*******************************************************************************/

//*** Original Stuff by Sandro *************************************************
void RadialGraphicsTreeView::setViewFlag(RadialGraphicsTreeView::ViewFlag viewFlag, bool enabled) {
    if (enabled)    m_viewFlags |= viewFlag;
    else            m_viewFlags &= ~viewFlag;

    refreshAll();
}

void RadialGraphicsTreeView::setViewFlags(ViewFlags viewFlags) {
    m_viewFlags = viewFlags;
}

void RadialGraphicsTreeView::setNodePen(const QPen &nodePen) {
    m_nodePen = nodePen;
}

void RadialGraphicsTreeView::setNodeBrush(const QBrush &nodeBrush) {
    m_nodeBrush = nodeBrush;
}

void RadialGraphicsTreeView::setEdgeChop(const bool edgeChop) {
    m_edgeChop = edgeChop;
    refreshEdges();
}

void RadialGraphicsTreeView::setEdgePathXOR(const bool edgePathXOR) {
    m_edgePathXOR = edgePathXOR;
    refreshEdges();
}

void RadialGraphicsTreeView::setInterLevelSpacing(qreal interLevelSpacing) {
    m_innerLevelSpace = interLevelSpacing;
}

void RadialGraphicsTreeView::setInterSpanSpacing(qreal interSpanSpacing) {
    m_interSpanSpacing = interSpanSpacing;
}

void RadialGraphicsTreeView::setBundlingStrength(qreal beta) {
    m_edgeHoltenBeta = qMin(qMax(beta, 0.0), 1.0);
    refreshEdges();
}

//*** Added Stuff by Avdo ******************************************************
void RadialGraphicsTreeView::setMapper(Mapper *m) {
    // Sanity check
    if (m_mapper != 0) {
        m_mapper = m;
    } else {
        // first time initialization
        m_mapper = m;
        connect(m_mapper, SIGNAL(updateRingColors()), this, SLOT(refreshTreeColors()));
        connect(m_mapper, SIGNAL(updateEdgeColors()), this, SLOT(refreshEdgeColors()));
        connect(m_mapper, SIGNAL(updateRing()),       this, SLOT(refreshAll()));

        refreshAll();

        // Reset pan & zoom after loading a datastore
        QRectF old = sceneRect();
        setSceneRect(old.x()*3,old.y()*3,old.width()*3,old.height()*3);
        fitInView(old, Qt::KeepAspectRatio);
        scale(0.9,0.9);
        rotate(180.0);  // No idea why, but necessary
    }
}

void RadialGraphicsTreeView::setFlattenRing(bool val) {
    if (m_flattenRing != val) {
        m_flattenRing = val;
        refreshAll();
    }
}

void RadialGraphicsTreeView::setEdgeRelaxation(qreal v) {
    qreal correctedValue = qMin(qMax(v, 0.5), 1.0);
    if (m_edgeRelaxation != correctedValue) {
        m_edgeRelaxation = correctedValue;
        refreshEdges();
    }
}

void RadialGraphicsTreeView::setShowObstructedEdges(const bool show) {
    if (m_showObstructedEdges != show) {
        m_showObstructedEdges = show;
        refreshEdges();
    }
}

void RadialGraphicsTreeView::setShowControlPoints(bool val) {
    if (m_showControlPoints != val) {
        m_showControlPoints = val;
        refreshEdges();
    }
}
