#include "graphicstreenode.h"

#include "datastore/filenode.h"
#include "datastore/scopenode.h"

GraphicsTreeNode::GraphicsTreeNode(QString name, qreal radius, QGraphicsItem *parent) :
    QGraphicsEllipseItem(-radius, -radius, 2*radius, 2*radius, parent)
{
    setToolTip(name);
    setZValue(2);
}
