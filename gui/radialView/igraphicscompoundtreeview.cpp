/********************************************************************************
 *   Copyright (C) 2012 by Sandro Andrade <sandroandrade@kde.org>               *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#include "igraphicscompoundtreeview.h"

#include <QtGui/QMouseEvent>
#include <QtGui/QWheelEvent>

#include <QtSvg/QSvgGenerator>
#include <QtWidgets/QFileDialog>

#if (QT_VERSION_MAJOR == 6)
#include <QtOpenGLWidgets/QOpenGLWidget>
#elif (QT_VERSION_MAJOR == 5)
#include <QtWidgets/QOpenGLWidget>
#endif

IGraphicsCompoundTreeView::IGraphicsCompoundTreeView(QWidget *parent) :
    QGraphicsView   (parent),
    m_exportDir     (".")
{
    setDragMode(ScrollHandDrag);
    setMouseTracking(true);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

#ifndef Q_CC_MSVC
    // Use OpenGL for rendering (does not work with MSVC)
    setViewport(new QOpenGLWidget());
#endif

    setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform);
    setViewportUpdateMode(QGraphicsView::SmartViewportUpdate);
    setTransformationAnchor(QGraphicsView::AnchorUnderMouse);

    setScene(new QGraphicsScene(this));
}

IGraphicsCompoundTreeView::~IGraphicsCompoundTreeView() {}


void IGraphicsCompoundTreeView::wheelEvent(QWheelEvent *event) {
    if (event->angleDelta().y() < 0)    scale(0.9, 0.9);
    else                                scale(1.0/0.9, 1.0/0.9);
}

void IGraphicsCompoundTreeView::resizeEvent(QResizeEvent *event) {
    qreal ratioX    = (qreal) event->size().width()  / event->oldSize().width();
//    qreal ratioY    = (qreal) event->size().height() / event->oldSize().height();

    scale(ratioX, ratioX);
}

void IGraphicsCompoundTreeView::exportPngDialog() {
    QString fileName = QFileDialog::getSaveFileName(this, "Export to PNG",m_exportDir,"PNG Files (*.png)");
    m_exportDir = fileName.mid(0,fileName.lastIndexOf("/"));
    exportPng(fileName);
}

void IGraphicsCompoundTreeView::exportSvgDialog() {
    QString fileName = QFileDialog::getSaveFileName(this, "Export to SVG",m_exportDir,"SVG Files (*.svg)");
    m_exportDir = fileName.mid(0,fileName.lastIndexOf("/"));
    exportSvg(fileName);
}

void IGraphicsCompoundTreeView::exportPng(QString fileName) {
    QImage image(sceneRect().size().toSize(), QImage::Format_ARGB32);
    QPainter painter(&image);
    painter.setRenderHint(QPainter::Antialiasing);
    scene()->render(&painter, QRectF(0,0,sceneRect().width(), sceneRect().height()));
    painter.end();

    image.save(fileName);
}


void IGraphicsCompoundTreeView::exportSvg(QString fileName) {
    QSvgGenerator svgGen;
    svgGen.setFileName(fileName);
    svgGen.setSize(sceneRect().size().toSize());

    QPainter painter(&svgGen);
    scene()->render(&painter, QRectF(0,0,svgGen.width()*5/4, svgGen.height()*5/4));
    painter.end();
}
