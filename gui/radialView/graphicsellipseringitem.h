/********************************************************************************
 *   Copyright (C) 2012 by Sandro Andrade <sandroandrade@kde.org>               *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#ifndef GRAPHICSELLIPSEPIEITEM_H
#define GRAPHICSELLIPSEPIEITEM_H

#include <QtGui/QPainterPath>
#include <QtWidgets/QGraphicsEllipseItem>
#include <QObject>

class RadialGraphicsTreeView;

class GraphicsEllipseRingItem : public QObject, public QGraphicsEllipseItem
{
    Q_OBJECT

public:
    explicit GraphicsEllipseRingItem(RadialGraphicsTreeView *view, QString name, qreal radius, int startAngle = 0, int spanAngle = 16*360, qreal ringFactor = 1.0, QGraphicsItem *parent = 0);
    virtual ~GraphicsEllipseRingItem();

    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);
    virtual QRectF boundingRect() const;
    virtual QPainterPath shape() const;

    qreal ringFactor() const;

public Q_SLOTS:
    void setRingFactor(qreal ringFactor);

signals:
    void sigDoubleClicked(const QString& name);

protected:
    void hoverEnterEvent(QGraphicsSceneHoverEvent *event);
    void hoverLeaveEvent(QGraphicsSceneHoverEvent *event);
    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event);

private:
    void buildPainterPath();

    QString                 m_name;
    QPainterPath            m_path;
    qreal                   m_ringFactor;
    RadialGraphicsTreeView *m_view;
};

#endif // GRAPHICSELLIPSEPIEITEM_H
