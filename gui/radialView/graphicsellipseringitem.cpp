/********************************************************************************
 *   Copyright (C) 2012 by Sandro Andrade <sandroandrade@kde.org>               *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#include "graphicsellipseringitem.h"

#include <QDebug>
#include <QPainter>
#include <QtMath>

#include "datastore/filenode.h"
#include "datastore/scopenode.h"
#include "gui/radialView/radialgraphicstreeview.h"

#define PI 3.14159265358979

GraphicsEllipseRingItem::GraphicsEllipseRingItem(RadialGraphicsTreeView *view,
                                                 QString name, qreal radius,
                                                 int startAngle, int spanAngle,
                                                 qreal ringFactor,
                                                 QGraphicsItem *parent) :
    QObject(view),
    QGraphicsEllipseItem(-radius, -radius,
                         2*radius, 2*radius, parent),
    m_name(name),
    m_ringFactor(qMin(qMax(ringFactor, 0.0), 1.0)),
    m_view(view)
{
    setStartAngle(startAngle);
    setSpanAngle(spanAngle);
    setAcceptHoverEvents(true);

//    setPen(QPen(Qt::black, 0.1));
    setBrush(Qt::lightGray);
    setToolTip(name);
    setZValue(2);
    setPen(Qt::NoPen);

    // Build the painter path only once
    buildPainterPath();
}

GraphicsEllipseRingItem::~GraphicsEllipseRingItem() {}

void GraphicsEllipseRingItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option)
    Q_UNUSED(widget)
    painter->setPen(pen());
    painter->setBrush(brush());
    painter->drawPath(m_path);
}

QPainterPath GraphicsEllipseRingItem::shape() const
{
    return m_path;
}

QRectF GraphicsEllipseRingItem::boundingRect() const
{
    return shape().controlPointRect();
}

qreal GraphicsEllipseRingItem::ringFactor() const
{
    return m_ringFactor;
}

void GraphicsEllipseRingItem::setRingFactor(qreal ringFactor)
{
    m_ringFactor = qMin(qMax(ringFactor, 0.0), 1.0);
}

void GraphicsEllipseRingItem::hoverEnterEvent(QGraphicsSceneHoverEvent *event)
{
    Q_UNUSED(event)
    if (this->pen() == Qt::NoPen)
        QGraphicsEllipseItem::setPen(QPen(Qt::black, 0.4444));
    else
        QGraphicsEllipseItem::setPen(QPen(Qt::black, pen().widthF() * 4));

    setZValue(2.5);
}

void GraphicsEllipseRingItem::hoverLeaveEvent(QGraphicsSceneHoverEvent *event)
{
    Q_UNUSED(event)
    if (this->pen().widthF() == 0.4444)
        QGraphicsEllipseItem::setPen(Qt::NoPen);
    else
        QGraphicsEllipseItem::setPen(QPen(Qt::black, pen().widthF() / 4));

    setZValue(2);
}

void GraphicsEllipseRingItem::buildPainterPath()
{
    m_path = QPainterPath();

    QRectF originalRect = rect();
    QRectF scaledRect (originalRect.x()*(1-m_ringFactor), originalRect.y()*(1-m_ringFactor), originalRect.width()*(1-m_ringFactor), originalRect.height()*(1-m_ringFactor));
    qreal a = originalRect.width()/2.0;
    qreal b = originalRect.height()/2.0;
    qreal scaledA = scaledRect.width()/2.0;
    qreal scaledB = scaledRect.height()/2.0;
    qreal startDegreeAngle = startAngle()/16.0;
    qreal stopDegreeAngle = startDegreeAngle + spanAngle()/16.0;
    qreal startRadAngle = (startDegreeAngle*PI)/180.0;
    qreal stopRadAngle = (stopDegreeAngle*PI)/180.0;
    qreal cosStartRadAngle = qCos(startRadAngle);
    qreal sinStartRadAngle = qSin(startRadAngle);
    qreal cosStopRadAngle = qCos(stopRadAngle);
    qreal sinStopRadAngle = qSin(stopRadAngle);
    qreal startRadAngleRadius = (a*b)/(qSqrt(qPow(b*cosStartRadAngle, 2)+qPow(a*sinStartRadAngle, 2)));
    qreal stopScaledRadAngleRadius = (scaledA*scaledB)/(qSqrt(qPow(scaledB*cosStopRadAngle, 2)+qPow(scaledA*sinStopRadAngle, 2)));

    m_path.moveTo(startRadAngleRadius*cosStartRadAngle, -startRadAngleRadius*sinStartRadAngle);
    m_path.arcTo(originalRect, startDegreeAngle, spanAngle()/16.0);
    m_path.lineTo(stopScaledRadAngleRadius*cosStopRadAngle, -stopScaledRadAngleRadius*sinStopRadAngle);
    m_path.arcTo(scaledRect, stopDegreeAngle, -spanAngle()/16.0);
    m_path.closeSubpath();
}

void GraphicsEllipseRingItem::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event) {
    Q_UNUSED(event);
    emit sigDoubleClicked(m_name);
}

