/********************************************************************************
 *   Copyright (C) 2012 by Sandro Andrade <sandroandrade@kde.org>               *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#include "radialgraphicstreeview.h"

#include <QApplication>
#include <QtCore/QDebug>
#include <QtCore/qmath.h>
#include <QtCore/QVariant>

#include <QtGui/QStandardItem>
#include <QtGui/QStandardItemModel>
#include <QtWidgets/QGraphicsEllipseItem>
#include <QMouseEvent>

#include "gui/mapper.h"
#include "gui/radialView/graphicsellipseringitem.h"
#include "gui/radialView/graphicstreenode.h"

#define PI 3.14159265358979

/*******************************************************************************
*   Constructor & Destructor
*******************************************************************************/
RadialGraphicsTreeView::RadialGraphicsTreeView(QWidget *parent) :
    IGraphicsCompoundTreeView(parent),
    m_freezePaint(false),
    m_processCalled(0),
    m_viewFlags( ShowMirroredRadialView   | ShowAdjacencyModel),
    m_flattenRing(true),
    m_showObstructedEdges(true),
    m_innerOuterSpace(10),
    m_innerLevelSpace(40),
    m_interSpanSpacing(0),
    m_outerLevelSpace(15),
    m_nodeBrush(Qt::yellow),
    m_nodePen(QPen(Qt::black, 2)),
    m_edgeChop(false),
    m_edgeRelaxation(0.75),
    m_edgeHoltenBeta(0.98),
    m_edgePathXOR(false),
    m_showControlPoints(false),
    m_mapper(0),
    m_figLabel(0)
{
}

RadialGraphicsTreeView::~RadialGraphicsTreeView() {}

/*******************************************************************************
*   Methods
*******************************************************************************/
void RadialGraphicsTreeView::mouseReleaseEvent(QMouseEvent *event) {
    if (event->button() == Qt::XButton1) {
        m_mapper->setRootNodeLevelUp();
    }

    QGraphicsView::mouseReleaseEvent(event);
}

void RadialGraphicsTreeView::paintEvent ( QPaintEvent * event )
{
    if (!m_freezePaint) {
        QGraphicsView::paintEvent(event);
    }
}

void RadialGraphicsTreeView::refreshAll() {
    // Sanity check
    if (m_mapper == 0) return;

    unsigned int myTime = ++m_processCalled;
    QApplication::processEvents(); // process remaining slot calls (events) until event queue is empty

    /* Prevent Queued generation */
    if (m_processCalled > myTime) return;

    m_processing.lock();
    m_freezePaint = true;
    qDebug() << ">> VIEW >> Refreshing all";

    // Clear all old stuff
    m_maxInnerRadius = 0.0;
    m_maxOuterRadius = 0.0;
    m_maxLevel       = 0;
    m_figLabel       = 0;

    m_ringItems.clear();
    m_edges.clear();

    m_spanOf.clear();
    m_childrenOf.clear();
    m_parentOf.clear();

    m_pathPoints.clear();
    m_edgeEndPoint.clear();
    m_centerPoint.clear();

    scene()->clear();

    makeTrees();
    makeEdges();
    makeLegend();
    makeTitle();
    updateFigLabel();

    if (myTime == m_processCalled) m_freezePaint = false;
    m_processing.unlock();

    emit this->sigRenderingDone();
}

void RadialGraphicsTreeView::refreshEdges() {
    // Sanity check
    if (m_mapper == 0) return;

    unsigned int myTime = ++m_processCalled;
    QApplication::processEvents(); // process remaining slot calls (events) until event queue is empty

    /* Prevent Queued generation */
    if (m_processCalled > myTime) return;

    m_processing.lock();
    m_freezePaint = true;
    qDebug() << ">> VIEW >> Refreshing edges";

    // Clear old control points
    foreach(QGraphicsEllipseItem *p, m_edgeControlPoint) {
        scene()->removeItem(p);
    }
    m_edgeControlPoint.clear();

    // Clear old edges
    //! \todo: Only remove edges we do not need anymore
    foreach(QGraphicsPathItem *pathItem, m_edges) {
        if (pathItem != 0) {
            scene()->removeItem(pathItem);
            delete pathItem;
        }
    }
    m_edges.clear();

    //! \todo: Only make edges that are not drawn yet
    makeEdges();

    if (myTime == m_processCalled) m_freezePaint = false;
    m_processing.unlock();
}

void RadialGraphicsTreeView::refreshEdgeColors() {
    foreach(QGraphicsPathItem* item, m_edges) {
        QList<QColor> colors = m_mapper->EdgeColorMap()->values(item->toolTip());
        if (colors.count() == 2) {   // Interclones
            // Make a gradient for interclones (source and target)
            QLinearGradient gradient(item->path().pointAtPercent(0), item->path().pointAtPercent(1));
            gradient.setColorAt(0, colors[0]);
            gradient.setColorAt(1, colors[1]);
            item->setPen(QPen(gradient, 1, item->pen().style()));

            // Set interclones on top
            if (item->zValue() < 1) item->setZValue(1);
            item->setVisible(true);
        } else if (colors.count() == 1) {
            item->setPen(QPen(m_mapper->EdgeColorMap()->value(item->toolTip()), 1, item->pen().style()));
            //item->setZValue(m_mapper->EdgeColorMap()->value(item->toolTip()).alphaF());
            item->setVisible(true);
        } else {
            item->setVisible(false);
        }
    }

    updateFigLabel();

    emit sigEdgeColorDone();
}

void RadialGraphicsTreeView::refreshTreeColors() {
    foreach(GraphicsEllipseRingItem* item, m_ringItems) {
        QBrush ib(Qt::lightGray, item->brush().style());

        if (m_mapper->RingColorMap()->contains(item->toolTip())) {
            ib.setColor(m_mapper->RingColorMap()->value(item->toolTip()));
            //! \todo: Add option to color Ring's brush
            //item->setPen(QColor(m_mapper->RingColorMap()->value(item->toolTip())));
        }

        item->setBrush(ib);
    }
}

// ***** GENERATION OF VISUAL OBJECTS ******************************************

void RadialGraphicsTreeView::makeEdges() {
    //! \todo: !!!!!!!!!!!!!!!!!!!!!! CLEAN UP THIS MESS !!!!!!!!!!!!!!!!!!!!!!

    GraphicsTreeNode *src, *tar;
    QPen pen(Qt::SolidLine);
    QString srcStr, tarStr, edgeName;
    //! \todo: Performance should be improved by NOT USING KEYS
    foreach(QString edge, m_mapper->EdgeElements().keys()) {
        pen.setStyle(Qt::SolidLine);
        tarStr = edge;
        srcStr = m_mapper->EdgeElements().value(edge);

        tar = m_pathPoints.value(tarStr,0);
        src = m_pathPoints.value(srcStr,0);

        // If the scope cannot be found, attach to its file
        //! \todo: Move this crap to Mapper
        if (src == 0) src = m_pathPoints.value(m_mapper->m_scopeFiles[srcStr],0);
        if (tar == 0) tar = m_pathPoints.value(m_mapper->m_scopeFiles[tarStr],0);

        // Cannot find source and target (should not happen with proper data)
        if (src == 0 || tar == 0) {
//            qDebug() << "Could not find src or target" << tarStr << srcStr;
//            qDebug() << "Could not find src or target" << m_mapper->m_scopeFiles.value(srcStr) << m_mapper->m_scopeFiles.value(tarStr);
            continue;
        }

        // Do not make (collapsed) edges to root
        if (src->toolTip() == m_mapper->RootElement() || tar->toolTip() == m_mapper->RootElement()) continue;

        // If source is not target, make sure the endpoints exist
        if (src != tar && (m_childrenOf.values(src).count() > 0 || m_childrenOf.values(tar).count() > 0)) {
            if (m_showObstructedEdges) pen.setStyle(Qt::DotLine);
            else continue;
        }

//        if (src == tar) {
//            // Do not create edge, instead give the node a brush
//            m_ringItems[tar->toolTip()]->setBrush(QBrush(Qt::DiagCrossPattern));
//            continue;
//        }

        edgeName = tarStr + " <==> " + srcStr;
        if (m_edges.contains(edgeName)) continue;

        QGraphicsPathItem *edgeItem = scene()->addPath(makeEdgePath(src, tar), pen);
        edgeItem->setToolTip(edgeName);

        // Draw self-clones on top of nodes
        if (src == tar)  edgeItem->setZValue(3);

        m_edges.insert(edgeName, edgeItem);
    }

    refreshEdgeColors();
}

void RadialGraphicsTreeView::makeTrees() {
    QString rootNode = m_mapper->RootElement();

    // Precalculate something before creating the radial nodes
    m_maxLevel       = m_mapper->MaxTreeDepth();
    m_maxInnerRadius = m_maxLevel * m_innerLevelSpace;
    m_maxOuterRadius = m_maxLevel * m_outerLevelSpace + m_maxInnerRadius + m_innerOuterSpace;

    // Generate the scene elements
    makeInnerTree(rootNode, 0.0, 2*PI, 0);
    makeOuterTree(m_pathPoints[rootNode]);

    refreshTreeColors();
}

void RadialGraphicsTreeView::updateFigLabel() {
    if (m_figLabel == 0) return;

    // Set the label text
    QString label = "Rev. " + QString::number(m_mapper->MinRevision())
                    + " - " + QString::number(m_mapper->MaxRevision());
    m_figLabel->setText(label);
    // Set the label position
    m_figLabel->setPos(0.5 * -m_figLabel->boundingRect().width(), m_figLabel->y());
}

void RadialGraphicsTreeView::makeTitle() {
    QString rootNode = m_mapper->RootElement() == "/" ? "/ (root)" : m_mapper->RootElement();

    // Set the scene to its bounding box
    QRectF boundingRect = scene()->itemsBoundingRect();

    // Add location text on top
    QGraphicsSimpleTextItem* text = new QGraphicsSimpleTextItem(rootNode);
    text->setPos(0.5 * -text->boundingRect().width(),
                 boundingRect.y() - 1.5* text->boundingRect().height());
    scene()->addItem(text);

    // Add location text on bottom
    boundingRect = scene()->itemsBoundingRect();
    m_figLabel = new QGraphicsSimpleTextItem();
    m_figLabel->setPos(0.5 * -m_figLabel->boundingRect().width(),
                 boundingRect.y() + boundingRect.height() + 5);
    scene()->addItem(m_figLabel);
}

void RadialGraphicsTreeView::makeLegend() {
    ;
}

//*** RENDERING OF SCENE TREES *************************************************
void RadialGraphicsTreeView::makeInnerTree(QString node, qreal startAngle, qreal spanAngle, GraphicsTreeNode *parentItem) {
    qreal level  = m_mapper->RingElementDepth()[node];
    qreal radius = m_maxInnerRadius - (m_mapper->RingElementInvDepth()[node] * m_innerLevelSpace);

    //*** Calculate position and size: radius, startAngle and spanAngle
    // Make gaps between outer nodes
//    if (level == 0) {
//        startAngle += m_interSpanSpacing/2.0;
//        spanAngle = qMax(spanAngle-m_interSpanSpacing, 0.0);
//    }

    // Draw level circles
//    if (m_viewFlags & ShowNormalRadialViewLevelCircles)
//        scene()->addEllipse(-radius, -radius, 2.0*radius, 2.0*radius, QPen(Qt::black));

    // Create a node in the tree (and possibly add to scene)
    GraphicsTreeNode *item = new GraphicsTreeNode(node);
    m_pathPoints[node] = item;
    QPointF itemCenter(radius*qCos(startAngle+spanAngle/2.0), -radius*qSin(startAngle+spanAngle/2.0));
    item->setPos(itemCenter);
    // Draw the node if enabled
    if (m_viewFlags & ShowNormalRadialViewNode) {
        item->setBrush(m_nodeBrush);
        item->setPen(m_nodePen);
        scene()->addItem(item);
    }

    if (parentItem != 0) {
        // Add a line to the scene if desired
        if (m_viewFlags & ShowNormalRadialViewEdge)
            scene()->addLine(QLineF(parentItem->pos(), itemCenter))->setZValue(1);
        // Generate helper values for radialview (parent's children, radius)
        if (m_viewFlags & ShowMirroredRadialView) {
            m_childrenOf.insert(parentItem, item);
            m_parentOf.insert(item, parentItem);
        }
    }

    // Generate span of the idem in radialView
    if (m_viewFlags & ShowMirroredRadialView)
        m_spanOf.insert(item, ItemSpan(startAngle, spanAngle));

    // Draw all ring elements
    qreal spanDelta;
    qreal nLeafs = m_mapper->RingElementLeafs().value(node);

    foreach(QString child, m_mapper->RingElements().values(node)) {
        spanDelta = ((m_mapper->RingElementLeafs().value(child)/nLeafs)*(spanAngle));
        makeInnerTree(child, startAngle, spanDelta, item);
        startAngle += spanDelta;
    }

    // Calculate node bottom center point for self-clones
    qreal scale = (m_maxOuterRadius - ((level-1) * m_outerLevelSpace)) / radius - 0.01;
    m_centerPoint.insert(item, itemCenter * scale);

    // The endpoint for edges (touches inner circle)
    // if (m_childrenOf.values(item).size() == 0) {
    if (!m_flattenRing)  scale = (m_maxOuterRadius - (level * m_outerLevelSpace)) / radius;
    else                scale = (m_maxInnerRadius + m_innerOuterSpace) / radius;
    m_edgeEndPoint.insert(item, itemCenter * scale);
    // }
}

void RadialGraphicsTreeView::makeOuterTree(GraphicsTreeNode *originalItem)
{
    qreal level     = m_mapper->RingElementDepth()[originalItem->toolTip()];
    qreal radius    = m_maxOuterRadius - (level * m_outerLevelSpace);

    foreach(GraphicsTreeNode *child, m_childrenOf.values(originalItem))
    {
        qreal ringFactor = 1-((radius-m_outerLevelSpace)/radius);
        if (m_flattenRing && m_childrenOf.values(child).size() == 0) {
            ringFactor *= m_maxLevel - level;
        }

        ItemSpan span = m_spanOf[child];

        GraphicsEllipseRingItem *item =
                    new GraphicsEllipseRingItem( this,
                                                child->toolTip(),
                                                radius,
                                                ((span.first*180)/PI)*16,
                                                ((span.second*180+0.1)/PI)*16,
                                                ringFactor);

        connect(item, SIGNAL(sigDoubleClicked(QString)), m_mapper, SLOT(setRootNode(QString)));

//        qDebug() << span.second - span.first;

        if (m_mapper->RingElementInvDepth()[child->toolTip()] > 0) {
            item->setPen(QPen(Qt::black, 0.08 * (m_maxLevel - level)));
        }

        scene()->addItem(item);
        m_ringItems.insert(child->toolTip(), item);

        makeOuterTree(child);
    }
}

//*** RENDERING OF EDGES *******************************************************
QPainterPath RadialGraphicsTreeView::makeEdgePath(GraphicsTreeNode *source, GraphicsTreeNode *target)
{
    // If selfclone, make simple glyph
    if (source == target) {
        QPainterPath output;
        output.moveTo(m_centerPoint[target]);
        output.lineTo(m_centerPoint[target] * 0.97);
        return output;
    }

    // Generate the paths from root
    QList<QGraphicsItem*> rToS, rToT;

    for (GraphicsTreeNode* i = source; i != 0; i = m_parentOf.value(i)) rToS.prepend(i);
    for (GraphicsTreeNode* i = target; i != 0; i = m_parentOf.value(i)) rToT.prepend(i);

    // Create LCA paths by removing common stuff from the top of both,
    // assuring that resulting amount of points remains more than 3
    while (rToS.size() >= 2 && rToT.size() >= 2 && rToS[0] == rToT[0] && rToS[1] == rToT[1]) {
        rToS.removeFirst();
        rToT.removeFirst();
    }
    if (rToS[0] == rToT[0]) {
        rToS.removeFirst();
        if (m_edgePathXOR) rToT.removeFirst();  // Remove common parent
    }

    // Generate list op points to traverse
    QList<QPointF> points;
    foreach(QGraphicsItem* i, rToT) points.prepend(i->pos());
    foreach(QGraphicsItem* i, rToS) points.append(i->pos());

    points.insert(0,                m_edgeEndPoint.value(target));
    points.insert(points.size(),    m_edgeEndPoint.value(source));

    // Make path and set its origin
    return makeSpline(points);
}

QPainterPath RadialGraphicsTreeView::makeSpline(QList<QPointF> &P) {
    QPainterPath path;

    // Generate control points?
    if (m_showControlPoints) {
        foreach(QPointF p, P) {
            m_edgeControlPoint << scene()->addEllipse(p.x()-0.5, p.y()-0.5, 1, 1, QColor("red"));
        }
    }

    // Generate Holten's control points
    QPointF P_t = P.last() - P.first();
    int N       = P.size();
    for (qreal i = 1; i < N-1; ++i) {
        // Replace P[i] by calculated P_i'
        P[i] =  m_edgeHoltenBeta * P[i] + (1.0-m_edgeHoltenBeta) * (P[0] + (i/(N-1)) * P_t);
    }

    // Interpolate splines
    QList<QPointF> pBuff;
    QPointF fst = P.first();
    QPointF lst = P.last();
    qreal   eps = m_edgeRelaxation;
    qreal   ieps= 1.0 - eps;
    for (int j=0; j < 4; ++j) {
        pBuff.clear();
        pBuff << fst;
        for (int i=1; i < P.size()-1; ++i) {
            pBuff << P[i] * eps + P[i-1] * ieps;
            pBuff << P[i] * eps + P[i+1] * ieps;
        }
        pBuff << lst;
        P.clear();
        P << pBuff;
    }

    // Make the path out of Qt line segments
    path.moveTo(P.takeFirst()); // Starting position
    foreach(QPointF p, P) path.lineTo(p);

    return path;
}
