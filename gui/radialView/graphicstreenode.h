#ifndef GRAPHICSTREENODE_H
#define GRAPHICSTREENODE_H

#include <QtWidgets/QGraphicsEllipseItem>

class FileNode;
class ScopeNode;

class GraphicsTreeNode : public QGraphicsEllipseItem
{
public:
    GraphicsTreeNode(QString name, qreal radius = 1, QGraphicsItem *parent = 0);
    ~GraphicsTreeNode() {}

signals:
    
public slots:
    
};

#endif // GRAPHICSTREENODE_H
