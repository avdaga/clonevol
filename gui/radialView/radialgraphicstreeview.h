/********************************************************************************
 *   Copyright (C) 2012 by Sandro Andrade <sandroandrade@kde.org>               *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#ifndef RADIALGRAPHICSTREEVIEW_H
#define RADIALGRAPHICSTREEVIEW_H

#include <QtCore/QFlags>
#include <QMutex>
#include <QList>

#include "gui/radialView/igraphicscompoundtreeview.h"

/******************************************************************************
*   Forward declarations
*******************************************************************************/
class GraphicsEllipseRingItem;
class GraphicsTreeNode;
class Mapper;

/*! ****************************************************************************
 * \author Avdo Hanjalic
 * \author Sandro Andrade (original code)
 * \date 11.06.2013
 * \class RadialGraphicsTreeView
 * \brief The radial tree view widget
***************************************************************************** */
class RadialGraphicsTreeView : public IGraphicsCompoundTreeView
{
    Q_OBJECT

/*******************************************************************************
*   Enums
*******************************************************************************/
public:
    enum ViewFlag {
        ShowNormalRadialViewNode            = 0x1,
        ShowNormalRadialViewEdge            = 0x2,
        ShowNormalRadialViewLevelCircles    = 0x4,
        ShowMirroredRadialView              = 0x8,
        ShowAdjacencyModel                  = 0x10
    };
    Q_DECLARE_FLAGS(ViewFlags, ViewFlag)

/*******************************************************************************
*   Constructor & Destructor
*******************************************************************************/
public:
    RadialGraphicsTreeView(QWidget *parent = 0);
    virtual ~RadialGraphicsTreeView();

/*******************************************************************************
*   Getters
*******************************************************************************/
    long nEdges()   const           { return m_edges.count(); }
    long nNodes()   const           { return m_ringItems.count(); }

/*******************************************************************************
*   Slots (Setters)
*******************************************************************************/
public slots:
    void setBundlingStrength        (qreal beta);
    void setEdgeChop                (const bool edgeChop);
    void setEdgePathXOR             (const bool edgePathXOR);
    void setEdgeRelaxation          (qreal v);
    void setFlattenRing             (bool val);
    void setInterLevelSpacing       (qreal interLevelSpacing);
    void setInterSpanSpacing        (qreal interSpanSpacing);
    void setNodeBrush               (const QBrush &nodeBrush);
    void setNodePen                 (const QPen &nodePen);
    void setShowObstructedEdges     (const bool show);
    void setShowControlPoints       (bool val);
    void setViewFlag                (ViewFlag viewFlag, bool enabled = true);
    void setViewFlags               (ViewFlags viewFlags);

public slots:
    void setMapper                  (Mapper* m);

/*******************************************************************************
*   Methods
*******************************************************************************/
protected:
    virtual void mouseReleaseEvent  (QMouseEvent *event);
    virtual void paintEvent         (QPaintEvent *event);

private slots:
    void refreshAll();
    void refreshEdges();
    void refreshEdgeColors();
    void refreshTreeColors();

signals:
    void sigEdgeColorDone();
    void sigRenderingDone();

private:
    // Re-building visual components
    void makeEdges();
    void makeTrees();

    void makeLegend();  //! \todo: Implement Legend
    void makeTitle();
    void updateFigLabel();

    // Edge helper functions
    void makeInnerTree              (QString node,
                                     qreal              startAngle,
                                     qreal              spanAngle,
                                     GraphicsTreeNode   *parentItem = 0);
    void makeOuterTree              (GraphicsTreeNode   *originalItem);
    QPainterPath makeEdgePath       (GraphicsTreeNode* source, GraphicsTreeNode* target);
    QPainterPath makeSpline         (QList<QPointF> &toLca);

/*******************************************************************************
*   Attributes
*******************************************************************************/
protected:
    // Rendering
    bool        m_freezePaint;
    QMutex      m_processing;
    unsigned int m_processCalled;

    // View properties
    ViewFlags   m_viewFlags;

    // Radial layout properties
    bool        m_flattenRing;
    bool        m_showObstructedEdges;
    qreal       m_innerOuterSpace;
    qreal       m_innerLevelSpace;
    qreal       m_interSpanSpacing;
    qreal       m_outerLevelSpace;
    QBrush      m_nodeBrush;
    QPen        m_nodePen;

    // Clone (Edge) properties
    bool        m_edgeChop;
    qreal       m_edgeRelaxation;
    qreal       m_edgeHoltenBeta;
    bool        m_edgePathXOR;
    bool        m_showControlPoints;

private:
    Mapper*     m_mapper;
    int         m_maxLevel;
    qreal       m_maxInnerRadius;
    qreal       m_maxOuterRadius;

    QGraphicsSimpleTextItem*                                m_figLabel;

    typedef QPair<qreal, qreal> ItemSpan;
    QHash       <GraphicsTreeNode *, ItemSpan>              m_spanOf;
    QMultiHash  <GraphicsTreeNode *, GraphicsTreeNode *>    m_childrenOf;
    QHash       <GraphicsTreeNode *, GraphicsTreeNode *>    m_parentOf;
    QHash       <GraphicsTreeNode *, QPointF>               m_edgeEndPoint;
    QList       <QGraphicsEllipseItem*>                     m_edgeControlPoint;
    QHash       <GraphicsTreeNode *, QPointF>               m_centerPoint;

    // Visual (helper) objects
    QHash       <QString, QGraphicsPathItem *>              m_edges;
    QHash       <QString, GraphicsTreeNode *>               m_pathPoints;
    QHash       <QString, GraphicsEllipseRingItem *>        m_ringItems;
};

Q_DECLARE_OPERATORS_FOR_FLAGS(RadialGraphicsTreeView::ViewFlags)

#endif // RADIALGRAPHICSTREEVIEW_H
