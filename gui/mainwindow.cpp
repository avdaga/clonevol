#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QEventLoop>
#include <QFileDialog>
#include <QInputDialog>
#include <QMessageBox>
#include <QThread>

#include "datastore/datastore.h"
#include "datastore/revisionnode.h"
#include "datastore/scopenode.h"
#include "datastore/filenode.h"
#include "gui/exportdialog.h"
#include "gui/logbrowser.h"
#include "gui/mapper.h"
#include "mining/miner.h"
#include "util/workerthread.h"

#define PI 3.14159265358979

MainWindow* MainWindow::m_instance = 0;

/* ************************************************************************************************
 * Getters & Setters
 **************************************************************************************************/
MainWindow* MainWindow::Instance()
{
    if (m_instance == 0) {
        m_instance = new MainWindow();
    }

    return m_instance;
}

/* ************************************************************************************************
 * Constructor & Destructor
 **************************************************************************************************/
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow     (parent)
  , ui              (new Ui::MainWindow)
  , m_dataStore     (0)
  , m_mapper        (0)
  , m_mapperThread  (0)
  , m_miner         (0)
  , m_minerThread   (0)
  , m_viewFileTree  (true)
  , m_nodeFilter    (0)
  , m_exportDir     (".")
{
    ui->setupUi(this);
    setWindowTitle(getApplicationName());

    ui->stepLabel->hide();
    ui->stepProgressBar->hide();

    ui->legend->setColorMap(Mapper::Color_Structure);

    connect(ui->radialTreeView, SIGNAL(sigRenderingDone()),
            this,               SLOT(onRenderingDone()));

    initMiner();
}

MainWindow::~MainWindow()
{
    delete ui;
    delete m_mapper;
}

/* ************************************************************************************************
 * Private Functions
 **************************************************************************************************/
QString MainWindow::getApplicationName()
{
    return QApplication::applicationName() + " (" + QApplication::applicationVersion() + ")";
}

void MainWindow::initDataStore()
{
    if (m_dataStore == 0) {
        m_dataStore = new DataStore(ui->nameBox->text(), ui->urlBox->text());
    } else {
        m_dataStore->setName(ui->nameBox->text());
        m_dataStore->setUrl(ui->urlBox->text());
    }
}

void MainWindow::initMapper(QString fileName)
{
    if (m_mapper == 0) {
        //---------------- thread initialization --------------
        if (m_mapperThread != 0) { delete m_mapperThread; }
        m_mapperThread = new WorkerThread(this);
        m_mapperThread->start();

        m_mapper = new Mapper(fileName);
        m_mapper->moveToThread(m_mapperThread);

        connect(ui->maxRevSlider,   SIGNAL(valueChanged(int)),
                m_mapper,           SLOT(setMaxRevision(int)));
        connect(ui->minRevSlider,   SIGNAL(valueChanged(int)),
                m_mapper,           SLOT(setMinRevision(int)));

        ui->radialTreeView->setMapper(m_mapper);
    }
}

void MainWindow::initMiner()
{
    if (m_miner == 0) {
        initDataStore();

        m_miner = new Miner(m_dataStore);

        connect(m_miner,    SIGNAL(sigProgress(ProgressInfo)),
                this,       SLOT(onProgressInfo(ProgressInfo)));
        connect(m_miner,    SIGNAL(sigStepProgress(ProgressInfo)),
                this,       SLOT(onStepProgressInfo(ProgressInfo)));
        connect(this,       SIGNAL(sigMineLogs()),
                m_miner,    SLOT(getLogs()));
        connect(this,       SIGNAL(sigMineDetails(int,int)),
                m_miner,    SLOT(getDetails(int,int)));
    }

    if (m_minerThread == 0 || !m_minerThread->isRunning()) {
        //---------------- thread initialization --------------
        if (m_minerThread != 0) { delete m_minerThread; }
        m_minerThread = new WorkerThread(this);
        m_minerThread->start();
        m_miner->moveToThread(m_minerThread);
    }
}

void MainWindow::loadFile(QString fileName)
{
    if (m_dataStore == 0)    initDataStore();
    if (m_mapper == 0)       initMapper(fileName);

    m_mapper->reloadDatabase();

    m_dataStore->setName(m_mapper->ProjectName());
    m_dataStore->setUrl(m_mapper->ProjectUrl());

    setWindowTitle(m_mapper->ProjectName() + " - " + getApplicationName());

    // Sanity check
    if (m_mapper->Revisions().isEmpty()) { return; }

    //--------------- update interface ---------------------
    on_resetNodesFilterButton_clicked();

    on_maxRevSlider_valueChanged(ui->maxRevSlider->value());
    on_minRevSlider_valueChanged(ui->minRevSlider->value());

    makeRevisionGradient();

    // Update progress bar(s)
    ui->progressBar->setValue(100);
    ui->tabWidget->setEnabled(true);
    ui->colorMapBox->setEnabled(true);

    ui->maxRevSlider->setMinimum(0);
    ui->maxRevSlider->setMaximum(m_mapper->Revisions().count()-1);
    ui->minRevSlider->setMinimum(0);
    ui->minRevSlider->setMaximum(m_mapper->Revisions().count()-1);

    ui->nameBox->setText(m_mapper->ProjectName());
    ui->urlBox->setText(m_mapper->ProjectUrl());

    ui->revMinLabel->setText(QString::number(m_mapper->Revisions().first()));
    ui->revMaxLabel->setText(QString::number(m_mapper->Revisions().last()));

    ui->urlBox->setEnabled(false);
    ui->nameBox->setEnabled(false);
    ui->openFileButton->setEnabled(false);
    ui->startButton->setEnabled(false);
    ui->getRevisionsButton->setEnabled(true);
    ui->repositoryGroupBox->setEnabled(true);

    ui->maxRevSlider->setEnabled(true);
    ui->minRevSlider->setEnabled(true);



    // Total stats
//        ui->nRevisionsTotalLabel->setText(QString::number(m_mapper->Revisions().count()));
//        long long fTotal = 0, sTotal = 0;
//        foreach(RevisionNode *r, m_dataStore->Revisions()) {
//            fTotal += r->FileCache().count();
//            sTotal += r->ScopeCache().count();
//        }
//        ui->nFilesTotalLabel->setText(QString::number(fTotal));
//        ui->nScopesTotalLabel->setText(QString::number(sTotal));

    // Union stats
//        ui->nFilesUnionLabel->setText(QString::number(m_mapper->FileChildren().count()));
//        ui->nScopesUnionLabel->setText(QString::number(m_mapper->ScopeChildren().count()));
}

void MainWindow::makeRevisionGradient()
{
    if (m_mapper != 0 && !m_mapper->RevisionsAcq().isEmpty()) {
        qreal revCount  = m_mapper->RevisionsAcq().count();
        bool  lastOp    = m_mapper->RevisionsAcq().first();
        qreal curOpCnt  = 0;

        QList<qreal> opCnt;
        QList<qreal> ops;

        foreach(bool b, m_mapper->RevisionsAcq()) {
            if (b == lastOp) {
                ++curOpCnt;
            } else {
                // Save last
                opCnt << curOpCnt;
                ops   << lastOp;
                // Set to current
                curOpCnt = 1;
                lastOp = b;
            }
        }
        opCnt << curOpCnt;
        ops   << lastOp;

        qreal currPos = 0;
        QString green = " rgba(0, 192, 0, 96)", trans = " rgba(0, 0, 0, 0)";
        QString gradient;
        gradient += "background-color: qlineargradient(spread:pad,";
        gradient += "x1:0, y1:0, x2:1, y2:0,";
        gradient += "stop: 0 " + trans + ", stop: 0.015 " + trans + ",";
        for (int i=0; i < ops.count(); ++i) {
            gradient += "stop:" + QString::number(0.015 + (currPos / revCount + 0.00001) * 0.968) + (ops[i] ? green : trans) + ",";
            currPos  += opCnt[i];
            gradient += "stop:" + QString::number(0.015 + (currPos / revCount) * 0.968) + (ops[i] ? green : trans);
            /*if (i < ops.count() -1) */gradient += ",";
        }
        gradient += "stop: 0.9831 " + trans + ", stop: 1 " + trans;
        gradient += ");";

        ui->maxRevSlider->setStyleSheet(gradient);
        ui->maxRevSlider->setTickInterval(revCount);
        ui->minRevSlider->setStyleSheet(gradient);
        ui->minRevSlider->setTickInterval(revCount);
    }
}

void MainWindow::exportSeq()
{
    QString fileName = QString::number(m_mapper->MinRevision()) +
            + " - " + QString::number(m_mapper->MaxRevision()) + ".png";
    qDebug() << "Exporting " << fileName;
    ui->radialTreeView->exportPng(m_exportDir + "/" + fileName);

    if (m_exportCurRevMax < m_exportConfig.maxRev) {
        if (m_exportConfig.slide) {
            m_exportCurRevMax += 1;
            m_exportCurRevMin += m_exportConfig.stretch ? 0 : 1;
        } else {
            int dist = qMin(m_exportConfig.step,
                            m_exportConfig.maxRev - m_exportCurRevMax);
            m_exportCurRevMax += dist;
            m_exportCurRevMin += m_exportConfig.stretch ? 0 : m_exportConfig.step;
        }

        emit sigMinMaxRevision(m_exportCurRevMin,m_exportCurRevMax);

        ui->usrMaxRevLabel->setText(QString::number(m_mapper->MaxRevision()));
        ui->usrMinRevLabel->setText(QString::number(m_mapper->MinRevision()));
    } else {
        disconnect (ui->radialTreeView, SIGNAL(sigEdgeColorDone()),
                    this,               SLOT(exportSeq()));
        disconnect (this,               SIGNAL(sigMinMaxRevision(int,int)),
                    m_mapper,           SLOT(setMinMaxRevision(int,int)));

        emit sigMinMaxRevision(m_exportConfig.minRev,m_exportConfig.maxRev);
        ui->usrMaxRevLabel->setText(QString::number(m_mapper->MaxRevision()));
        ui->usrMinRevLabel->setText(QString::number(m_mapper->MinRevision()));

        ui->userInput->setEnabled(true);
    }
}

/* ************************************************************************************************
 * Private Slots
 **************************************************************************************************/
void MainWindow::onProgressInfo(ProgressInfo info)
{
    if (info.percent >= 0)   ui->progressBar->setValue(info.percent);
    if (info.percent == 100) {
        ui->stepProgressBar->hide();
        ui->stepLabel->hide();
    }

    if (info.message != "")  ui->progressBox->setTitle(info.message);

    if (info.status == true && info.percent == 100)  {
        loadFile(m_miner->dataStore()->Name() + ".sqlite");
        ui->getRevisionsButton->setEnabled(true);
        ui->stepLabel->hide();
        ui->stepProgressBar->hide();
    } else if (info.status == false && info.percent == -1) {
        ui->repositoryGroupBox->setEnabled(true);
    }
}

void MainWindow::onRenderingDone()
{
    if (m_mapper == 0) return;

    ui->nEdgesLabel->setText(QString::number(ui->radialTreeView->nEdges()));
    ui->nNodesLabel->setText(QString::number(ui->radialTreeView->nNodes()));

    // Union stats
    ui->nFilesUnionLabel->setText(QString::number(m_mapper->FileCount()));
    ui->nScopesUnionLabel->setText(QString::number(m_mapper->ScopeCount()));

    // Total stats
    ui->nFilesTotalLabel->setText(QString::number(m_mapper->FileCountTot()));
    ui->nScopesTotalLabel->setText(QString::number(m_mapper->ScopeCountTot()));
    ui->nRevisionsTotalLabel->setText(QString::number(m_mapper->Revisions().count()));
}

void MainWindow::onStepProgressInfo(ProgressInfo info)
{
    ui->stepLabel->show();
    ui->stepProgressBar->show();

    if (info.percent >= 0)   ui->stepProgressBar->setValue(info.percent);
    if (info.message != "")  ui->progressBox->setTitle(info.message);
}

//*** BUTTONS *************************************************************************************/
void MainWindow::on_startButton_clicked()
{
    initDataStore();

    if (m_miner == 0) {
        initMiner();
        emit(ui->startButton->clicked());
        return;
    }

    ui->repositoryGroupBox->setEnabled(false);

    emit sigMineLogs();
}

void MainWindow::on_getRevisionsButton_clicked()
{
    initMiner();

    ui->getRevisionsButton->setEnabled(false);
    ui->cancelRevisionsButton->setEnabled(true);

    emit sigMineDetails(ui->usrMinRevLabel->text().toInt(), ui->usrMaxRevLabel->text().toInt());
}


void MainWindow::on_openFileButton_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this,
                            "Open Project", ".", "SQLITE (*.sqlite)");

    if (m_miner == 0)    initMiner();
    if (fileName != "")  loadFile(fileName);
}

void MainWindow::on_ExportSvgButton_clicked()
{
    ui->radialTreeView->exportSvgDialog();
}

void MainWindow::on_levelUpButton_clicked()
{
    if (m_mapper != 0)   m_mapper->setRootNodeLevelUp();
}

//*** SLIDERS *************************************************************************************/

void MainWindow::on_maxRevSlider_valueChanged(int value)
{
    // Sanity check
    if (value < 0 || value > m_mapper->Revisions().count() - 1) { return; }

    int rev = m_mapper->Revisions()[value];
    ui->usrMaxRevLabel->setText(QString::number(rev));

    if (value < ui->minRevSlider->value()) ui->minRevSlider->setValue(value);
}

void MainWindow::on_minRevSlider_valueChanged(int value)
{
    // Sanity check
    if (value < 0 || value > m_mapper->Revisions().count() - 1) { return; }

    int rev = m_mapper->Revisions()[value];
    ui->usrMinRevLabel->setText(QString::number(rev));

    if (value > ui->maxRevSlider->value()) ui->maxRevSlider->setValue(value);
}

void MainWindow::on_bundlingStrengthSlider_valueChanged(int value) {
    ui->radialTreeView->setBundlingStrength((qreal)value / 100);
    ui->bundlingStrengthLabel->setText(QString::number((qreal)value / 100));
}

void MainWindow::on_splineFragmentSlider_valueChanged(int value)
{
    qreal v = value; v /= 100;
    ui->radialTreeView->setEdgeRelaxation(v);
    ui->splineFragmentsLabel->setText(QString::number(v));
}

//*** CHECKBOXES **********************************************************************************/

void MainWindow::on_edgesCheck_toggled(bool checked)
{
    ui->radialTreeView->setViewFlag(RadialGraphicsTreeView::ShowNormalRadialViewEdge, checked);
}

void MainWindow::on_nodesCheck_toggled(bool checked)
{
    ui->radialTreeView->setViewFlag(RadialGraphicsTreeView::ShowNormalRadialViewNode, checked);
}

void MainWindow::on_flattenRingCheck_toggled(bool checked)
{
    ui->radialTreeView->setFlattenRing(checked);
}

void MainWindow::on_oldIntraClonesCheck_toggled(bool checked)
{
    Q_UNUSED(checked);

    m_mapper->setShowClones(ui->oldIntraClonesCheck->isChecked(),
                            ui->intraClonesCheck->isChecked(),
                            ui->InterClonesCheck->isChecked());
}

void MainWindow::on_intraClonesCheck_toggled(bool checked)
{
    if (!checked && !ui->InterClonesCheck->isChecked()) {
        ui->InterClonesCheck->setChecked(true);
    } else {
        m_mapper->setShowClones(ui->oldIntraClonesCheck->isChecked(),
                                ui->intraClonesCheck->isChecked(),
                                ui->InterClonesCheck->isChecked());
    }
}

void MainWindow::on_InterClonesCheck_toggled(bool checked)
{
    if (!checked && !ui->intraClonesCheck->isChecked()) {
        ui->intraClonesCheck->setChecked(true);
    } else {
        m_mapper->setShowClones(ui->oldIntraClonesCheck->isChecked(),
                                ui->intraClonesCheck->isChecked(),
                                ui->InterClonesCheck->isChecked());
    }
}

void MainWindow::on_structureColorButton_toggled(bool checked)
{
    if (checked) {
        m_mapper->setColorMap(Mapper::Color_Structure);
        ui->legend->setColorMap(Mapper::Color_Structure);
        ui->edgeMetricCombo->setEnabled(true);
    }
}

void MainWindow::on_diffColorButton_toggled(bool checked)
{
    if (checked) {
        m_mapper->setColorMap(Mapper::Color_Differences);
        ui->legend->setColorMap(Mapper::Color_Differences);
        ui->edgeMetricCombo->setEnabled(true);
    }

    ui->InterClonesCheck->setEnabled(checked);
    ui->intraClonesCheck->setEnabled(checked);
}

void MainWindow::on_activityColorButton_toggled(bool checked)
{
    if (checked) {
        m_mapper->setColorMap(Mapper::Color_Activity);
        ui->legend->setColorMap(Mapper::Color_Activity);
        ui->edgeMetricCombo->setEnabled(false);
    }
}

void MainWindow::on_setNodesFilterButton_clicked()
{
    m_nodeFilter = TYPE_DEFAULT;
    if (ui->filterAttribCheck->isChecked())      m_nodeFilter |= TYPE_ATTRIBUTE;
    if (ui->filterClassCheck->isChecked())       m_nodeFilter |= TYPE_CLASS;
    if (ui->filterDefineCheck->isChecked())      m_nodeFilter |= TYPE_DEFINE;
    if (ui->filterEnumCheck->isChecked())        m_nodeFilter |= TYPE_ENUM;
    if (ui->filterFuncCheck->isChecked())        m_nodeFilter |= TYPE_FUNCTION;
    if (ui->filterNameSpaceCheck->isChecked())   m_nodeFilter |= TYPE_NAMESPACE;
    if (ui->filterOtherCheck->isChecked())       m_nodeFilter |= TYPE_LOGIC;
    if (ui->filterSignalCheck->isChecked())      m_nodeFilter |= TYPE_SIGNAL;
    if (ui->filterSlotCheck->isChecked())        m_nodeFilter |= TYPE_SLOT;

    m_mapper->setNodeFilter(m_nodeFilter);
}

void MainWindow::on_resetNodesFilterButton_clicked()
{
    m_nodeFilter = m_mapper->NodeFilter();
    ui->filterAttribCheck->setChecked           (m_nodeFilter & TYPE_ATTRIBUTE);
    ui->filterClassCheck->setChecked            (m_nodeFilter & TYPE_CLASS);
    ui->filterDefineCheck->setChecked           (m_nodeFilter & TYPE_DEFINE);
    ui->filterEnumCheck->setChecked             (m_nodeFilter & TYPE_ENUM);
    ui->filterFuncCheck->setChecked             (m_nodeFilter & TYPE_FUNCTION);
    ui->filterNameSpaceCheck->setChecked        (m_nodeFilter & TYPE_NAMESPACE);
    ui->filterOtherCheck->setChecked            (m_nodeFilter & TYPE_LOGIC);
    ui->filterSignalCheck->setChecked           (m_nodeFilter & TYPE_SIGNAL);
    ui->filterSlotCheck->setChecked             (m_nodeFilter & TYPE_SLOT);
}

void MainWindow::on_setNodesFilterAllButton_clicked()
{
    bool value = m_nodeFilter == TYPE_ALL ? false : true;

    ui->filterAttribCheck->setChecked           (value);
    ui->filterClassCheck->setChecked            (value);
    ui->filterDefineCheck->setChecked           (value);
    ui->filterEnumCheck->setChecked             (value);
    ui->filterFuncCheck->setChecked             (value);
    ui->filterNameSpaceCheck->setChecked        (value);
    ui->filterOtherCheck->setChecked            (value);
    ui->filterSignalCheck->setChecked           (value);
    ui->filterSlotCheck->setChecked             (value);

    m_nodeFilter = value ? TYPE_ALL : TYPE_DEFAULT;
}

void MainWindow::on_checkBox_toggled(bool checked)
{
    ui->radialTreeView->setShowObstructedEdges(checked);
}

void MainWindow::on_edgePathXOR_toggled(bool checked)
{
    ui->radialTreeView->setEdgePathXOR(checked);
}

void MainWindow::on_conrolPointsCheck_toggled(bool checked)
{
    ui->radialTreeView->setShowControlPoints(checked);
}
void MainWindow::on_logButton_clicked()
{
    LogBrowser::Instance()->show();
}

void MainWindow::on_cancelRevisionsButton_clicked()
{
    if (m_minerThread->isRunning()) {
        if (QMessageBox::critical(this,
            "Stop Mining?",
            "Are you sure that you want to stop the Miner?\n"
            "You will keep the downloaded files, but lose all other progress...",
            QMessageBox::Yes|QMessageBox::No) == QMessageBox::Yes)
        {
            m_dataStore->setRunning(false);
            onProgressInfo({false,100,"Mining operation canceled!"});
            ui->cancelRevisionsButton->setEnabled(false);
            ui->getRevisionsButton->setEnabled(true);
        }
    }
}

void MainWindow::on_ExportPngButton_clicked()
{
    ui->radialTreeView->exportPngDialog();
}

void MainWindow::on_ExportSequence_clicked()
{
    if (m_mapper == 0 || m_mapper->MinRevision() == m_mapper->MaxRevision()) {
        QMessageBox::information(this, "Export sequence",
                                 "First load a project and select a RANGE of revisions to export!");
        return;
    }

    bool ok             = false;
    int  maxWindowSize  = ui->maxRevSlider->value()-ui->minRevSlider->value();
    m_exportConfig      = ExportDialog::getConfig(maxWindowSize, &ok);
    if (!ok) return;

    // Get export dir
    QFileDialog dialog(this);
    m_exportDir     = dialog.getExistingDirectory(this, "Export to Directory", m_exportDir);

    // CONFIRM WHETHER TO START EXPORTING
    ok = QMessageBox::question(this,"Export sequence",
                              "This process might take a long time and cannot be aborted... Continue?",
                              QMessageBox::Yes, QMessageBox::No)
                    ==  QMessageBox::Yes;

    if (!ok) return;

    // START EXPORT
    m_exportConfig.minRev = ui->minRevSlider->value();
    m_exportConfig.maxRev = ui->maxRevSlider->value();
    m_exportCurRevMin   = m_exportConfig.minRev;
    m_exportCurRevMax   = qMin(m_exportConfig.minRev + m_exportConfig.step, m_exportConfig.maxRev);

    connect(ui->radialTreeView, SIGNAL(sigEdgeColorDone()),
            this,               SLOT(exportSeq()));
    connect(this,               SIGNAL(sigMinMaxRevision(int,int)),
            m_mapper,           SLOT(setMinMaxRevision(int,int)));

    m_mapper->setMinMaxRevision(m_exportCurRevMin, m_exportCurRevMax);

    ui->usrMaxRevLabel->setText(QString::number(m_mapper->MaxRevision()));
    ui->usrMinRevLabel->setText(QString::number(m_mapper->MinRevision()));

    ui->userInput->setEnabled(false);
}

void MainWindow::on_edgeMetricCombo_currentIndexChanged(const QString &arg1)
{
    Mapper::EdgeMetric metric = Mapper::Metric_None;

    if      (arg1 == "None")         metric = Mapper::Metric_None;
    else if (arg1 == "Clone Age")    metric = Mapper::Metric_Age;
    else if (arg1 == "Clone Size")   metric = Mapper::Metric_Size;

    qDebug() << "Setting metric to " << arg1;

    m_mapper->setEdgeMetric(metric);
}
