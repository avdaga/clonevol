#include <QApplication>

#include "mapper.h"

/* ************************************************************************************************
 * Public Slots
 **************************************************************************************************/
void Mapper::setColorMap(ColorMap c)
{
    unsigned int myTime = ++m_processCalled;
    QApplication::processEvents(); // process remaining slot calls (events) until event queue is empty

    /* Prevent Queued generation */
    if (m_processCalled > myTime || m_colorMap == c)
        return; // If the mapping is newer or same, return

    m_processing.lock();
    m_colorMap = c;
    makeRingColorMap();
    makeEdgeColorMap();
    m_processing.unlock();
}

void Mapper::setEdgeMetric(EdgeMetric m)
{
    unsigned int myTime = ++m_processCalled;
    QApplication::processEvents(); // process remaining slot calls (events) until event queue is empty

    /* Prevent Queued generation */
    if (m_processCalled > myTime || m_edgeMetric == m)
        return; // If the mapping is newer or same, return

    m_processing.lock();
    m_edgeMetric = m;
    makeEdgeColorMap();
    m_processing.unlock();
}

void Mapper::setMaxRevision(int n)
{
    // Sanity check
    if (n < 0 || n > m_revisions.count() - 1) { return; }

    unsigned int myTime = ++m_processCalled;
    QApplication::processEvents(); // process remaining slot calls (events) until event queue is empty

    /* Prevent Queued generation */
    if (m_processCalled > myTime || m_maxRevision == m_revisions[n])
        return; // If the mapping is newer or same, return

    m_processing.lock();
    m_maxRevision = m_revisions[n];
    if (m_maxRevision < m_minRevision) m_minRevision = m_maxRevision;

    makeRingColorMap();
    makeEdgeColorMap();
    m_processing.unlock();
}

void Mapper::setMinRevision(int n)
{
    // Sanity check
    if (n < 0 || n > m_revisions.count() - 1) { return; }

    unsigned int myTime = ++m_processCalled;
    QApplication::processEvents(); // process remaining slot calls (events) until event queue is empty

    /* Prevent Queued generation */
    if (m_processCalled > myTime || m_minRevision == m_revisions[n])
        return; // If the mapping is newer or same, return

    /* Lock data and process request */
    m_processing.lock();
    m_minRevision = m_revisions[n];
    if (m_maxRevision < m_minRevision) m_maxRevision = m_minRevision;

    makeRingColorMap();
    makeEdgeColorMap();
    m_processing.unlock();
}

void Mapper::setMinMaxRevision(int n, int m)
{
    unsigned int myTime = ++m_processCalled;
    QApplication::processEvents(); // process remaining slot calls (events) until event queue is empty

    /* Prevent Queued generation */
    if (m_processCalled > myTime) return;

    m_processing.lock();
    m_minRevision = m_revisions[n];
    m_maxRevision = m_revisions[m];

    makeRingColorMap();
    makeEdgeColorMap();
    m_processing.unlock();
}

void Mapper::setNodeFilter(unsigned int filter)
{
    unsigned int myTime = ++m_processCalled;
    QApplication::processEvents(); // process remaining slot calls (events) until event queue is empty

    /* Prevent Queued generation */
    if (m_processCalled > myTime || m_nodeFilter == filter)
        return; // If the mapping is newer or same, return

    /* Lock data and process request */
    m_processing.lock();
    m_nodeFilter = filter;

    qDebug() << "Setting NodeFilter to " << filter;

    readFiles();
    readScopes();
    readClones();

    makeRingElements();
    makeRingColorMap();
    makeEdgeColorMap();
    m_processing.unlock();

    emit updateRing();
}

void Mapper::setRootNode(const QString &nodeName, bool force)
{
    // Do not descend into non-existing files (the ones that have no parent)
    if (!nodeName.startsWith('/')) return;

    qDebug() << "Setting root node " << nodeName;
    unsigned int myTime = ++m_processCalled;
    QApplication::processEvents(); // process remaining slot calls (events) until event queue is empty

    /* Prevent Queued generation */
    if ((m_processCalled > myTime || m_currentRoot == nodeName) && !force)
        return; // If the mapping is newer or same, return

    /* Lock data and process request */
    m_processing.lock();
    m_currentRoot = nodeName;

    readFiles();
    readScopes();
    readClones();

    makeRingElements();
    makeRingColorMap();
    makeEdgeColorMap();

    m_processing.unlock();

    emit updateRing();
}

void Mapper::setRootNodeLevelUp()
{
    setRootNode(m_fileParent[m_currentRoot]);
}

void Mapper::setShowClones(bool old, bool intra, bool inter)
{
    if (m_showOldIntraClones != old || m_showIntraClones != intra ||
            m_showInterClones != inter)
    {
        m_showOldIntraClones    = old;
        m_showIntraClones       = intra;
        m_showInterClones       = inter;
        makeEdgeColorMap();
    }
}
