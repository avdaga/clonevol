#include "exportdialog.h"
#include "ui_exportdialog.h"

ExportDialog *ExportDialog::m_instance = 0;

/* ************************************************************************************************
 * Public Functions
 **************************************************************************************************/
ExportConfig ExportDialog::getConfig(int max, bool *ok)
{
    if (m_instance == 0) m_instance = new ExportDialog();

    m_instance->ui->spinBox->setMinimum(1);
    m_instance->ui->spinBox->setMaximum(max);
    m_instance->exec();

    *ok = m_instance->result() == ExportDialog::Accepted;

    return m_instance->m_config;
}

/* ************************************************************************************************
 * Constructor & Destructor
 **************************************************************************************************/
ExportDialog::ExportDialog(QWidget *parent) :
    QDialog (parent)
  , ui      (new Ui::ExportDialog)
{
    m_config = {0,0,1,false,true};
    ui->setupUi(this);
//    this->setFixedSize(this->width(),this->height());
}

ExportDialog::~ExportDialog()
{
    delete ui;
}

/* ************************************************************************************************
 * Private Slots
 **************************************************************************************************/
void ExportDialog::on_spinBox_valueChanged(int arg1)
{
    m_config.step = arg1;
}

void ExportDialog::on_radioButton_7_toggled(bool checked)
{
    m_config.stretch = checked;
}

void ExportDialog::on_radioButton_9_toggled(bool checked)
{
    m_config.slide = checked;
}
