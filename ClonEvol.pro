#***************************************************************************************************
# Dependencies
#***************************************************************************************************
# Version-independent modules
CONFIG     += c++11
QT         += core widgets xml svg sql

# Version-dependent modules
equals(QT_MAJOR_VERSION, 6) { QT += openglwidgets }
equals(QT_MAJOR_VERSION, 5) { QT += opengl }

#***************************************************************************************************
# Output
#***************************************************************************************************
# Artefacts
OBJECTS_DIR = obj
MOC_DIR     = moc
UI_DIR      = ui

# Target proper build
TEMPLATE    = app
DESTDIR     = bin

CONFIG(debug,   debug|release) { TARGET = ClonEvolD }
CONFIG(release, debug|release) { TARGET = ClonEvol }

#***************************************************************************************************
# Sources
#***************************************************************************************************
SOURCES += \
    datastore/codeclone.cpp \
    datastore/datastore.cpp \
    datastore/filenode.cpp \
    datastore/revisionnode.cpp \
    datastore/scopeclone.cpp \
    datastore/scopenode.cpp \
    gui/exportdialog.cpp \
    gui/legend.cpp \
    gui/logbrowser.cpp \
    gui/mainwindow.cpp \
    gui/mapper.cpp \
    gui/mapper_slots.cpp \
    gui/radialView/graphicsellipseringitem.cpp \
    gui/radialView/graphicstreenode.cpp \
    gui/radialView/igraphicscompoundtreeview.cpp \
    gui/radialView/radialgraphicstreeview.cpp \
    gui/radialView/radialgraphicstreeview_getset.cpp \
    main.cpp \
    mining/cloneextractor.cpp \
    mining/databasewriter.cpp \
    mining/miner.cpp \
    mining/repositorydownloader.cpp \
    mining/scopeextractor.cpp \
    util/logpipe.cpp

HEADERS += \
    datastore/codeclone.h \
    datastore/datastore.h \
    datastore/filenode.h \
    datastore/nodetype.h \
    datastore/revisionnode.h \
    datastore/scopeclone.h \
    datastore/scopenode.h \
    gui/exportdialog.h \
    gui/legend.h \
    gui/logbrowser.h \
    gui/mainwindow.h \
    gui/mapper.h \
    gui/radialView/graphicsellipseringitem.h \
    gui/radialView/graphicstreenode.h \
    gui/radialView/igraphicscompoundtreeview.h \
    gui/radialView/radialgraphicstreeview.h \
    mining/cloneextractor.h \
    mining/databasewriter.h \
    mining/miner.h \
    mining/progressinfo.h \
    mining/repositorydownloader.h \
    mining/scopeextractor.h \
    util/logpipe.h \
    util/workerthread.h

FORMS += \
    gui/exportdialog.ui \
    gui/legend.ui \
    gui/logbrowser.ui \
    gui/mainwindow.ui

RESOURCES += \
    resources/resources.qrc

RC_FILE = resources/clonevol.rc

DISTFILES += \
    README.md \
    resources/doxyfile \
    resources/simian.conf
