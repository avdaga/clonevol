#ifndef REVISIONNODE_H
#define REVISIONNODE_H

#include <QHash>

class DataStore;
class CodeClone;
class FileNode;
class ScopeNode;
class ScopeClone;

/*! ***********************************************************************************************
 * \author  Avdo Hanjalic
 * \date    27.03.2013
 * \class   RevisionNode
 *
 * \brief   Root node of a revision, child of DataStore
 **************************************************************************************************/
class RevisionNode
{
public:
    /* ****************************************************************************************
     * Typedefs
     ******************************************************************************************/
    typedef QHash<QString, ScopeNode*>  ScopeHash;
    typedef QHash<QString, ScopeClone*> ScopeCloneHash;
    typedef QHash<QString, FileNode*>   FileHash;

    /* ****************************************************************************************
     * Constructor & Destructor
     ******************************************************************************************/
    RevisionNode(DataStore *ds, int revNo);

    /* ****************************************************************************************
     * Getters & Setters
     ******************************************************************************************/
    int                 Id() const                                  { return m_id; }
    bool                IsAcquired() const                          { return m_isAcquired; }
    QString             Path() const;
    RevisionNode        *PrevRevision();
    FileNode            *RootFile() const                           { return m_rootFile; }
    ScopeNode           *RootScope() const                          { return m_rootScope; }

    //! \warning: DO NOT USE DURING CREATION OF REVISION CONTENTS
    const ScopeHash     &DeletedScopes()                            { return m_deletedScopes; }
    const ScopeCloneHash &DeletedScopeClones()                      { return m_deletedScopeClones; }
    const FileHash      &FileCache();
    const ScopeHash     &ScopeCache();
    const ScopeCloneHash &IntraCloneCache();
    const ScopeCloneHash &InterCloneCache();

    QString             toString() const;

    /* ****************************************************************************************
     * Public Functions
     ******************************************************************************************/
    void                addDeletedScope     (ScopeNode* scp);
    void                addDeletedScopeClone(ScopeClone* sc);

    void                invalidateFileCache()                       { m_fileCacheDirty  = true; }
    void                invalidateScopeCache()                      { m_scopeCacheDirty = true; }
    void                invalidateIntraCloneCache()                 { m_intraCloneCacheDirty = true; }
    void                invalidateInterCloneCache()                 { m_interCloneCacheDirty = true; }
    void                setAcquired()                               { m_isAcquired = true; }

    void                print();

private:
    /* ****************************************************************************************
     * Attrributes
     ******************************************************************************************/
    DataStore           *m_dataStore;
    int                 m_id;
    bool                m_isAcquired;

    FileNode            *m_rootFile;                                //!< Root of revision's FileNodes
    ScopeNode           *m_rootScope;                               //!< Root of revision's ScopeNodes
    RevisionNode        *m_prevRev;

    bool                m_fileCacheDirty;
    bool                m_scopeCacheDirty;
    bool                m_intraCloneCacheDirty;
    bool                m_interCloneCacheDirty;

    ScopeHash           m_deletedScopes;
    ScopeCloneHash      m_deletedScopeClones;
    FileHash            m_fileCache;
    ScopeHash           m_scopeCache;
    ScopeCloneHash      m_intraCloneCache;
    ScopeCloneHash      m_interCloneCache;
};

#endif // REVISIONNODE_H
