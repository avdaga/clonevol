#ifndef FILENODE_H
#define FILENODE_H

#include <QMap>

#include "datastore/nodetype.h"

class CodeClone;
class RevisionNode;
class ScopeNode;

/*! ***********************************************************************************************
 * \author  Avdo Hanjalic
 * \date    18.12.2012
 * \class   FileNode
 *
 * \brief   Representation of a node in the file-tree
 **************************************************************************************************/
class FileNode
{
public:
    /* ****************************************************************************************
     * Typedefs
     ******************************************************************************************/
    typedef QList<CodeClone *>          CloneList;
    typedef QMap<QString, FileNode *>   FileMap;
    typedef QMap<QString, ScopeNode *>  ScopeMap;
    typedef QList<ScopeNode *>          ScopeList;

    /* ****************************************************************************************
     * Constructor & Destructor
     ******************************************************************************************/
    FileNode(RevisionNode* rev, FileNode* parent, QString name, NodeType type);

    /* ****************************************************************************************
     * Getters & Setters
     ******************************************************************************************/
    const QString       &Name() const                               { return m_name; }
    char                Operation() const                           { return m_operation; }
    int                 RevisionId() const;
    NodeType            Type() const                                { return m_nodeType; }

    FileNode            *Parent() const                             { return m_parent; }
    RevisionNode        *Revision() const                           { return m_revision; }


    const FileMap       &Children() const                           { return m_children; }
    const ScopeMap      &DefScopes() const                          { return m_DefNodes; }
    const ScopeMap      &ImpScopes() const                          { return m_ImpNodes; }

    QString             toString() const;

    /* ****************************************************************************************
     * Public Functions
     ******************************************************************************************/
    void                addChild    (FileNode *subNode);
    void                addCodeClone(CodeClone *newClone);
    void                addDef      (ScopeNode *subNode);
    void                addImp      (ScopeNode *subNode);

    void                copy        (FileNode* source);

    FileNode            *getChild   (QString subPath);
    FileNode            *makeChild  (QString subPath, char operation = 0);

    void                printClones (int indent = 0);
    void                printNodes  (int indent = 0);

    ScopeList           scopesAt    (int startLine, int endLine);

    void                setOperation(char c) { m_operation = c; }

private:
    /* ****************************************************************************************
     * Private Functions
     ******************************************************************************************/
    void                makeScopeClones (CodeClone* srcCC);

    /* ****************************************************************************************
     * Attrributes
     ******************************************************************************************/
    // FileNode location stuff
    RevisionNode        *m_revision;
    FileNode            *m_parent;
    FileMap             m_children;

    // Properties
    QString             m_name;
    NodeType            m_nodeType;

    char                m_operation;                                //!< (Modified, Added, Deleted)

    // ScopeNode relations
    ScopeMap            m_DefNodes;                                 //!< Defined Scopes
    ScopeMap            m_ImpNodes;                                 //!< Implemented Scopes

    // Related CodeClones
    CloneList           m_codeClones;
};

#endif // FILENODE_H
