#ifndef SCOPECLONE_H
#define SCOPECLONE_H

#include <QList>

class CodeClone;
class ScopeNode;

/*! ***********************************************************************************************
 * \author  Avdo Hanjalic
 * \date    06.05.2013
 * \class   ScopeClone
 *
 * \brief   This class represents a match between two scope nodes
 **************************************************************************************************/
class ScopeClone
{
public:
    /* ****************************************************************************************
     * Constructor & Destructor
     ******************************************************************************************/
    ScopeClone(ScopeNode* m_source, ScopeNode* m_target);

public:
    /* ****************************************************************************************
     * Getters & Setters
     ******************************************************************************************/
    bool                IsIntra() const                             { return m_isIntra; }
    char                DriftType() const                           { return m_driftType; }
    char                IntraType() const                           { return m_intraType; }
    int                 NLines() const                              { return m_nLines; }

    ScopeNode           *Source() const                             { return m_source; }
    ScopeNode           *Target() const                             { return m_target; }

    QString             toString() const;
    QString             reverse() const;

    void                destroy();

    /* ****************************************************************************************
     * Public Functions
     ******************************************************************************************/
public:
    void                setDriftType   (char c)                     { m_driftType = c; }
    void                setIntraType   (char c)                     { m_intraType = c; }

    void                addCodeClone   (CodeClone* cClone);
    void                addLines       (int nLines);

    void                print          (int indent = 0);

private:
    /* ****************************************************************************************
     * Attrributes
     ******************************************************************************************/
    ScopeNode           *m_source;
    ScopeNode           *m_target;

    bool                m_isIntra;
    char                m_driftType;
    char                m_intraType;

    int                 m_nLines;

    QList<CodeClone*>   m_codeClones;                               //!< CodeClones that make this

    friend class ScopeNode;
};

#endif // SCOPECLONE_H
