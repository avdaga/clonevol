#ifndef CODECLONE_H
#define CODECLONE_H

#include <QList>

class DataStore;
class FileNode;
class ScopeNode;
class RevisionNode;

/*! ***********************************************************************************************
 * \author  Avdo Hanjalic
 * \date    06.05.2013
 * \class   CodeClone
 *
 * \brief   This class represents a match between two code blocks
 **************************************************************************************************/
class CodeClone
{
public:
    /* ****************************************************************************************
     * Typedefs
     ******************************************************************************************/
    typedef QList<CodeClone*>   CloneList;

    /* ****************************************************************************************
     * Constructor & Destructor
     ******************************************************************************************/
    CodeClone(FileNode* file, int start, int end, CloneList* m_cloneSet = 0);

    /* ****************************************************************************************
     * Getters & Setters
     ******************************************************************************************/
    CloneList           *CloneSet()                                 { return m_cloneSet; }
    FileNode            *File()                                     { return m_fileNode; }
    int                 EndLine()                                   { return m_endLine; }
    int                 StartLine()                                 { return m_startLine; }
    QList<ScopeNode*>   Scopes();

    /* ****************************************************************************************
     * Public Functions
     ******************************************************************************************/
    void                printClones    (int indent = 0);

private:
    /* ****************************************************************************************
     * Attrributes
     ******************************************************************************************/
    FileNode*           m_fileNode;
    int                 m_startLine;
    int                 m_endLine;
    CloneList           *m_cloneSet;                                //!< All members share same instance
};

#endif // CODECLONE_H
