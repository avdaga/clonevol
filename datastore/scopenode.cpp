#include "scopenode.h"

#include <QDebug>
#include <QStringList>

#include "datastore/codeclone.h"
#include "datastore/filenode.h"
#include "datastore/scopeclone.h"

/* ************************************************************************************************
 * Constructor & Destructor
 **************************************************************************************************/
ScopeNode::ScopeNode(ScopeNode* parent, QString name, NodeType type) :
    m_parent    (parent)
  , m_name      (name)
  , m_nodeType  (type)
  , m_operation (0)
  , m_defFile   (0)
  , m_impFile   (0)
{}

/* ************************************************************************************************
 * Public Functions
 **************************************************************************************************/
void ScopeNode::addChild(ScopeNode* subNode)
{
    m_children.insert(subNode->Name(), subNode);
}

void ScopeNode::copy(ScopeNode *source, FileNode *rootFile)
{
    this->m_name        = source->m_name;
    this->m_nodeType    = source->m_nodeType;

    ScopeNode *child;
    for (ScopeNode* srcChild : source->m_children) {
        child = getChild(srcChild->Name());

        // Check whether the child already exists
        if (child == 0) {
            child = new ScopeNode(this, srcChild->Name(), srcChild->Type());

            // Connect the related fileNodes
            if (srcChild->ImpFile() != 0) {
                child->setImpFile(rootFile->getChild(srcChild->ImpFile()->toString()),-1,-1);
                child->ImpFile()->addImp(child);
            }

            if (srcChild->DefFile() != 0) {
                child->setDefFile(rootFile->getChild(srcChild->DefFile()->toString()),-1);
                child->DefFile()->addDef(child);
            }

            this->addChild(child);
        }

        child->copy(srcChild, rootFile);
    }
}

ScopeNode* ScopeNode::getChild(QString path)
{
    //qDebug() << "Getting node: " << path;

    ScopeNode*   currentNode = this;
    QStringList subnodes    = path.split("::", Qt::SkipEmptyParts);
    if (!path.startsWith("::")) subnodes.removeFirst();

    // Can we traverse anything?
    if (subnodes.count() == 0 || m_children.count() == 0) return 0;

    // Traverse the path
    while(subnodes.count() > 0 && currentNode != 0) {
        currentNode = currentNode->m_children.value(subnodes.takeFirst(), 0);
    }

    return currentNode;
}

QList<ScopeNode*> ScopeNode::Children() const
{
    return QList<ScopeNode*>(m_children.values());
}

QList<ScopeClone*> ScopeNode::Clones() const
{
    QList<ScopeClone*> output;

    output << m_scopeClones.values();

    foreach(ScopeNode* child, m_children) {
        output << child->Clones();
    }

    return output;
}

QList<ScopeClone*> ScopeNode::IntraClones() const
{
    QList<ScopeClone*> output;

    foreach(ScopeClone* sc, m_scopeClones) {
        if (sc->IsIntra()) output << sc;
    }

    foreach(ScopeNode* child, m_children) {
        output << child->IntraClones();
    }

    return output;
}

QList<ScopeNode*> ScopeNode::PathFromRoot()
{
    QList<ScopeNode*>   output;
    ScopeNode          *prev = this;

    while(prev != 0) {
        output.prepend(prev);
        prev = prev->Parent();
    }

    return output;
}

void ScopeNode::setDefFile(FileNode *file, int line)
{
    m_defFile = file;
    m_defLine = line;
}

void ScopeNode::setImpFile(FileNode *file, int startLine, int endLine)
{
    m_impFile       = file;
    m_impStartLine  = startLine;
    m_impEndLine    = endLine;
}

ScopeNode* ScopeNode::makeChild(QString path)
{
    ScopeNode*   currentNode = this;
    ScopeNode*   nextNode    = NULL;

    QStringList subnodes    = path.split("::", Qt::SkipEmptyParts);
    if (!path.startsWith("::")) subnodes.removeFirst();

    // Traverse the path
    while(subnodes.count() > 0) {
        // SubPath exists?
        if (currentNode->m_children.contains(subnodes.first())) {
            currentNode = currentNode->m_children.value(subnodes.first());
        } else {
            nextNode    = new ScopeNode(currentNode, subnodes.first(), TYPE_LOGIC);

            currentNode->m_children.insert(subnodes.first(), nextNode);
            currentNode = nextNode;
        }

        subnodes.removeFirst();
    }

    return currentNode;
}

ScopeClone* ScopeNode::makeScopeClone(ScopeNode *source, CodeClone *cClone, int nLines)
{
    if (source == this) return 0; // Prevent Self-clones

    // Trace existing ScopeClone
    ScopeClone* sClone = m_scopeClones.value(source,0);

    // Create new clone if not already exists
    if (sClone == 0) {
        sClone = new ScopeClone(source, this);
        m_scopeClones.insert(source,sClone);
    }

    // Add the CodeClone if it exists
    if (cClone != 0) sClone->addCodeClone(cClone);

    sClone->addLines(nLines);

    return sClone;
}

void ScopeNode::removeScopeClone(ScopeClone *clone)
{
    m_scopeClones.remove(clone->Source());
}

bool ScopeNode::hasChild(QString subPath)
{
    return this->getChild(subPath) != 0;
}

QString ScopeNode::toString() const
{
    QString output = "::" + m_name;

    // Build name 'recursively'
    ScopeNode *curr = this->Parent();
    while(curr != 0 && curr->m_impFile == m_impFile) {
        output.prepend("::" + curr->m_name);
        curr = curr->Parent();
    }

    if (m_nodeType != TYPE_ROOT) {
        if (ImpFile() != 0)
            output.prepend(this->ImpFile()->toString());
        else if (DefFile() != 0)
            output.prepend(this->DefFile()->toString());
    }

    return output;
}

void ScopeNode::printClones(int indent, bool verbose)
{
    QString output;

    for (int i=0; i<indent; i++) { output += "    "; }

    for (ScopeClone* i : Clones()) {
        if (verbose) {
            qDebug() << output + i->m_target->toString()
                     << 'r' << i->m_target->RevisionId()
                     << '[' << i->m_target->m_impStartLine
                     << ',' << i->m_target->m_impEndLine << ']'
                     << " <==> " + i->m_source->toString()
                     << 'r' << i->m_source->RevisionId()
                     << '[' << i->m_source->m_impStartLine
                     << ',' << i->m_source->m_impEndLine << ']';

            for (CodeClone* j : i->m_codeClones) {
                qDebug() << "        " +  output + "> " + j->File()->toString()
                         << 'r' << j->File()->RevisionId()
                         << '[' << j->StartLine()
                         << ',' << j->EndLine() << ']';
            }
        } else {
            bool isIntraClone = i->m_target->RevisionId() == i->m_source->RevisionId();

            // Filter out only nodes that differ
            //if (i->target->toString() != i->source->toString())
                qDebug() << output + i->m_target->toString()
                          + (isIntraClone ? " <=!!=> " : " <==> ")
                          + i->m_source->toString();
        }
    }
}

void ScopeNode::printNodes(int indent)
{
    QString output;
    QString nameStr;

    for (int i=1; i<indent; i++) { output += "    "; }
    if (indent > 0) output += "  |-";

    switch(m_nodeType) {
        case TYPE_NAMESPACE:    nameStr += "NS ";           break;
        case TYPE_CLASS:        nameStr += "CL ";           break;
        case TYPE_FUNCTION:     nameStr += "FU ";           break;
        case TYPE_ATTRIBUTE:    nameStr += "AT ";           break;
        case TYPE_ENUM:         nameStr += "EN ";           break;
        case TYPE_LOGIC:        nameStr += "?? ";           break;
        default:                nameStr += " ";             break;
    }
    nameStr += m_name;

    if (Type() != TYPE_ROOT) qDebug() << output + nameStr;

    // Print Imp/Def
    if (m_defFile != 0)
        qDebug() << "    " + output + "> Def: " + m_defFile->toString() << m_defLine;
    if (m_impFile != 0)
        qDebug() << "    " + output + "> Imp: " + m_impFile->toString() << m_impStartLine << m_impEndLine;

    // Print all children
    for (ScopeNode* i : m_children) i->printNodes(indent+1);
}

int ScopeNode::RevisionId() const
{
    // Sanity check
    if (m_impFile == 0) return -1;

    return m_impFile->RevisionId();
}
