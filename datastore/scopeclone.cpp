#include "scopeclone.h"

#include <QDebug>

#include "datastore/codeclone.h"
#include "datastore/filenode.h"
#include "datastore/scopenode.h"

/* ************************************************************************************************
 * Constructor & Destructor
 **************************************************************************************************/
ScopeClone::ScopeClone(ScopeNode *source, ScopeNode *target) :
    m_source    (source)
  , m_target    (target)
  , m_isIntra   (source->RevisionId() == target->RevisionId())
  , m_driftType (0)
  , m_intraType (0)
  , m_nLines    (0)
{}

/* ************************************************************************************************
 * Getters & Setters
 **************************************************************************************************/
QString ScopeClone::toString() const
{
    return  m_target->toString() + "<==>" + m_source->toString();
}

QString ScopeClone::reverse() const
{
    return  m_source->toString() + "<==>" + m_target->toString();
}

void ScopeClone::destroy()
{
    this->Target()->removeScopeClone(this);
}

/* ************************************************************************************************
 * Public Functions
 **************************************************************************************************/
void ScopeClone::addCodeClone(CodeClone *cClone)
{
    m_codeClones << cClone;
}

void ScopeClone::addLines(int nLines)
{
    m_nLines += nLines;
}

void ScopeClone::print(int indent)
{
    QString output;

    for (int i=1; i<indent; i++) { output += "    "; }
    if (indent > 0) output += "  |-";

    qDebug() << output + m_source->toString() + " <==> " + m_target->toString();

    // Print clones
    for (CodeClone* i : m_codeClones) {
        qDebug() << "    " + output + i->File()->toString() << i->StartLine() << i->EndLine();
    }
}

//float ScopeCloneExtractor::matchFactor(CodeClone *cc, ScopeNode *sn)
//{
//    float scpLen = sn->impEndLine - sn->impStartLine + 1;
//    float difTop = std::max(0, cc->StartLine() - sn->impStartLine);
//    float difBot = std::max(0, sn->impEndLine - cc->EndLine());

//    // Subtract the differences and divide by scope length
//    return (scpLen - difTop - difBot) / scpLen;
//}
