#ifndef DATASTORE_H
#define DATASTORE_H

#include <QMap>
#include <QHash>
#include <QString>

class FileNode;
class RevisionNode;
class ScopeNode;

/*! ***********************************************************************************************
 * \author  Avdo Hanjalic
 * \date    27.03.2012
 * \class   DataStore
 *
 * \brief   Root element of a project
 **************************************************************************************************/
class DataStore
{
public:
    /* ****************************************************************************************
     * Typedefs
     ******************************************************************************************/
    typedef QList<RevisionNode*>        RevisionList;
    typedef QMap<int, RevisionNode*>    RevisionMap;

    /* ****************************************************************************************
     * Constructor & Destructor
     ******************************************************************************************/
    DataStore(QString name, QString url);

    /* ****************************************************************************************
     * Getters & Setters
     ******************************************************************************************/
    RevisionNode        *Union();
    RevisionList const  &Revisions();

    QString             Name() const                                { return m_name; }
    QString             Url() const                                 { return m_url; }

    bool                IsRunning() const                           { return m_isRunning; }

    // Setters
    bool                setName (QString name);
    void                setRunning(bool b)                          { m_isRunning = b; }
    bool                setUrl  (QString url);

    /* ****************************************************************************************
     * Public Functions
     ******************************************************************************************/
    RevisionNode        *addRevision (int revNo);
    RevisionNode        *getRevision (int revNo);
    bool                hasRevision  (int revNo)                    { return m_revisions.contains(revNo); }

    void                print();

private:
    /* ****************************************************************************************
     * Private Functions
     ******************************************************************************************/
    void                generateUnion();

    /* ****************************************************************************************
     * Attrributes
     ******************************************************************************************/
    RevisionMap         m_revisions;                                //! Maintains order!

    QString             m_name;
    QString             m_url;

    RevisionNode        *m_union;
    bool                m_unionDirty;

    bool                m_isRunning;

    RevisionList        m_revisionCache;
    bool                m_revisionCacheDirty;
};

#endif // DATASTORE_H
