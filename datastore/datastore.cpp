#include "datastore.h"

#include <QDebug>
#include <QDir>

#include "datastore/filenode.h"
#include "datastore/revisionnode.h"
#include "datastore/scopeclone.h"
#include "datastore/scopenode.h"

/* ************************************************************************************************
 * Constructor & Destructor
 **************************************************************************************************/
DataStore::DataStore(QString name, QString url) :
    m_name              (name)
  , m_url               (url)
  , m_union             (0)
  , m_unionDirty        (true)
  , m_isRunning         (true)
  , m_revisionCacheDirty(true)
{
    // Make sure the url ends without "/"
    if (m_url.endsWith("/")) m_url.chop(1);

    // Create the directory
    QDir dir;
    dir.mkdir(name);
}

/* ************************************************************************************************
 * Getters & Setters
 **************************************************************************************************/
RevisionNode *DataStore::Union()
{
    // Sanity check
    if (m_unionDirty)    generateUnion();

    return m_union;
}

QList<RevisionNode*> const &DataStore::Revisions()
{
    if (m_revisionCacheDirty) {
        m_revisionCache.clear();
        for (RevisionNode *r : m_revisions) {
             if (r->Id() >= 1) m_revisionCache << r;
        }
        m_revisionCacheDirty = false;
    }

    return m_revisionCache;
}

/* ************************************************************************************************
 * Public Functions
 **************************************************************************************************/
RevisionNode* DataStore::addRevision(int revNo)
{
    if (!m_revisions.contains(revNo)) {
        // Connect it to predecessor, if it exists
        RevisionNode *newRev    = new RevisionNode(this, revNo);
        m_revisions.insert(revNo, newRev);

        m_unionDirty = true;
        m_revisionCacheDirty = true;
    }

    return getRevision(revNo);
}

RevisionNode* DataStore::getRevision(int revNo)
{
    return m_revisions.value(revNo, 0);
}

bool DataStore::setName(QString name)
{
    if (m_revisions.count() == 0) {
        m_name = name;
        return true;
    } else {
        qDebug() << "DataStore already contains Revisions! Cannot change Name!!!";
        return false;
    }
}

bool DataStore::setUrl(QString url)
{
    if (m_revisions.count() == 0) {
        m_url = url;
        return true;
    } else {
        qDebug() << "DataStore already contains Revisions! Cannot change URL!!!";
        return false;
    }
}

void DataStore::print()
{
    qDebug() << "### Printing DataStore " + m_name + ":";

    for (RevisionNode* i : m_revisions) i->print();
}

/* ************************************************************************************************
 * Private Functions
 **************************************************************************************************/
void DataStore::generateUnion()
{
    if (m_union == 0)    m_union = addRevision(-2);

    // Copy the fileTrees
    for (RevisionNode *rev : m_revisions) {
        if (rev->Id() < 1) continue;
        m_union->RootFile()->copy(rev->RootFile());
    }

    // Copy the scopeTrees (wil link to fileTree)
    for (RevisionNode *rev : m_revisions) {
        if (rev->Id() < 1) continue;
        m_union->RootScope()->copy(rev->RootScope(), m_union->RootFile());
    }

    // Refresh the caches of revisions
//    for (RevisionNode *rev : m_revisions)    { rev->refreshCache(); }

//    generateScopeOperations();
//    generateScopeCloneOperations();

    m_unionDirty = false;
}
