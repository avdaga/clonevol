#ifndef NODETYPE_H
#define NODETYPE_H

enum NodeType 
{
    TYPE_DEFAULT    = 7,
    TYPE_ROOT       = 1,
    TYPE_DIR        = 2,
    TYPE_FILE       = 4,
    TYPE_NAMESPACE  = 8,
    TYPE_CLASS      = 16,
    TYPE_FUNCTION   = 32,
    TYPE_SLOT       = 64,
    TYPE_ATTRIBUTE  = 128,
    TYPE_SIGNAL     = 256,
    TYPE_ENUM       = 512,
    TYPE_DEFINE     = 1024,
    TYPE_LOGIC      = 2048,      // Other logic node type
    TYPE_ALL        = 4095
};

#endif // NODETYPE_H
