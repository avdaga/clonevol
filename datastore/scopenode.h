#ifndef LOGICNODE_H
#define LOGICNODE_H

#include <QHash>
#include <QMap>
#include <QString>

#include "datastore/nodetype.h"

class CodeClone;
class FileNode;
class ScopeClone;

/*! ***********************************************************************************************
 * \author  Avdo Hanjalic
 * \date    27.02.2012
 * \class   ScopeNode
 *
 * \brief   Representation of a node in the logic-tree
 **************************************************************************************************/
class ScopeNode
{
public:
    /* ****************************************************************************************
     * Typedefs
     ******************************************************************************************/
    typedef QHash<ScopeNode*, ScopeClone*>  ScopeNodeCloneHash;
    typedef QMap<QString,ScopeNode*>        ScopeNodeMap;

    /* ****************************************************************************************
     * Constructor & Destructor
     ******************************************************************************************/
    ScopeNode(ScopeNode* parent, QString name, NodeType type);

    /* ****************************************************************************************
     * Getters & Setters
     ******************************************************************************************/
    QString             Name() const                                { return m_name; }
    char                Operation() const                           { return m_operation; }
    ScopeNode           *Parent() const                             { return m_parent; }
    int                 RevisionId() const;
    NodeType            Type() const                                { return m_nodeType; }

    QList<ScopeNode*>   Children() const;
    QList<ScopeClone*>  Clones() const;
    FileNode            *DefFile() const                            { return m_defFile; }
    FileNode            *ImpFile() const                            { return m_impFile; }
    QList<ScopeClone*>  IntraClones() const;
    QList<ScopeNode*>   PathFromRoot();

    QString             toString() const;

    /* ****************************************************************************************
     * Public Functions
     ******************************************************************************************/
    void                addChild        (ScopeNode* subNode);

    void                copy            (ScopeNode* source, FileNode *parentFile);

    ScopeNode           *getChild       (QString subPath);
    bool                hasChild        (QString subPath);
    ScopeNode           *makeChild      (QString subPath);
    ScopeClone          *makeScopeClone (ScopeNode* source, CodeClone* cClone, int nLines);

    void                printClones     (int indent = 0, bool verbose = false);
    void                printNodes      (int indent = 0);

    void                removeScopeClone(ScopeClone* clone);

    void                setDefFile      (FileNode* file, int line);
    void                setImpFile      (FileNode* file, int startLine, int endLine);

    void                setOperation    (char c) { m_operation = c; }

private:
    /* ****************************************************************************************
     * Attrributes
     ******************************************************************************************/
    // ScopeNode location stuff
    ScopeNode           *m_parent;
    ScopeNodeMap        m_children;

    // Properties
    QString             m_name;
    NodeType            m_nodeType;

    char                m_operation;                                //!< (Modified, Added, Deleted)

    // FileNode relations
    FileNode            *m_defFile;
    int                 m_defLine;
    FileNode            *m_impFile;
    int                 m_impStartLine;
    int                 m_impEndLine;

    // Related ScopeClones
    ScopeNodeCloneHash  m_scopeClones;

    friend class FileNode;
    friend class ScopeExtractor;
};

#endif // LOGICNODE_H
