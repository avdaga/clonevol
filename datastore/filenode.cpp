#include "filenode.h"

#include <QDebug>
#include <QStringList>

#include "datastore/codeclone.h"
#include "datastore/revisionnode.h"
#include "datastore/scopenode.h"

/* ************************************************************************************************
 * Constructor & Destructor
 **************************************************************************************************/
FileNode::FileNode(RevisionNode* rev, FileNode* parent, QString name, NodeType type) :
    m_revision  (rev)
  , m_parent    (parent)
  , m_name      (name)
  , m_nodeType  (type)
  , m_operation (0)
{}

/* ************************************************************************************************
 * Public Functions
 **************************************************************************************************/
void FileNode::addChild(FileNode* subNode)
{
    m_children.insert(subNode->Name(), subNode);
}

void FileNode::addCodeClone(CodeClone* newClone)
{
    m_codeClones << newClone;
    makeScopeClones(newClone);
}

void FileNode::addDef(ScopeNode *subNode)
{
    m_DefNodes.insert(subNode->Name(), subNode);
}

void FileNode::addImp(ScopeNode *subNode)
{
    m_ImpNodes.insert(subNode->Name(), subNode);
}

void FileNode::copy(FileNode *source)
{
    m_name      = source->m_name;
    m_nodeType  = source->m_nodeType;

    FileNode* child;
    for (FileNode* srcChild : source->m_children) {
        child = getChild(srcChild->Name());

        // Check whether the child already exists
        if (child == 0) {
            child = new FileNode(m_revision,this,srcChild->Name(), srcChild->Type());
            addChild(child);
        }

        child->copy(srcChild);
    }
}

FileNode* FileNode::getChild(QString path)
{
    FileNode*   currentNode = this;
    QStringList subnodes    = path.split('/', Qt::SkipEmptyParts);

    // Can we traverse anything?
    if (subnodes.count() == 0 || m_children.count() == 0) return 0;

    // Traverse the path
    while(subnodes.count() > 0 && currentNode != 0) {
        currentNode = currentNode->m_children.value(subnodes.takeFirst(), 0);
    }

    return currentNode;
}

FileNode* FileNode::makeChild(QString path, char operation)
{
    // Sanity check: if the file already exists, return it without changes
    if (FileNode *child = getChild(path)) { return child; }

    FileNode*   currentNode = this;
    FileNode*   nextNode    = NULL;
    NodeType    nextType;
    bool        nextIsFile;

    QStringList subnodes    = path.split('/', Qt::SkipEmptyParts);

    // Traverse the path
    while(subnodes.count() > 0) {
        // SubPath exists?
        if (currentNode->m_children.contains(subnodes.first())) {
            currentNode = currentNode->m_children.value(subnodes.first());
        } else {
            // What type should the next node be?
            nextIsFile  = subnodes.count() == 1 && subnodes.first().contains(".");

            nextType    = nextIsFile ? TYPE_FILE : TYPE_DIR;
            nextNode    = new FileNode(m_revision, currentNode, subnodes.first(), nextType);            

            currentNode->m_children.insert(subnodes.first(), nextNode);
            currentNode = nextNode;
        }

        subnodes.removeFirst();
    }

    // Set action
    currentNode->setOperation(operation);

    return currentNode;
}

void FileNode::makeScopeClones(CodeClone *srcCC)
{
    // Initialize ScopeClone sources and targets
    QList<ScopeNode*> srcScopes = srcCC->Scopes();
    QList<ScopeNode*> tarScopes;

    // Find new ScopeClones that result from creation of this
    for (CodeClone* tarCC : *srcCC->CloneSet()) {
        // Skip self
        if (srcCC == tarCC) continue;

        tarScopes = tarCC->Scopes();    // Targets scopes for iterated CodeClone

        // Calculate displacement of the target codeblock
        int displace    = srcCC->StartLine() - tarCC->StartLine();

        // Connect sources and targets between this and iterated CodeClone
        for (ScopeNode* srcScp : srcScopes) {
            for (ScopeNode* tarScp : tarScopes) {
                // Do the blocks overlap?
                if (srcScp->m_impStartLine > tarScp->m_impEndLine + displace
                    || srcScp->m_impEndLine < tarScp->m_impStartLine + displace)
                    continue;

                int matchLines = qMin(tarScp->m_impEndLine,   tarCC->EndLine())
                               - qMax(tarScp->m_impStartLine, tarCC->StartLine());

                // If there's a match, create a subsequent ScopeClone
                if (matchLines > 0) {
                    if (srcScp->RevisionId() > tarScp->RevisionId()) {
                        srcScp->makeScopeClone(tarScp, srcCC, matchLines);
                    } else if (srcScp->RevisionId() < tarScp->RevisionId()) {
                        tarScp->makeScopeClone(srcScp, srcCC, matchLines);
                    } else {
                        // Intra-clones go both ways
                        tarScp->makeScopeClone(srcScp, srcCC, matchLines);
                        srcScp->makeScopeClone(tarScp, srcCC, matchLines);
                    }
                }
            }
        }
    }
}

FileNode::ScopeList FileNode::scopesAt(int startLine, int endLine)
{
    QList<ScopeNode*> output;

    // Check all implementation nodes we know
    for (ScopeNode* i : m_ImpNodes) {
        if (i->m_impStartLine < endLine && i->m_impEndLine >= startLine) output << i;
    }

    // Could not find a node
    return output;
}

QString FileNode::toString() const
{
    QString output;

    // Return name 'recursively'
    if (m_parent != 0 && m_parent->Type() != TYPE_ROOT)
        output = m_parent->toString();

    if (m_nodeType == TYPE_ROOT)
        output.append(m_name);
    else
        output.append("/" + m_name);

    return output;
}

void FileNode::printClones(int indent)
{
    QString output;

    for (int i=1; i<indent; i++) { output += "    "; }

    if (Type() != TYPE_ROOT) qDebug() << output + "  |-" + m_name;

    // Print clones
    for (CodeClone* i : m_codeClones) {
        for (CodeClone* j : *i->CloneSet()) {
            if (i == j) continue;

            qDebug().nospace() << "    " + output
                               << '[' << i->StartLine()
                               << ',' << i->EndLine() << ']'
                               << "\t<==>\t"
                               << 'r' << j->File()->RevisionId()
                               << ' ' << j->File()->Name() << ' '
                               << '[' << j->StartLine()
                               << ',' << j->EndLine() << ']';
        }
    }

    for (FileNode* i : m_children) {
        i->printClones(indent+1);
    }
}


void FileNode::printNodes(int indent)
{
    QString output;

    for (int i=1; i<indent; i++) { output += "    "; }
    if (indent > 0) output += "  |-";

    if (Type() != TYPE_ROOT) qDebug() << output + m_name;

    // Print defined nodes
    for (ScopeNode* i : m_DefNodes) {
        qDebug() << "    " + output + ">Def " + i->toString();
    }
    // Print implemented nodes
    for (ScopeNode* i : m_ImpNodes) {
        qDebug() << "    " + output + ">Imp " + i->toString();
    }
    // Print file nodes
    for (FileNode* i : m_children) i->printNodes(indent+1);
}

int FileNode::RevisionId() const
{
    return m_revision->Id();
}
