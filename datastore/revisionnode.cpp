#include "revisionnode.h"

#include <QDebug>

#include "datastore/codeclone.h"
#include "datastore/datastore.h"
#include "datastore/filenode.h"
#include "datastore/scopeclone.h"
#include "datastore/scopenode.h"

/* ************************************************************************************************
 * Constructor & Destructor
 **************************************************************************************************/
RevisionNode::RevisionNode(DataStore* ds, int revNo) :
    m_dataStore             (ds)
  , m_id                    (revNo)
  , m_isAcquired            (false)
  , m_prevRev               (0)
  , m_fileCacheDirty        (true)
  , m_scopeCacheDirty       (true)
  , m_intraCloneCacheDirty  (true)
  , m_interCloneCacheDirty  (true)
{
    m_rootFile  = new FileNode (this, 0, "/",  TYPE_ROOT);
    m_rootScope = new ScopeNode(0, "::", TYPE_ROOT);
}

/* ************************************************************************************************
 * Getters & Setters
 **************************************************************************************************/
QString RevisionNode::Path() const
{
    return m_dataStore->Name() + "/" + QString::number(m_id);
}

RevisionNode* RevisionNode::PrevRevision()
{
    if (m_prevRev == 0) {
        QList<RevisionNode*> revList = m_dataStore->Revisions();
        int idx = revList.indexOf(this);

        m_prevRev = (idx > 0) ? revList[idx-1] : 0;
    }

    return m_prevRev;
}

const QHash<QString, FileNode *> &RevisionNode::FileCache()
{
    if (m_fileCacheDirty) {
        // Generate FileNode cache
        m_fileCache.clear();
        QList<FileNode*> fileQueue;     fileQueue << RootFile();
        FileNode* file = 0;
        while(!fileQueue.isEmpty()) {
            file = fileQueue.takeFirst();
            m_fileCache.insert(file->toString(), file);
            fileQueue << file->Children().values();
        }

        m_fileCacheDirty = false;
    }
    return m_fileCache;
}

const RevisionNode::ScopeHash &RevisionNode::ScopeCache()
{
    if (m_scopeCacheDirty) {
        // Generate ScopeNode cache
        m_scopeCache.clear();
        QList<ScopeNode*> scopeQueue;     scopeQueue << RootScope();
        ScopeNode* scope = 0;
        while(!scopeQueue.isEmpty()) {
            scope = scopeQueue.takeFirst();
            m_scopeCache.insert(scope->toString(), scope);
            scopeQueue << scope->Children();
        }

        m_scopeCacheDirty = false;
    }
    return m_scopeCache;
}

const QHash<QString, ScopeClone *> &RevisionNode::IntraCloneCache()
{
    if (m_intraCloneCacheDirty) {
        // Generate ScopeClone cache
        m_intraCloneCache.clear();
        foreach (ScopeClone* sc, RootScope()->Clones()) {
            if (sc->IsIntra())   m_intraCloneCache.insert(sc->toString(), sc);
        }

        m_intraCloneCacheDirty = false;
    }

    return m_intraCloneCache;
}

const QHash<QString, ScopeClone *> &RevisionNode::InterCloneCache()
{
    if (m_interCloneCacheDirty) {
        // Generate ScopeClone cache
        m_interCloneCache.clear();
        foreach (ScopeClone* sc, RootScope()->Clones()) {
            if (!sc->IsIntra())  m_interCloneCache.insert(sc->toString(), sc);
        }

        m_interCloneCacheDirty = false;
    }

    return m_interCloneCache;
}

QString RevisionNode::toString() const
{
    return QString::number(m_id);
}

/* ************************************************************************************************
 * Public Functions
 **************************************************************************************************/
void RevisionNode::addDeletedScope(ScopeNode* scp)
{
    m_deletedScopes.insert(scp->toString(), scp);
}

void RevisionNode::addDeletedScopeClone(ScopeClone *sc)
{
    m_deletedScopeClones.insert(sc->toString(), sc);
}

void RevisionNode::print()
{
    qDebug() << "###############################################################";
    qDebug() << "### Revision" << m_id;
    qDebug() << "###############################################################";

    qDebug() << "  |- FileTree";
//    m_rootFile->printNodes(1);

    qDebug() << "  |- ScopeTree";
//    m_rootScope->printNodes(1);

    qDebug() << "  |- CodeClones";
    m_rootFile->printClones(1);

    qDebug() << "  |- ScopeClones";
    m_rootScope->printClones(1);

//    qDebug() << "  |- CodeClones " << codeCloneSources.count();
//    for (CodeClone* i : codeCloneSources) {
//        i->printClones(2);
//    }

//    qDebug() << "  |- ScopeClones " << scopeCloneSources.count();
//    for (ScopeClone* i : scopeCloneSources) {
//        i->print(2);
//    }
}
