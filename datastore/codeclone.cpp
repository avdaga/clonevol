#include "codeclone.h"

#include <QDebug>

#include "datastore/filenode.h"
#include "datastore/scopeclone.h"
#include "datastore/scopenode.h"

/* ************************************************************************************************
 * Constructor & Destructor
 **************************************************************************************************/
CodeClone::CodeClone(FileNode *file, int start, int end, CloneList *cloneSet) :
    m_fileNode  (file)
  , m_startLine (start)
  , m_endLine   (end)
  , m_cloneSet  (cloneSet)
{}

/* ************************************************************************************************
 * Public Functions
 **************************************************************************************************/
void CodeClone::printClones(int indent)
{
    QString output;

    for (int i=1; i<indent; i++) { output += "    "; }
    if (indent > 0) output += "  |-";

    qDebug() << output + m_fileNode->toString() << m_startLine << m_endLine;

    // Print clones
    for (CodeClone* i : *m_cloneSet) {
        if (i == this) continue;     // Skip if self;
        qDebug() << "    " + output + i->m_fileNode->toString() << i->m_startLine << i->m_endLine;
    }
}

QList<ScopeNode*> CodeClone::Scopes()
{
    return m_fileNode->scopesAt(m_startLine, m_endLine);
}
