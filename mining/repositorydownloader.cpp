#include "repositorydownloader.h"

#include <QtCore/QCoreApplication>
#include <QtCore/QDebug>
#include <QtCore/QDir>
#include <QtXml/QDomDocument>

#include "datastore/datastore.h"
#include "datastore/filenode.h"
#include "datastore/revisionnode.h"

/* ************************************************************************************************
 * Constructor & Destructor
 **************************************************************************************************/
RepositoryDownloader::RepositoryDownloader(DataStore *ds) :
    QObject         (0)
  , m_dataStore     (ds)
  , m_nRevisions    (-1)
  , m_isInfoObtained(false)
  , m_gitProc       (new QProcess())
{
    /*! ***************************************************************************************
     * Constructor
     ******************************************************************************************/
}

/* ************************************************************************************************
 * Public Functions
 **************************************************************************************************/
bool RepositoryDownloader::getInfo()
{
    /*! ***************************************************************************************
     * Extract respository information, i.e. revisions and repo root
     ******************************************************************************************/
    qDebug() << "###############################################################";
    qDebug() << "### Looking up repository information...";
    qDebug() << "###############################################################";

    /* 1a. Check whether repo already exists
     ******************************************************************************************/
    bool isCloneNeeded = true;

    // Check whether repodit already exists and is a valid git repo clone
    if (QDir(repodir()).exists()) {
        // Check whether it's a git repo
        m_gitProc->setWorkingDirectory(repodir());
        m_gitProc->start("git", { "status" });
        m_gitProc->waitForFinished(-1);

        // If exit code was 0, the dir is a valid repo
        isCloneNeeded = !(m_gitProc->exitCode() == 0);
    }

    /* 1b. Clone git repo if needed
     ******************************************************************************************/
    if (!isCloneNeeded) {
        qDebug() << ":) Repository aleady cloned\n";
    } else {
        // Run 'git clone <URL>'
        m_gitProc->setWorkingDirectory(QDir::currentPath());
        m_gitProc->start("git", { "clone", m_dataStore->Url(), repodir() });
        m_gitProc->waitForFinished(-1);
        m_gitProc->waitForReadyRead(-1);

        if (m_gitProc->exitCode() != 0) {
            qDebug() << ":( Cloning repository failed\n";
            m_isInfoObtained = false;
            return m_isInfoObtained;
        }

        qDebug() << ":) Cloning repository successfull\n";
        m_isInfoObtained = true;
    }

    /* 2. Get number of commits
     ******************************************************************************************/
    // Run 'git rev-list --count HEAD' (returns number of commits)
    m_gitProc->setWorkingDirectory(repodir());
    m_gitProc->start("git", { "rev-list", "--count", "HEAD" });
    m_gitProc->waitForFinished(-1);
    m_gitProc->waitForReadyRead(-1);

    // Process output: store number of commits
    m_nRevisions = m_gitProc->readAllStandardOutput().toInt();

    /* 3. Update administration
     ******************************************************************************************/
    qDebug() << "\tURL:" << m_dataStore->Url();
    qDebug() << "\tnRevisions: " << m_nRevisions;

    m_isInfoObtained = (m_nRevisions != -1);

    // Update log
    qDebug() << (m_isInfoObtained
                ? ":) Connecting to repository successfull\n"
                : ":( Connecting to repository failed\n");

    // If info is there, get the logs, else return false
    return m_isInfoObtained;
}

bool RepositoryDownloader::getLogs()
{
    /*! ***************************************************************************************
     * Parse logs, i.e. files and changes
     *
     * \todo Handle deleted directories
     ******************************************************************************************/
    // Sanity check: Stop if repository information unavailable
    if (!m_isInfoObtained) { return false; }

    qDebug() << "###############################################################";
    qDebug() << "### Processing changelogs";
    qDebug() << "###############################################################";

    m_gitProc->setWorkingDirectory(repodir());
    m_gitProc->start("git", { "log", "--pretty=oneline", "--name-status", "--reverse" });
    m_gitProc->waitForFinished(-1);
    m_gitProc->waitForReadyRead(-1);

    processLog(m_gitProc->readAllStandardOutput());

//    // Post-process deleted directories (get deleted files)
//    foreach(int revNr, m_deletedDirs.keys()) {
////        getLogDeleted(revNr);
//        getDeleted(revNr);
//    }

    return true;
}

bool RepositoryDownloader::getRevisions(int startRev, int endRev)
{
    // Stop if repository information unavailable
    if (!m_isInfoObtained) {
        if (!this->getInfo()) return false;
        if (!this->getLogs()) return false;
    }

    // Clean up previous run
    QDir base(m_dataStore->Name() + "/-1");
    if (base.exists()) {
        base.removeRecursively();
    }

    // Stop if base files necessary but not available
    if (startRev > m_dataStore->Revisions().first()->Id()) {
        emit sigProgress({true, -1, "Downloading Base Files of Revision "
                          + QString::number(startRev)});

        if (!this->getBase(startRev - 1)) return false;
    }

    // Make list of revisions to acquire
    QList<RevisionNode*> revs;
    foreach(RevisionNode* rev, m_dataStore->Revisions()) {
        // Process only assigned revisions
        if (rev->Id() < startRev-1 || rev->Id() > endRev)
            continue;
        else
            revs << rev;
    }

    qDebug() << "###############################################################";
    qDebug() << "### Acquiring revision files...";
    qDebug() << "###############################################################";
    // Get Files
    for (int i = 0; i < revs.count(); ++i) {
        if (!m_dataStore->IsRunning()) return false;

        RevisionNode* rev = revs[i];

        emit sigProgress({true, 100 * qreal(i) / revs.size(),
                          "Downloading Files of Revision " + QString::number(rev->Id())});

        if (!this->getFiles(rev->Id())) {
            if (m_dataStore->IsRunning()) {
                emit sigProgress({false, 0, "Failed to acquire Files of Revision " + QString::number(rev->Id())});
            }

            return false;
        }
    }

    emit sigProgress({true, 100, "Downloaded Files for Revisions " +
                      QString::number(startRev) + " to " + QString::number(endRev)});

    return true;
}


/* ************************************************************************************************
 * Private Functions
 **************************************************************************************************/
bool RepositoryDownloader::getBase(int revision)
{
    /*! ***************************************************************************************
     * Gets the 'base files' for revision. These are all the files required for doxygen to be
     * able to do its job, more specifically: all c/c++ header files.
     ******************************************************************************************/
    // Sanity check: Stop if repository information unavailable
    if (!m_isInfoObtained) { return false; }

    /* 1. List the base files
     ******************************************************************************************/
    qDebug() << "###############################################################";
    qDebug() << "### Listing base files...";
    qDebug() << "###############################################################";

    m_gitProc->setWorkingDirectory(repodir());
    m_gitProc->start("git", {"ls-tree", "--name-only", "-r", QString::number(revision,16)});
    m_gitProc->waitForFinished(-1);
    m_gitProc->waitForReadyRead(-1);

    // Create all base files in base revision
    processBase(m_gitProc->readAllStandardOutput());

    /* 2. Download all base files
     ******************************************************************************************/
    qDebug() << "###############################################################";
    qDebug() << "### Acquiring contents of base files...";
    qDebug() << "###############################################################";

    return getFiles(revision, true);
}

bool RepositoryDownloader::getFiles(int revision, bool baseRev)
{
    /*! ***************************************************************************************
     * 'Downloads' the files for revision into a separate dir, so that they can be compared.
     ******************************************************************************************/
    RevisionNode *revNode = m_dataStore->addRevision(baseRev ? -1 : revision);

    QString revisionHash = "HEAD~" + QString::number(m_nRevisions-revision-1);

    // Sanity check: does the revision node exist?
    if (revNode == nullptr) { return false; }

    /* 1. Create revision target directory: ./project/revNr/
     ******************************************************************************************/
    qDebug() << "### Acquiring files of revision" << revisionHash << "...";

    // Create folder for revision
    QDir(QDir::currentPath()).mkpath(revNode->Path());

    /* 2. Create breadth-first queue for all files to export
     ******************************************************************************************/
    QList<FileNode*> nodeQueue{ revNode->RootFile()->Children().values() };
    QList<FileNode*> files;

    while(!nodeQueue.isEmpty()) {
        FileNode* currentNode = nodeQueue.takeFirst();

        // Sanity check: if the file/dir was deleted, there is nothing to do
        if (currentNode->Operation() == 'D') continue;

        // Queue file for acquisition
        files << currentNode;

        // Enqueue all children
        nodeQueue << currentNode->Children().values();
    }

    /* 3. Acquire files of revision
     ******************************************************************************************/
    m_gitProc->setWorkingDirectory(repodir());

    int fileCnt = 0;
    for (FileNode *file : files)
    {
        /* 3a. Get file information and check existence
         **********************************************************************************/
        // Sanity check: abort if datastore in use
        if (!m_dataStore->IsRunning()) { return false; }

        // Get the current node to obtain and queue it's children
        QString localFilePath   = file->toString().remove(0, 1);
        QString fullFilePath    = revNode->Path() + "/" + localFilePath;

        // Sanity chcek: if subdir or file exists, skip it
        if ((file->Type() == TYPE_DIR && QDir(fullFilePath).exists())
                || (file->Type() == TYPE_FILE && QFile(fullFilePath).exists()))
        {
            qDebug() << "\t!!" << localFilePath << "already exists, skipped...";
            continue;
        }

        /* 3b. Acquire current file
         **********************************************************************************/
        qDebug() << "\t<>" << localFilePath;

        emit sigProgress({true, -1, "Exporting File (" + QString::number(fileCnt+1)
                          + " of " + QString::number(files.size()) +
                         ") of Revision " + revisionHash});

        // If the node is a dir, simply create it
        if (file->Type() == TYPE_DIR) {
            QDir(QDir::currentPath()).mkpath(fullFilePath);
            continue;
        }
        // If the node is a file, export it from git into a local dir
        else {
            m_gitProc->start("git", { "show", revisionHash + ":" + localFilePath });
            m_gitProc->waitForFinished(-1);
            m_gitProc->waitForReadyRead(-1);

            // Check whether git was successful
            if (m_gitProc->exitCode() != 0) {
                qWarning() << "Failed to get file " + localFilePath;
                continue;
            }

            // Write file
            QFile outFile(fullFilePath);
            if (outFile.open(QFile::WriteOnly)) {
                outFile.write(m_gitProc->readAllStandardOutput());
                outFile.close();
            } else {
                qWarning() << "Failed to write file " + fullFilePath;
                continue;
            }

        }
    }

    qDebug() << "";

    return true;
}

void RepositoryDownloader::processBase(const QByteArray &base)
{
    /*! ***************************************************************************************
     * Parse all base file names (headers) and add them to revision -1
     ******************************************************************************************/
    // Store base files (headers) in revision -1
    FileNode* rootFile  = m_dataStore->addRevision(-1)->RootFile();

    // Get all filenames in separate bytearrays
    QList<QByteArray> fileNames = base.split('\n');

    // Create all files
    for (const QByteArray &fileName : fileNames) {
        if (fileName.endsWith(".h")) {
            qDebug() << "\t++" << fileName;
            rootFile->makeChild(fileName);
        } else {
            qDebug() << "\t--" << fileName;
            continue;
        }
    }
}

void RepositoryDownloader::processLog(const QByteArray &log)
{
    /*! ***************************************************************************************
     * Parse logs, expected format:
     *
     * 3f5418cb17833b0f7c432664b732c3abf4741e97 Add model support list to README.
     * M       README.md
     *
     * \todo update DataStore to support revision hashes
     ******************************************************************************************/
    // Get loglines
    QList<QByteArray> logLines = log.split('\n');

    QByteArray revHash = "";
    int revNr = 0;

    // proccess loglines
    for (const QByteArray &logLine : logLines) {
        // Sanity check: skip empty lines
        if (logLine.isEmpty()) { continue; }

        // Revision hashes are 40 lines
        if (logLine.length() > 40 && logLine.indexOf(' ') == 40) {
            revHash = logLine.mid(0,8);
            revNr++;
            continue;
        }

        // Sanity check: do we have a revision
        if (revHash.isEmpty()) { continue; }

        // Process log record
        processLogRecord(logLine, revNr);
    }
}

void RepositoryDownloader::processLogRecord(const QByteArray &logLine, int revNr)
{
    /*! ***************************************************************************************
     * Parse logs, expected format:
     *
     * M       README.md
     *
     * \todo Use list of supported extensions
     ******************************************************************************************/
    // Split into different words
    QList<QByteArray> logLineArgs = logLine.split('\t');

    // Sanity check: make sure we have 2 args or more
    if (logLineArgs.size() < 2) { return; }

    QString filePath    = logLineArgs[1];
    char    operation   = logLineArgs[0][0];

    /*** Get pointers to possibly newly created revision nodes ****************/
    /**************************************************************************/
    FileNode* currRootFile  = m_dataStore->addRevision(revNr)->RootFile();
    FileNode* prevRootFile  = m_dataStore->addRevision(revNr-1)->RootFile();

    // Create new revision node's if they do not exist yet
    //if (current == 0)  current  = rootNode->addNode(revision);
    //if (previous == 0) previous = rootNode->addNode(revision-1);


    // Filter on file extension
    if (isFileExtensionSupported(filePath)) {
        qDebug() << "\t++" << filePath;
    } else {
        qDebug() << "\t--" << filePath;
        return;
    }

    switch (operation) {
        // File was copied
        case 'C':
            if (logLineArgs.size() >= 3) {
                // original
                currRootFile->makeChild(filePath, 0);
                // clone
                currRootFile->makeChild(logLineArgs[2], 'A');
            }
            break;
        // File was renamed
        case 'R':
            if (logLineArgs.size() >= 3) {
                // original
                prevRootFile->makeChild(filePath, 0);
                currRootFile->makeChild(filePath, 'D');
                // renamed
                currRootFile->makeChild(logLineArgs[2], 'A');
            }
            break;
        // File was modified
        case 'M':
            // Create in previous revision if if didn't already exist
            prevRootFile->makeChild(filePath, 0);
            // Fallthrough
        // File was added or deleted (does not affect other revision)
        case 'A':
        case 'D':
            currRootFile->makeChild(filePath, operation);
            break;
    }
}

bool RepositoryDownloader::isFileExtensionSupported(const QString &filePath)
{
    /*! ***************************************************************************************
     * Checks whether the extension is supported
     ******************************************************************************************/
    const QStringList SUPPORTED_EXTENSIONS { ".cpp", ".h", ".c", ".c++", ".java" };

    // Check all extensions
    for (const QString &extension : SUPPORTED_EXTENSIONS) {
        if (filePath.endsWith(extension)) { return true; }
    }

    // Failed to find supported extension
    return false;
}

QString RepositoryDownloader::repodir() const
{
    /*! ***************************************************************************************
     * Returns dir in which the git repo is stored (relative to working dir).
     ******************************************************************************************/
    return m_dataStore->Name() + "/gitrepo";
}
