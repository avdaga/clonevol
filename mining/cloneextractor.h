#ifndef CLONEINTERPRETER_H
#define CLONEINTERPRETER_H

#include <QObject>
#include <QtCore/QProcess>
#include <QtXml/QDomNode>

#include "progressinfo.h"

class DataStore;
class FileNode;
class ScopeNode;
class RevisionNode;

/*! ***********************************************************************************************
 * \author  Avdo Hanjalic
 * \date    17.04.2013
 * \class   CloneExtractor
 *
 * \brief   This class executes Simian for each of the revisions, reads the generated clones and
 *          interprets them.
 **************************************************************************************************/
class CloneExtractor : public QObject
{
    Q_OBJECT

public:
    /* ****************************************************************************************
     * Constructor & Destructor
     ******************************************************************************************/
    CloneExtractor(DataStore* ds);

    /* ****************************************************************************************
     * Public Functions
     ******************************************************************************************/
    bool                getClones(int startRev, int endRev);

signals:
    /* ****************************************************************************************
     * Signals
     ******************************************************************************************/
    void                sigProgress(ProgressInfo);

private:
    /* ****************************************************************************************
     * Private Functions
     ******************************************************************************************/
    bool                generateIntraClones (RevisionNode *rev);
    bool                generateInterClones (RevisionNode *revA, RevisionNode *revB);
    //! Propagates Simian Output to DataStore
    bool                processSimianOutput (RevisionNode *revA, RevisionNode *revB);

    //! Inserts clone into DriftList, if the clone is valid
    bool                processDomNode      (const QDomNode &domNode,
                                             RevisionNode *revA, RevisionNode *revB);

    bool                refineOperations    (RevisionNode *rev);
    void                cleanIntraClones    (RevisionNode *rev);
    void                cleanInterClones    (RevisionNode *rev);

private:
    /* ****************************************************************************************
     * Attrributes
     ******************************************************************************************/
    DataStore           *m_dataStore;

    // External process communication
    QProcess            *m_simian;
};

#endif // CLONEINTERPRETER_H
