#ifndef LOGICEXTRACTOR_H
#define LOGICEXTRACTOR_H

#include <QObject>
#include <QtCore/QProcess>

#include "progressinfo.h"

class DataStore;
class FileNode;
class ScopeNode;
class RevisionNode;
class QDomNode;

/*! ***********************************************************************************************
 * \author  Avdo Hanjalic
 * \date    20.02.2013
 * \class   ScopeExtractor
 *
 * \brief   This class executes Doxygen for each of the revisions and inserts the logical nodes
 *          produced by Doxygen.
 *
 * \todo    ScopeNodes with same name but different input arguments are merged, but should be
 *          distinguished!
 **************************************************************************************************/
class ScopeExtractor : public QObject
{
    Q_OBJECT

public:
    /* ****************************************************************************************
     * Constructor & Destructor
     ******************************************************************************************/
    ScopeExtractor(DataStore* ds);

    /* ****************************************************************************************
     * Public Functions
     ******************************************************************************************/
    //! Generate and propagate Doxygen Output
    bool                getLogic(int startRev, int endRev);

signals:
    /* ****************************************************************************************
     * Signals
     ******************************************************************************************/
    void                sigProgress(ProgressInfo);

private:
    /* ****************************************************************************************
     * Private Functions
     ******************************************************************************************/
    //! Generates Doxygen Output
    bool                generateDoxygenOutput   (RevisionNode* revNode);
    //! Filters output and runs processDoxygenFile on all generated files
    bool                processDoxygenOutput    (RevisionNode* revNode);
    //! Propagates one Doxygen Output file to DataStore
    bool                processDoxygenFile      (RevisionNode* revNode, QString fileName);

    bool                refineOperations        (RevisionNode* currRev);

    //! Inserts node into LogicTree, if def and imp available
    bool                processDomNode          (ScopeNode* parent, RevisionNode* revNode,
                                                 const QDomNode &domNode);
    /* ****************************************************************************************
     * Attrributes
     ******************************************************************************************/
    DataStore           *m_dataStore;

    // External process communication
    QProcess            *m_doxygen;                                 //!< QProcess for doxygen execution
};

#endif // LOGICEXTRACTOR_H
