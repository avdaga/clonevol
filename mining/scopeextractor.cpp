#include "scopeextractor.h"

#include <QtCore/QCoreApplication>
#include <QtCore/QDebug>
#include <QtCore/QDir>
#include <QtXml/QDomDocument>
#include <QXmlStreamReader>

#include "datastore/datastore.h"
#include "datastore/filenode.h"
#include "datastore/scopenode.h"
#include "datastore/revisionnode.h"

/* ************************************************************************************************
 * Constructor & Destructor
 **************************************************************************************************/
ScopeExtractor::ScopeExtractor(DataStore* ds) :
    QObject     (0)
  , m_dataStore (ds)
  , m_doxygen   (new QProcess())
{}

/* ************************************************************************************************
 * Public Functions
 **************************************************************************************************/
void copyDir(QString sourceFolder, QString destFolder)
{
    QDir sourceDir(sourceFolder);
    QDir destDir(destFolder);

    if (!sourceDir.exists()) return;
    if (!destDir.exists())   QDir().mkdir(destFolder);

    QStringList files = sourceDir.entryList(QDir::Files);
    for (int i = 0; i< files.count(); i++) {
        QString srcName = sourceFolder + "/" + files[i];
        QString destName = destFolder + "/" + files[i];
        QFile(destName).remove();
        QFile::copy(srcName, destName);
    }

    files.clear();
    files = sourceDir.entryList(QDir::AllDirs | QDir::NoDotAndDotDot);
    for (int i = 0; i< files.count(); i++) {
        QString srcName = sourceFolder + "/" + files[i];
        QString destName = destFolder + "/" + files[i];
        copyDir(srcName, destName);
    }
}

/* ************************************************************************************************
 * Private Functions
 **************************************************************************************************/
bool ScopeExtractor::getLogic(int startRev, int endRev)
{
    qDebug() << "###############################################################";
    qDebug() << "### Generating doxygen output for revisions...";
    qDebug() << "###############################################################";

    QDir doxyDir    (m_dataStore->Name() + "/DoxygenOutput");
    QDir scratchDir (m_dataStore->Name() + "/Scratch");

    // 1. Make list of revisions to acquire
    QList<RevisionNode*> revs;
    foreach(RevisionNode* rev, m_dataStore->Revisions()) {
        // Process only assigned revisions
        if (rev->Id() < startRev-1 || rev->Id() > endRev)
            continue;
        else
            revs << rev;
    }

    // 2. Clean up potential remaining of previous run
    if (scratchDir.exists()) {
        qDebug() << "\t!!! Deleting previous Scratch dir" << Qt::endl;
        scratchDir.removeRecursively();
    }
    copyDir(m_dataStore->Name() + "/-1", scratchDir.path());

    // 3. Execute doxygen for each available revision
    m_doxygen->setWorkingDirectory(scratchDir.path());
    for (qreal i = 0; i < revs.count(); ++i) {
        if (!m_dataStore->IsRunning()) return false;

        RevisionNode* rev = revs[i];

        emit sigProgress({true, 100 * i / revs.size(),
                          "Extracting Logic of Revision " + QString::number(revs[i]->Id())});

        rev->invalidateScopeCache();

        // Remove previously deleted stuff
        if (rev->PrevRevision() != 0) {
            qDebug() << "\tRemoving deleted files from ScratchDir..";
            foreach(FileNode *f, rev->FileCache()) {
                if (f->Operation() == 'D') {
                    qDebug() << "\tDeleting" << scratchDir.path() + f->toString();
                    if (f->Type() == TYPE_DIR)
                        QDir(scratchDir.path() + f->toString()).removeRecursively();
                    else
                        QFile::remove(scratchDir.path() + f->toString());
                }
            }
        }

        // A. Prepare ScratchDir
        QDir baseDir (rev->Path());

        // Check whether the dir exists
        if (!baseDir.exists()) {
            qDebug() << "\tPath does not exist, skipping..";
            continue;
        }
        // Copy BaseFiles to ScratchDir (-1 should always be first)
        qDebug() << "\tCopying files to ScratchDir..";
        copyDir(baseDir.path(), scratchDir.path());

        // B. Let Doxygen generate its output
        if (doxyDir.exists()) {
            qDebug() << "\t!!! Deleting previous DoxygenOutput dir" << Qt::endl;
            doxyDir.removeRecursively();
        }
        if (!this->generateDoxygenOutput(rev)) {
            qDebug() << "### Failed generating DoxygenOutput for Revision" << rev->Id() << Qt::endl;
        }

        // C. Process the output to logicTree
        if (!this->processDoxygenOutput(rev)) {
            qDebug() << "### Failed reading LogicTree for Revision" << rev->Id() << Qt::endl;
        }

        // D. Refine operations performed on the scopes
        if (!this->refineOperations(rev)) {
            qDebug() << "### Failed refining scope operations for Revision" << rev->Id() << Qt::endl;
        }

        // E. DELETE REVISION'S NON-HEADER FILES FROM SCRATCHDIR
        foreach(FileNode *f, rev->FileCache()) {
            if (f->Type() == TYPE_FILE && !f->Name().endsWith(".h"))
                QFile::remove(scratchDir.path() + f->toString());
        }

        qDebug() << "### Succesfully generated and read LogicTree for Revision" << rev->Id() << Qt::endl;

        rev->invalidateScopeCache();
    }

    emit sigProgress({true, 100, "Extracted Logic for Revisions " +
                      QString::number(startRev) + " to " + QString::number(endRev)});

    return true;
}

bool ScopeExtractor::generateDoxygenOutput(RevisionNode* revNode)
{
    Q_UNUSED(revNode); // The default execution dir is scratchdir nowadays

    // Export config from resources if it doesnt exist
    if (!QFile::exists("doxyfile")) {
        QFile::copy(":/doxyfile", "doxyfile");
    }

    qDebug() << "### Starting doxygen in" << m_doxygen->workingDirectory();

    // Prepare doxygen arguments
    QStringList arguments;
    arguments << "../../doxyfile";

    // Execute doxygen
    m_doxygen->start("doxygen", arguments);
    m_doxygen->waitForFinished(-1);

    // Return depending on success of execution
    if (m_doxygen->exitCode() == 0) {
        m_doxygen->waitForReadyRead(-1);
        return true;
    } else {
        qDebug() << "ERROR! Failed to run Doxygen!";
        return false;
    }
}

bool ScopeExtractor::processDoxygenOutput(RevisionNode* revNode)
{
    // Get all doxygen generated xml files
    QStringList nameFilters;    nameFilters << "*.xml";

    QDir        revDir(m_dataStore->Name() + "/DoxygenOutput/xml/");
    QStringList fileNames = revDir.entryList(nameFilters, QDir::Files);
    QString     filePath;

    // Process the files
    for (int i=0; i<fileNames.count(); ++i) {
        if (!m_dataStore->IsRunning()) return false;

        filePath = revDir.path() + "/" + fileNames[i];
        processDoxygenFile(revNode, filePath);
    }

    return true;
}

bool ScopeExtractor::processDoxygenFile (RevisionNode* revNode, QString fileName)
{
    QFile           doxyFile    (fileName);
    QDomDocument    doxyIndex   ("DoxyIndex");

//    qDebug() << "\tProcessing Doxygen file:" << fileName;

    // Open, read and close file
    if (!doxyFile.open(QIODevice::ReadOnly)) return false;
    // Set XML content
    if (!doxyIndex.setContent(&doxyFile)) {
        doxyFile.close();
        return false;
    }
    doxyFile.close();

    // Parse file
    QDomElement docElem = doxyIndex.documentElement();
    processDomNode(revNode->RootScope(), revNode, docElem);

    return true;
}

bool ScopeExtractor::processDomNode(ScopeNode* parent, RevisionNode* revNode,
                                    const QDomNode &domNode)
{
    bool        output      = false;

    QString     cwd         = QDir().absolutePath() + "/" + m_dataStore->Name() + "/Scratch";
    FileNode*   fileRoot    = revNode->RootFile();
    ScopeNode*  newNode     = 0;

    /*** Nodes that should have location **************************************/
    if (domNode.nodeName() == "memberdef" || domNode.nodeName() == "compounddef") {
        QDomNode location = domNode.namedItem("location");
        // Properties of DomNode and therewith future ScopeNode
        QString     name, kind, defFile, bodyFile;
        int         defLine, startLine, endLine;
        bool        hasBody;

        /*** Get attributes of domNode ****************************************/
        name        = domNode.namedItem("compoundname").toElement().text();    // Class, Namespace, File
        if (name == "") name = domNode.namedItem("name").toElement().text();    // Func, Attrib
        name.replace("::", "><"); // Java package fix

        kind        = domNode.attributes().namedItem("kind").nodeValue();

        defFile     = location.attributes().namedItem("file").nodeValue().replace(cwd, "");
        defLine     = location.attributes().namedItem("line").nodeValue().toInt();

        bodyFile    = location.attributes().namedItem("bodyfile").nodeValue().replace(cwd, "");
        startLine   = location.attributes().namedItem("bodystart").nodeValue().toInt();
        endLine     = location.attributes().namedItem("bodyend").nodeValue().toInt();

        hasBody     = endLine > 0;

        /*** Get related FileNodes ********************************************/
        FileNode*   defNode = fileRoot->getChild(defFile);
        FileNode*   impNode = fileRoot->getChild(bodyFile);

        NodeType type = TYPE_LOGIC;
        if     (kind == "dir")      type = TYPE_DIR;
        else if (kind == "file")     type = TYPE_FILE;
        else if (kind == "function") type = TYPE_FUNCTION;
        else if (kind == "slot")     type = TYPE_SLOT;
        else if (kind == "variable") type = TYPE_ATTRIBUTE;
        else if (kind == "define")   type = TYPE_DEFINE;
        else if (kind == "class")    type = TYPE_CLASS;
        else if (kind == "enum")     type = TYPE_ENUM;
        else if (kind == "namespace")type = TYPE_NAMESPACE;
        else if (kind == "signal")   type = TYPE_SIGNAL;

        newNode = new ScopeNode(parent, name, type);
        //! \todo: enumvalue

        // Set the properties
        newNode->m_defLine        = defLine;
        if (hasBody) {
            newNode->m_impStartLine   = startLine;
            newNode->m_impEndLine     = endLine;
        }

        if (defNode != 0) {
            if (type == TYPE_DIR || type == TYPE_FILE) {
                newNode->setDefFile(defNode, defLine);
                defNode->addDef(newNode);
            } else {
                newNode->setImpFile(defNode, defLine, defLine);
                defNode->addImp(newNode);
            }
            newNode->setOperation(defNode->Operation());
        }

        if (impNode != 0) {
            newNode->setImpFile(impNode, startLine, endLine);
            newNode->setOperation(impNode->Operation());
            impNode->addImp(newNode);
        }

        // Process children
        QDomNodeList childList = domNode.childNodes();
        for (int i=0; i < childList.count(); ++i) {
            processDomNode(newNode, revNode, childList.at(i));
        }

        // If we have defNode, impNode OR children, add to parent, else discard
        if (defNode != 0 || impNode != 0 || newNode->m_children.count() > 0) {
            parent->addChild(newNode);
            output = true;
        } else {
            delete newNode;
            output = false;
        }
    }

    /*** Process children of potentially relevant nodes ***********************/
    else if ( domNode.nodeName() == "sectiondef" || domNode.nodeName() == "doxygen" ) {
        QDomNodeList childList = domNode.childNodes();
        for (int i=0; i < childList.count(); ++i) {
            processDomNode(parent, revNode, childList.at(i));
        }
    }

    return output;
}

bool ScopeExtractor::refineOperations(RevisionNode *currRev)
{
    RevisionNode *prevRev = currRev->PrevRevision();

    // Sanity check
    if (prevRev == 0) return false;

    // Refine modified nodes as added
    ScopeNode *currScope = 0, *prevScope = 0;
    //! \todo: Maybe use iterators instead?
    foreach(QString scopeName, currRev->ScopeCache().keys()) {
        currScope = currRev->ScopeCache()[scopeName];
        prevScope = prevRev->ScopeCache()[scopeName];

        // If the scope was modified and did not exist in prev revision,
        // then it was added
        if (currScope->Operation() == 'M' && prevScope == 0) {
            currScope->setOperation('A');
        }
    }

    // Refine modified nodes as deleted
    FileNode *prevFile, *currFile;
    foreach(QString scopeName, prevRev->ScopeCache().keys()) {
        prevScope = prevRev->ScopeCache()[scopeName];

        prevFile = prevScope->ImpFile();
        if (prevFile == 0) prevFile = prevScope->DefFile();
        if (prevFile == 0) continue;
        currFile = currRev->FileCache()[prevFile->toString()];

        // If the file still exits AND (it was deleted OR
        // (the scope is missing AND is not deleted) )
        bool isDeleted = currFile != 0 && (currFile->Operation() == 'D' ||
                (currScope == 0 && prevScope->Operation() != 'D')) ;

        if (isDeleted) currRev->addDeletedScope(prevScope);
    }

    return true;
}
