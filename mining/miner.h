#ifndef MINER_H
#define MINER_H

#include <QObject>

#include "progressinfo.h"

class CloneExtractor;
class DataStore;
class RepositoryDownloader;
class ScopeExtractor;

/*! ***********************************************************************************************
 * \author  Avdo Hanjalic
 * \date    15.07.2013
 * \class   Miner
 *
 * \brief   This class is used to start the mining process in a separate thread.
 **************************************************************************************************/
class Miner : public QObject
{
    Q_OBJECT

public:
    /* ****************************************************************************************
     * Constructor & Destructor
     ******************************************************************************************/
    explicit Miner(DataStore *ds);

    /* ****************************************************************************************
     * Getters & Setters
     ******************************************************************************************/
    DataStore           *dataStore() const                          { return m_dataStore; }
    
public slots:
    /* ****************************************************************************************
     * Public Slots
     ******************************************************************************************/
    void                getLogs();
    void                getDetails(int minRev, int maxRev);

signals:
    /* ****************************************************************************************
     * Signals
     ******************************************************************************************/
    void                sigProgress    (ProgressInfo);
    void                sigStepProgress(ProgressInfo);

private:
    /* ****************************************************************************************
     * Private Functions
     ******************************************************************************************/
    void                initialize();

private slots:
    /* ****************************************************************************************
     * Private Slots
     ******************************************************************************************/
    void                onRDSigProgress(ProgressInfo info);         // RepoDownloader progress
    void                onSXSigProgress(ProgressInfo info);         // ScopeExtractor progress
    void                onCXSigProgress(ProgressInfo info);         // CloneExtractor progress

private:
    /* ****************************************************************************************
     * Attrributes
     ******************************************************************************************/
    DataStore           *m_dataStore;
    RepositoryDownloader*m_repoDownloader;
    ScopeExtractor      *m_scopeExtractor;
    CloneExtractor      *m_cloneExtractor;

    bool                m_isInitialized;
};

#endif // MINER_H
