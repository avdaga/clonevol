#include "miner.h"

#include <QDebug>
#include <QElapsedTimer>

#include "datastore/datastore.h"
#include "mining/cloneextractor.h"
#include "mining/databasewriter.h"
#include "mining/repositorydownloader.h"
#include "mining/scopeextractor.h"

/* ************************************************************************************************
 * Constructor & Destructor
 **************************************************************************************************/
Miner::Miner(DataStore* ds) :
    QObject         (0)
  , m_dataStore     (ds)
  , m_repoDownloader(0)
  , m_scopeExtractor(0)
  , m_cloneExtractor(0)
  , m_isInitialized (false)
{
    /*! ***************************************************************************************
     * Constructor
     ******************************************************************************************/
    qRegisterMetaType<ProgressInfo>("ProgressInfo");
}

/* ************************************************************************************************
 * Public Slots
 **************************************************************************************************/
void Miner::getLogs()
{
    /*! ***************************************************************************************
     * Obtains file-level information from the repo's changelogs.
     ******************************************************************************************/
    if (!m_isInitialized) { initialize(); }

    double logTime = 0, dbTime = 0;

    m_dataStore->setRunning(true);

    QElapsedTimer myTimer;
    myTimer.start();

    /* 1. Attempt to connectt to repository
     ******************************************************************************************/
    emit sigProgress({true,0,"Connecting to repository..."});

    if (!m_repoDownloader->getInfo()) {
        emit sigProgress({false,-1,"Could not connect to repository :("});
        return;
    }

    /* 2. Process logs
     ******************************************************************************************/
    emit sigProgress({true,10,"Acquiring Logs..."});

    if (!m_repoDownloader->getLogs()) {
        emit sigProgress({false,-1,"Could not acquire logs :("});
        return;
    }

    logTime = myTimer.elapsed() / 1000.0;

    qDebug() << Qt::endl << "Time elapsed (sec) after acquiring logs: " << logTime;

    /* 3. Store information in database
     ******************************************************************************************/
    emit sigProgress({true,90,"Storing data to DataBase..."});

    DataBaseWriter::writeToDB(m_dataStore);
    dbTime = myTimer.elapsed() / 1000.0;

    qDebug() << "Time elapsed (sec) after writing to DataBase: " << dbTime;

    /* 0. Report on used time
     ******************************************************************************************/
    emit sigProgress({true,100,"Log acquisition complete!"});

    qDebug() << Qt::endl << "Finished my job, bbye!" << Qt::endl;
}

void Miner::getDetails(int minRev, int maxRev)
{
    /*! ***************************************************************************************
     * Obtains file-content-level information by processing the files.
     ******************************************************************************************/
    if (!m_isInitialized) { initialize(); }

    double dlTime = 0, lxTime = 0, cxTime = 0, dbTime = 0;

    m_dataStore->setRunning(true);

    QElapsedTimer myTimer;
    myTimer.start();

    /* 1. Download files
     ******************************************************************************************/
    emit sigProgress({true,0,"Downloading Files..."});

    m_repoDownloader->getRevisions(minRev, maxRev);
    dlTime = myTimer.elapsed() / 1000.0;

    qDebug() << "Time elapsed (sec) after Downloading: " << dlTime;

    // Stop if operation was aborted
    if (!m_dataStore->IsRunning()) return;

    /* 2. Extract scopes using doxygen
     ******************************************************************************************/
    emit sigProgress({true,30,"Extracting Scopes..."});

    m_scopeExtractor->getLogic(minRev, maxRev);
    lxTime = myTimer.elapsed() / 1000.0;

    qDebug() << "Time elapsed (sec) after Logic Extraction: " << lxTime;

    // Stop if operation was aborted
    if (!m_dataStore->IsRunning()) return;

    /* 3. Extract clones using simian
     ******************************************************************************************/
    emit sigProgress({true,60,"Extracting Clones..."});

    m_cloneExtractor->getClones(minRev, maxRev);
    cxTime = myTimer.elapsed() / 1000.0;

    qDebug() << "Time elapsed (sec) after Clone Extraction: " << cxTime;

    // Stop if operation was aborted
    if (!m_dataStore->IsRunning()) return;

    /* 4. Store information in database
     ******************************************************************************************/
    emit sigProgress({true,90,"Storing data to DataBase..."});

    DataBaseWriter::writeToDB(m_dataStore);
    dbTime = myTimer.elapsed() / 1000.0;

    qDebug() << "Time elapsed (sec) after writing to DataBase: " << dbTime;

    /* 0. Report on used time
     ******************************************************************************************/
    emit sigProgress({true,100,"Details for revision " + QString::number(minRev) +
                            " to " + QString::number(maxRev) + " acquired!"});

    qDebug() << Qt::endl << "Total time needed for steps" << Qt::endl
             << "Download:" << dlTime << Qt::endl
             << "LogicX:  " << lxTime << Qt::endl
             << "CloneX:  " << cxTime << Qt::endl
             << "DataBase:" << dbTime << Qt::endl;

    qDebug() << Qt::endl << "Finished my job, bbye!" << Qt::endl;
}

/* ************************************************************************************************
 * Private Functions
 **************************************************************************************************/
void Miner::initialize()
{
    /*! ***************************************************************************************
     * Initialization: create different pipes and hook up signals
     ******************************************************************************************/
    if (!m_isInitialized) {
        // Init RepositoryDownloader
        if (m_repoDownloader == nullptr) {
            m_repoDownloader  = new RepositoryDownloader(m_dataStore);
            connect(m_repoDownloader, &RepositoryDownloader::sigProgress, this, &Miner::onRDSigProgress);
        }
        // Init ScopeExtractor
        if (m_scopeExtractor == nullptr) {
            m_scopeExtractor  = new ScopeExtractor(m_dataStore);
            connect(m_scopeExtractor, &ScopeExtractor::sigProgress, this, &Miner::onSXSigProgress);
        }
        // Init CloneExtractor
        if (m_cloneExtractor == nullptr) {
            m_cloneExtractor  = new CloneExtractor(m_dataStore);
            connect(m_cloneExtractor, &CloneExtractor::sigProgress, this, &Miner::onCXSigProgress);
        }

        m_isInitialized = true;
    }
}

/* ************************************************************************************************
 * Private Slots
 **************************************************************************************************/
void Miner::onRDSigProgress(ProgressInfo info)
{
    /*! ***************************************************************************************
     * Propagate RepositoryDownloader progress.
     ******************************************************************************************/
    emit sigStepProgress(info);
    emit sigProgress({info.status, 0 + 0.3 * info.percent, ""});
}

void Miner::onSXSigProgress(ProgressInfo info)
{
    /*! ***************************************************************************************
     * Propagate ScopeExtractor progress.
     ******************************************************************************************/
    emit sigStepProgress(info);
    emit sigProgress({info.status, 30 + 0.3 * info.percent, ""});
}

void Miner::onCXSigProgress(ProgressInfo info)
{
    /*! ***************************************************************************************
     * Propagate CloneExtractor progress.
     ******************************************************************************************/
    emit sigStepProgress(info);
    emit sigProgress({info.status, 60 + 0.3 * info.percent, ""});
}
