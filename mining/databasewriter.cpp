#include "databasewriter.h"

#include <QFile>
#include <QString>
#include <QtSql>
#include <QVariantList>

#include "datastore/codeclone.h"
#include "datastore/datastore.h"
#include "datastore/filenode.h"
#include "datastore/revisionnode.h"
#include "datastore/scopeclone.h"
#include "datastore/scopenode.h"

/* ************************************************************************************************
 * Public Functions
 **************************************************************************************************/
void DataBaseWriter::writeToDB(DataStore *ds)
{
    QString fileName = ds->Name() + ".sqlite";

    // Make new file if neccessary
    if (!QFile::exists(fileName)) {
        initDataBaseFile(fileName);
    }

    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE", fileName);
    db.setDatabaseName(fileName);
    db.open();

    //! \todo: make optional (can break database)
    db.exec("PRAGMA synchronous  = NORMAL");
    db.exec("PRAGMA journal_mode = WAL");

    // Write Revisions
    writeDataStore  (ds, db);
    writeRevisions  (ds, db);
    writeFiles      (ds, db);
    writeScopeClones(ds, db);
    writeScopes     (ds, db);

    // Clean up
//    db.exec("DELETE FROM ScopeClone WHERE sourceName = targetName");
    db.exec("DELETE FROM Scope WHERE fileName = \"\"");
}

/* ************************************************************************************************
 * Private Functions
 **************************************************************************************************/
void DataBaseWriter::initDataBaseFile(const QString &filename)
{
    /*! ***************************************************************************************
     * Force-copies the empty database (in resources) to filename.
     *
     * \note QFile::copy() has a delay when copying from resource, which breaks the program.
     ******************************************************************************************/
    // Open empty db and target db
    QFile sourceDBFile(":/sqlitedb/db.sqlite");
    QFile targetDBFile(filename);

    sourceDBFile.open(QFile::ReadOnly);
    targetDBFile.open(QFile::WriteOnly);

    // Force copy all data
    targetDBFile.write(sourceDBFile.readAll());
    targetDBFile.flush();

    // Finish up
    targetDBFile.close();
    sourceDBFile.close();
}

void DataBaseWriter::writeDataStore(DataStore *ds, QSqlDatabase &db)
{
    // Query preparation and execution
    QSqlQuery query(db);
    query.prepare("INSERT INTO DataStore (dataStoreName, URL) "
                  "VALUES(?, ?)");
    query.bindValue(0,  ds->Name());
    query.bindValue(1,  ds->Url());
    query.exec();
}

void DataBaseWriter::writeRevisions(DataStore *ds, QSqlDatabase &db)
{
    // Pre-processing for batch insert
    QVariantList ids, authors, logs, rootFiles, rootScopes, acquired;
    foreach(RevisionNode* r, ds->Revisions()) {
        ids         << r->Id();
        authors     << "";          //! \todo: Author name here
        logs        << "";          //! \todo: Log message here
        rootFiles   << r->RootFile()->toString();
        rootScopes  << r->RootScope()->toString();
        acquired    << r->IsAcquired();
    }

    // Query preparation and execution
    QSqlQuery query(db);
    query.prepare("INSERT OR IGNORE INTO Revision "
                  "(revisionNr, authorName, logMessage, rootFileName, rootScopeName, isAcquired)"
                  "VALUES (?, ?, ?, ?, ?, ?)");
    query.addBindValue(ids);
    query.addBindValue(authors);
    query.addBindValue(logs);
    query.addBindValue(rootFiles);
    query.addBindValue(rootScopes);
    query.addBindValue(acquired);

    query.execBatch();

    // Update previously ignored revisions
    query.prepare("UPDATE Revision SET isAcquired = isAcquired + ? "
                  "WHERE revisionNr = ?");
    query.addBindValue(acquired);
    query.addBindValue(ids);
    query.execBatch();
}

void DataBaseWriter::writeFiles(DataStore *ds, QSqlDatabase &db)
{
    // Pre-processing for batch insert
    QVariantList ids, names, parents, types, operations;
    foreach(RevisionNode* r, ds->Revisions()) {
        foreach(FileNode* f, r->FileCache()) {
            ids         << r->Id();
            names       << f->toString();
            parents     << (f->Parent() == 0 ? "" : f->Parent()->toString());
            types       << f->Type();
            // Prioritize operations for diff colormap so they can be ordered
            switch(f->Operation()) {
                case 'D': operations  << 3; break;
                case 'A': operations  << 2; break;
                case 'M': operations  << 1; break;
                default:  operations  << 0; break;
            }
        }
    }

    // Query preparation and execution
    QSqlQuery query(db);
    query.prepare("INSERT OR IGNORE INTO File (revisionNr, fileName, parentName, FNType, operation)"
                  "VALUES (?, ?, ?, ?, ?)");
    query.addBindValue(ids);
    query.addBindValue(names);
    query.addBindValue(parents);
    query.addBindValue(types);
    query.addBindValue(operations);

    query.execBatch();
}

void DataBaseWriter::writeScopes(DataStore *ds, QSqlDatabase &db)
{
    // Pre-processing for batch insert
    QVariantList ids, names, parents, files, types, operations;
    foreach(RevisionNode* r, ds->Revisions()) {
        // First write all deleted nodes
        foreach (ScopeNode* s, r->DeletedScopes()) {
            ids         << r->Id();
            names       << s->toString();
            parents     << (s->Parent()  == 0 ? "" : s->Parent()->toString());
            files       << (s->ImpFile() == 0 ? "" : s->ImpFile()->toString());
            types       << s->Type();
            operations  << 3;
        }

        foreach(ScopeNode* s, r->ScopeCache()) {
            ids         << r->Id();
            names       << s->toString();
            parents     << (s->Parent()  == 0 ? "" : s->Parent()->toString());
            files       << (s->ImpFile() == 0 ? "" : s->ImpFile()->toString());
            types       << s->Type();
            // Prioritize operations for diff colormap so they can be ordered
            switch(s->Operation()) {
                case 'D': operations  << 3; break;
                case 'A': operations  << 2; break;
                case 'M': operations  << 1; break;
                default:  operations  << 0; break;
            }
        }
    }

    // Query preparation and execution
    QSqlQuery query(db);
    query.prepare("INSERT OR IGNORE INTO Scope (revisionNr, scopeName, parentName, fileName, SNType, operation)"
                  "VALUES (?, ?, ?, ?, ?, ?)");
    query.addBindValue(ids);
    query.addBindValue(names);
    query.addBindValue(parents);
    query.addBindValue(files);
    query.addBindValue(types);
    query.addBindValue(operations);

    query.execBatch();
    // qDebug() << query.lastError();
    // qDebug() << query.lastError().text();
}

void DataBaseWriter::writeScopeClones(DataStore *ds, QSqlDatabase &db)
{
    // Pre-processing for batch insert
    QVariantList tarIds, targets, srcIds, sources, driftTypes, intraTypes, locs, isIntras;
    foreach(RevisionNode* r, ds->Revisions()) {
        // Deleted Intra-clones
        foreach(ScopeClone *sc, r->DeletedScopeClones()) {
            tarIds      << r->Id();
            targets     << sc->Target()->toString();
            srcIds      << r->Id();
            sources     << sc->Source()->toString();
            driftTypes  << 0;
            intraTypes  << 'D';
            locs        << sc->NLines();        //! \todo: Calculate clone lines
            isIntras    << true;
        }

        // Intra-clones
        foreach(ScopeClone *sc, r->IntraCloneCache()) {
            tarIds      << sc->Target()->RevisionId();
            targets     << sc->Target()->toString();
            srcIds      << sc->Source()->RevisionId();
            sources     << sc->Source()->toString();
            driftTypes  << 0;
            intraTypes  << sc->IntraType();
            locs        << sc->NLines();        //! \todo: Calculate clone lines
            isIntras    << true;
        }
        // Inter-clones
        foreach(ScopeClone *sc, r->InterCloneCache()) {
            tarIds      << sc->Target()->RevisionId();
            targets     << sc->Target()->toString();
            srcIds      << sc->Source()->RevisionId();
            sources     << sc->Source()->toString();
            driftTypes  << sc->DriftType();
            intraTypes  << 0;
            locs        << sc->NLines();        //! \todo: Calculate clone lines
            isIntras    << false;
        }
    }

    // Query preparation and execution
    QSqlQuery query(db);
    query.prepare("INSERT OR IGNORE INTO ScopeClone (targetRevisionNr, targetName, sourceRevisionNr, sourceName, driftType, intraType, matchLines, isIntra)"
                  "VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
    query.addBindValue(tarIds);
    query.addBindValue(targets);
    query.addBindValue(srcIds);
    query.addBindValue(sources);
    query.addBindValue(driftTypes);
    query.addBindValue(intraTypes);
    query.addBindValue(locs);
    query.addBindValue(isIntras);

    query.execBatch();

    // qDebug() << query.lastError();
    // qDebug() << query.lastError().text();
}
