#ifndef REPOSITORYDOWNLOADER_H
#define REPOSITORYDOWNLOADER_H

#include <QObject>
#include <QtCore/QEventLoop>
#include <QtCore/QHash>
#include <QtCore/QList>
#include <QtCore/QObject>
#include <QtCore/QProcess>
#include <QtCore/QString>
#include <QtXml/QDomNode>

#include "progressinfo.h"

class DataStore;
class FileNode;
class RevisionNode;

/*! ***********************************************************************************************
 * \author  Avdo Hanjalic
 * \date    03.01.2021
 * \class   RepositoryDownloader
 *
 * \brief   This class performs communication with the Git repository and acquires files for each
 *          revision.
 **************************************************************************************************/
class RepositoryDownloader : public QObject {

    Q_OBJECT

public:
    /* ****************************************************************************************
     * Constructor & Destructor
     ******************************************************************************************/
    RepositoryDownloader(DataStore *ds);

    /* ****************************************************************************************
     * Public Functions
     ******************************************************************************************/
    bool                getInfo();
    bool                getLogs();
    bool                getRevisions(int startRev, int endRev);

signals:
    /* ****************************************************************************************
     * Signals
     ******************************************************************************************/
    void                sigProgress(ProgressInfo);

private:
    /* ****************************************************************************************
     * Private Functions
     ******************************************************************************************/
    // Input handling functions
    bool                getBase         (int revision);
    bool                getFiles        (int revision, bool fullRev = false);

    void                processBase     (const QByteArray &base);
    void                processLog      (const QByteArray &log);
    void                processLogRecord(const QByteArray &logLine, int revNr);

    // Helper functions
    bool                isFileExtensionSupported(const QString &filePath);
    QString             repodir() const;

private:
    /* ****************************************************************************************
     * Attrributes
     ******************************************************************************************/
    // Repository information
    DataStore           *m_dataStore;

    int                 m_nRevisions;                               //!< Revisions in repository
    bool                m_isInfoObtained;

    // External process communication
    QProcess            *m_gitProc;                                 //!< QProcess handle for git
};


#endif // REPOSITORYDOWNLOADER_H
