#ifndef DATABASEWRITER_H
#define DATABASEWRITER_H

class QSqlDatabase;
class DataStore;
class QString;

/*! ***********************************************************************************************
 * \author  Avdo Hanjalic
 * \date    27.03.2012
 * \class   DataBaseWriter
 * \brief   Writes a datastore to a SQL(ite) database
 *
 * \todo    This should become the interface for reading the DB as well, to solve the current
 *          multi-threaded DB access complications.
 **************************************************************************************************/
class DataBaseWriter
{
public:
    /* ****************************************************************************************
     * Public Functions
     ******************************************************************************************/
    static void         writeToDB       (DataStore* ds);

private:
    /* ****************************************************************************************
     * Private Functions
     ******************************************************************************************/
    static void         initDataBaseFile(const QString &filename);

    static void         writeDataStore  (DataStore* ds, QSqlDatabase &db);
    static void         writeRevisions  (DataStore* ds, QSqlDatabase &db);
    static void         writeFiles      (DataStore* ds, QSqlDatabase &db);
    static void         writeScopes     (DataStore* ds, QSqlDatabase &db);
    static void         writeScopeClones(DataStore* ds, QSqlDatabase &db);
};

#endif // DATABASEWRITER_H
