#include "cloneextractor.h"

#include <QtCore/QCoreApplication>
#include <QtCore/QDebug>
#include <QtCore/QDir>
#include <QtXml/QDomDocument>

#include "datastore/codeclone.h"
#include "datastore/datastore.h"
#include "datastore/filenode.h"
#include "datastore/revisionnode.h"
#include "datastore/scopeclone.h"
#include "datastore/scopenode.h"

/* ************************************************************************************************
 * Constructor & Destructor
 **************************************************************************************************/
CloneExtractor::CloneExtractor(DataStore *ds) :
    QObject     (0)
  , m_dataStore (ds)
  , m_simian    (new QProcess())
{}

/* ************************************************************************************************
 * Public Functions
 **************************************************************************************************/
bool CloneExtractor::getClones(int startRev, int endRev)
{
    qDebug() << "###############################################################";
    qDebug() << "### Generating simian output for revisions...";
    qDebug() << "###############################################################";

    // Make list of revisions to acquire
    QList<RevisionNode*> revs;
    foreach(RevisionNode* rev, m_dataStore->Revisions()) {
        // Process only assigned revisions
        if (rev->Id() < startRev-1 || rev->Id() > endRev)
            continue;
        else
            revs << rev;
    }

    m_simian->setWorkingDirectory(m_dataStore->Name());

    // GENERATE INTRA-CLONES
    //! \todo: make conditional
    for (qreal i = 0; i < revs.count(); ++i) {
        if (!m_dataStore->IsRunning()) return false;

        RevisionNode* rev = revs[i];

        emit sigProgress({true, 100 * i / revs.size(),
                          "Extracting Clones of Revision " + QString::number(revs[i]->Id())});

        rev->invalidateIntraCloneCache();
        rev->invalidateInterCloneCache();

        qDebug() << "### Generating Intra-CodeClones for Revision" << rev->Id() << Qt::endl;
        // A.  Let Simian generate Intra-Clones
        if (!this->generateIntraClones(rev)) {
            qDebug() << "### Failed generating Intra-clones for Revision" << rev->Id() << Qt::endl;
        }
        // B. Process the output to codeClones
        if (!this->processSimianOutput(rev,rev)) {
            qDebug() << "### Failed reading CodeClones for Revision" << rev->Id() << Qt::endl;
            continue;
        }
        qDebug() << "### Succesfully generated and read Intra-CodeClones for Revision" << rev->Id() << Qt::endl;

        // C. If there is a previous revision, generate Inter-clones
        if (rev->PrevRevision() != 0 && rev->PrevRevision()->Id() >= startRev) {
            qDebug() << "### Generating Inter-CodeClones for Revision" << rev->Id() << rev->PrevRevision()->Id() << Qt::endl;
            // D. Let Simian generate Inter-Clones
            if (!this->generateInterClones(rev, rev->PrevRevision())) {
                qDebug() << "### Failed generating Inter-clones for Revisions" << rev->Id() << rev->PrevRevision()->Id() << Qt::endl;
            }
            // E. Process the output to codeClones
            if (!this->processSimianOutput(rev, rev->PrevRevision())) {
                qDebug() << "### Failed reading CodeClones for Revisions" << rev->Id() << rev->PrevRevision()->Id() << Qt::endl;
                continue;
            }
            qDebug() << "### Succesfully generated and read Inter-CodeClones for Revisions" << rev->Id() << rev->PrevRevision()->Id() << Qt::endl;
        }

        // F. First remove inter-clones colliding with intra-clones
        cleanInterClones(rev);
        // G. Remove duplicate Intra-Clones (A --> B has also B --> A)
        cleanIntraClones(rev);

        // H. Refine operations (requires a previous revision to exist)
        if (!this->refineOperations(rev)) {
            qDebug() << "### Failed refining operations on ScopeClones for" << rev->Id() << Qt::endl;
        }

        rev->setAcquired();
    }

    emit sigProgress({true, 100, "Extracted Clones for Revisions " +
                      QString::number(startRev) + " to " + QString::number(endRev)});

    return true;
}

/* ************************************************************************************************
 * Private Functions
 **************************************************************************************************/
bool CloneExtractor::generateIntraClones(RevisionNode *rev)
{
    // Export config from resources if it doesnt exist
    if (!QFile::exists("simian.conf")) {
        QFile::copy(":/simian.conf", "simian.conf");
    }

    qDebug() << "### Starting Simian in" << m_simian->workingDirectory();
    qDebug() << "### Generating intra-clones";

    // *** look for intraclones
    // Prepare Simian arguments
    QStringList arguments {
        "-config=../simian.conf",
        QString::number(rev->Id()) + "/*",
        QString::number(rev->Id()) + "/**/**"
    };

    // Execute Simian
#ifdef Q_OS_WINDOWS
    m_simian->start("simian", arguments);
#else
    m_simian->start("java", QStringList{"-jar", "../simian.jar"} + arguments);
#endif
    m_simian->waitForFinished(-1);
    m_simian->waitForReadyRead(-1);

    // Return depending on success of execution
    if (m_simian->exitCode() == 0) {
        return true;
    } else {
        qDebug() << "ERROR! Failed to run Simian!";
        return false;
    }
}

bool CloneExtractor::generateInterClones(RevisionNode* revA, RevisionNode* revB)
{
    // Export config from resources if it doesnt exist
    if (!QFile::exists("simian.conf")) {
        QFile::copy(":/simian.conf", "simian.conf");
    }

    qDebug() << "### Starting Simian in" << m_simian->workingDirectory();
    qDebug() << "### Generating inter-clones";

    // *** Now look for interclones
    // Prepare Simian arguments
    QStringList arguments {
        "-config=../simian.conf",
        QString::number(revA->Id()) + "/*",
        QString::number(revA->Id()) + "/**/**",
        QString::number(revB->Id()) + "/*",
        QString::number(revB->Id()) + "/**/**"
    };

    // Execute Simian
#ifdef Q_OS_WINDOWS
    m_simian->start("simian", arguments);
#else
    m_simian->start("java", QStringList{"-jar", "../simian.jar"} + arguments);
#endif
    m_simian->waitForFinished(-1);
    m_simian->waitForReadyRead(-1);

    // Return depending on success of execution
    if (m_simian->exitCode() == 0) {
        return true;
    } else {
        qDebug() << "ERROR! Failed to run Simian!";
        return false;
    }
}

bool CloneExtractor::processSimianOutput(RevisionNode *revA, RevisionNode *revB)
{
    QDomDocument    simianXML       ("SimianXML");
    QString         simianOutput    = m_simian->readAll();

    // Remove prependix
    simianOutput.remove(0, simianOutput.indexOf("<?xml version"));
    simianXML.setContent(simianOutput);

    //qDebug() << simianOutput;
    //qDebug() << simianXML.toString();

    // Descend to level of <set />'s
    QDomElement     simianRoot      = simianXML.documentElement();
    QDomNodeList    simianClones    = simianRoot.firstChild().childNodes();

    // Process all <set />'s
    for (int i = 0; i < simianClones.count(); ++i) {
        if (simianClones.at(i).nodeName() == "set") {
            processDomNode(simianClones.at(i), revA, revB);
        }
    }

    return true;
}

bool CloneExtractor::processDomNode(const QDomNode &domNode, RevisionNode *revA, RevisionNode *revB)
{
    qDebug() << "    CloneSet";

    QString             cwd     = QDir().absolutePath() + "/" + m_dataStore->Name() + "/";
    QDomNodeList        blocks  = domNode.childNodes();
    QDomNamedNodeMap    block;

    QString         filePath;
    int             startLine, endLine;

    QList<CodeClone*>* cloneSet = 0;

    // Process all <block />'s
    for (int i = 0; i < blocks.count(); ++i) {
        if (blocks.at(i).nodeName() == "block") {
            block       = blocks.at(i).attributes();

            filePath    = block.namedItem("sourceFile").nodeValue();
            startLine   = block.namedItem("startLineNumber").nodeValue().toInt();
            endLine     = block.namedItem("endLineNumber").nodeValue().toInt();

            filePath.replace("\\","/");     // Fix simian output in windows
            filePath.replace(cwd, "");

            int revLoc  = filePath.indexOf('/');
            int revId   = filePath.left(revLoc).toInt();
            filePath.remove(0, revLoc);

            // Trace the actual FileNode
            RevisionNode*   cloneRev    = (revId == revA->Id()) ? revA : revB;
            FileNode*       fileNode    = cloneRev->RootFile()->getChild(filePath);

            // Create a CodeClone
            if (fileNode != 0) {
                // Sanity check
                if (cloneSet == 0) cloneSet = new QList<CodeClone*>();

                //!\todo: prevent duplicates from inter and intra codeclones
                CodeClone* newClone = new CodeClone(fileNode, startLine, endLine, cloneSet);
                cloneSet->append(newClone);

                // Register cloneset with target revision
                if (cloneRev == revB) fileNode->addCodeClone(newClone);

                qDebug()    << "\t" << revId << filePath << startLine << endLine;
            } else {
                qDebug() << "!! Could not trace FileNode";
                continue;
            }
        }
    }

    return true;
}

bool CloneExtractor::refineOperations(RevisionNode *currRev)
{
    RevisionNode *prevRev = currRev->PrevRevision();

    // If there is no previous revision, everything was added...
    if (prevRev == 0) {
        foreach(ScopeClone* currSc, currRev->IntraCloneCache()) {
            currSc->setIntraType('A');
        }
        return true;
    }

    // Find added intraclones (exist in currRev, not exist in prevRev)
    foreach(ScopeClone* currSc, currRev->IntraCloneCache()) {
        ScopeClone* prevSc = prevRev->IntraCloneCache()[currSc->toString()];
        if (prevSc == 0 || prevSc->IntraType() == 'D') {
            // Clone did not exist before
            if (currSc->Target()->Operation() != 0 || currSc->Source()->Operation() != 0)
                currSc->setIntraType('A');
        }
    }

    // Find deleted intraclones (not exist in currRev, exist in prevRev)
    foreach(ScopeClone* prevSc, prevRev->IntraCloneCache()) {
        // Sanity check
        if (prevSc->IntraType() == 'D') continue;

        ScopeClone* currSc = currRev->IntraCloneCache()[prevSc->toString()];
        ScopeNode* currTar = currRev->ScopeCache()[prevSc->Target()->toString()];
        if (currTar == 0) {
            currTar = currRev->DeletedScopes()[prevSc->Target()->toString()];
        }
        ScopeNode* currSrc = currRev->ScopeCache()[prevSc->Source()->toString()];
        if (currSrc == 0) {
            currSrc = currRev->DeletedScopes()[prevSc->Source()->toString()];
        }

        // Clone must be absent in current revision, but src and tar must exist
        bool isDeleted = (currSc == 0 && currSrc != 0 && currTar != 0);

        if (isDeleted) {
            currRev->addDeletedScopeClone(prevSc);
        }
    }

    // Handle interclones
    foreach(ScopeClone* sc, currRev->InterCloneCache()) {
        sc->setDriftType('D');

        //! \todo: Splits and merges
    }

    return true;
}

void CloneExtractor::cleanIntraClones(RevisionNode *rev)
{
    // Remove duplicate clones
    QList<ScopeClone*> duplicates;
    foreach(ScopeClone* sc, rev->IntraCloneCache()) {
        ScopeClone* dup = rev->IntraCloneCache()[sc->reverse()];

        if (sc->Source()->toString() == sc->Target()->toString()) {
            if (sc->Source()->ImpFile() == sc->Target()->ImpFile() &&
                    sc->Source()->DefFile() == sc->Target()->DefFile())
            {
                // It is a self-clone, remove both
                if (dup != 0)   duplicates << sc << dup;
                else            duplicates << sc;
            }
        } else if (dup != 0) {
            // Else remove only the duplicate
            duplicates << ((sc->toString() > dup->toString()) ? sc : dup);
        }
    }

    // Destroy all irrelevant clones
    foreach(ScopeClone* duplicate, duplicates) duplicate->destroy();
    rev->invalidateIntraCloneCache();
}

void CloneExtractor::cleanInterClones(RevisionNode *rev)
{
    RevisionNode *prevRev = rev->PrevRevision();
    QList<ScopeClone*> garbage;

    foreach(ScopeClone* sc, rev->InterCloneCache()) {
        ScopeClone* dup = rev->InterCloneCache()[sc->reverse()];

        if (sc->Source()->toString() == sc->Target()->toString()) {
            // If it is a self-clone, remove both
            if (dup != 0)   garbage << sc << dup;
            else            garbage << sc;
        } else if ( (rev->IntraCloneCache()[sc->toString()] != 0) ||
            (prevRev != 0 && prevRev->IntraCloneCache()[sc->toString()] != 0)) {
            // If an intraclone exists in prevRev or currRev, it is not a drift
            garbage << sc;
        }
    }

    // Destroy all irrelevant clones
    foreach(ScopeClone* duplicate, garbage) duplicate->destroy();
    rev->invalidateInterCloneCache();
}
