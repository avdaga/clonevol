#ifndef PROGRESSINFO_H
#define PROGRESSINFO_H

#include <QString>

/*! ***********************************************************************************************
 * \author  Avdo Hanjalic
 * \date    2013
 * \struct  ProgressInfo
 *
 * \brief   Message that's passed around to communicate progress updates from Miner to GUI.
 **************************************************************************************************/
struct ProgressInfo
{
    bool    status;
    qreal   percent;
    QString message;
};

#endif // PROGRESSINFO_H
