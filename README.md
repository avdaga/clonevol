# DISCLAIMER
THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF
THIS SOFTWARE.

# Changelog
- **v21.01**
  - Added Git support (SVN support will be added back later)
  - Added cross-platform support (linux)
  - Added OpenGL visualisation rendering
  - Preliminary code refactoring
- **v14.01**
  - First version as part of Master's thesis completion

# Installation of Dependencies
**ClonEvol is a toolchain based on Git, Doxygen and Simian.** 

ClonEvol must be able to start these applications from the command line. This 
can be achieved by:
1. Installing the applications and adding the locations to your systems PATH
   environment variable (Google "path environment variable").
2. [Portable] Extracting the applications into the same directory where the 
   ClonEvol binary is located.

Dependencies:
- Git:      You need a command-line Git client!  
            https://git-scm.com/downloads
- Doxygen:  ClonEvol is tested with Doxygen 1.9.1, newer _should_ be ok...  
            https://www.doxygen.nl/download.html
            Either use the installer or extract the binaries into ClonEvol dir.  
- Simian:   Simian comes with a non-free license for commercial use...  
            https://www.harukizaemon.com/simian/get_it_now.html  
            Finally, you will have to rename simian-X.X.XX.exe/jar to 
            simian.exe/jar  
            *Note that Simian  will also require java to run on linux.

<!-- CheckDependencies.bat will check whether the required applications can be found;
The path of each found application will be shown, otherwise an error is printed. -->

# RECOMMENDED System Requirements
- OS:       Windows, Linux (Mac not tested but should work)
- CPU:      Quad-core CPU
- RAM:      4GB+
- STORAGE:  SSD with 10GB+ free space*

*ClonEvol performs many disk I/O operations during the mining procedure, that
will take a very long time on an conventional HDD. Either run ClonEvol from an
SSD or make an RAMdisk - many solutions can be found on Google. The contents for
a repo Like FileZilla (~5200 revisions) used about 1.2GB of space.

# Using ClonEvol / FAQ
Usage of ClonEvol is (hopefully) straight-forward:
Choose a project name, set the URL, hit start. If the URL is OK, you will get an
overview of the files and changelogs in a few seconds up to a few minutes
(depending on the speed of the repository).
- All URL's supported by Git can be used, e.g. 
  https://github.com/basvodde/filezilla.git
   
Once you have the first overview, details can be acquired for revisions, by
selecting a revision range and hit 'Get revisions' under the sliders. The LOG
shows more details about the progress.

DO NOT MINE TEN THOUSANDS OF REVISIONS AT ONCE. This will take a lot of time,
and IF anything goes wrong, you will LOSE ALL PROGRESS. Better is to mine one
thousand revisions ten times, as these will be stored. Moreover, you have the
opportunity to view the partial data.

DO NOT MINE THE SAME PROJECT FROM MULTIPLE INSTANCES. There are no locks on
the files and you will probably just get rubbish data. If you still insist,
make sure that the scope extraction is not run at the same time from multiple
instances.

# BUG REPORTS
ClonEvol is not a final product. Expect crashes and other inconveniences. Still,
it is highly appreciated if you report the problems you encounter. You can open
a ticket, or simply send an e-mail to avdaga[AT]avdaga.net. Please report
- What project you were using (if possible, provide the database too)
- What you were trying to do (which button/slider/etc you used at that moment)
