﻿
CREATE TABLE DataStore
(
	dataStoreName VARCHAR(16383) NOT NULL,
	URL VARCHAR(16383) NOT NULL,
	CONSTRAINT DataStore_PK PRIMARY KEY(dataStoreName),
	CONSTRAINT DataStore_UC UNIQUE(URL)
);

CREATE TABLE Revision
(
	revisionNr INT NOT NULL,
	authorName VARCHAR(16383) NOT NULL,
	logMessage VARCHAR(16383) NOT NULL,
	rootFileName VARCHAR(16383) NOT NULL,
	rootFileRevisionNr INT NOT NULL,
	rootScopeName VARCHAR(16383) NOT NULL,
	rootScopeRevisionNr INT NOT NULL,
	isAcquired BIT(1),
	CONSTRAINT Revision_PK PRIMARY KEY(revisionNr),
	CONSTRAINT Revision_UC1 UNIQUE(rootFileRevisionNr, rootFileName),
	CONSTRAINT Revision_UC2 UNIQUE(rootScopeRevisionNr, rootScopeName)
);

CREATE TABLE FileNode
(
	revisionNr INT NOT NULL,
	fileName VARCHAR(16383) NOT NULL,
	parentRevisionNr INT,
	parentName VARCHAR(16383),
	FNType INT NOT NULL,
	operation CHAR(1) NOT NULL,
	CONSTRAINT FileNode_PK PRIMARY KEY(revisionNr, fileName)
);

CREATE TABLE ScopeNode
(
	revisionNr INT NOT NULL,
	scopeName VARCHAR(16383) NOT NULL,
	parentRevisionNr INT,
	parentName VARCHAR(16383),
	fileNodeRevisionNr INT NOT NULL,
	fileName VARCHAR(16383) NOT NULL,
	SNType INT NOT NULL,
	operation CHAR(1) NOT NULL,
	CONSTRAINT ScopeNode_PK PRIMARY KEY(revisionNr, scopeName)
);

CREATE TABLE CodeClone
(
	endLine INT NOT NULL,
	fileName VARCHAR(16383) NOT NULL,
	revisionNr INT NOT NULL,
	startLine INT NOT NULL,
	cloneSetId INT AUTO_INCREMENT NOT NULL,
	CONSTRAINT CodeClone_PK PRIMARY KEY(revisionNr, fileName, startLine, endLine)
);

CREATE TABLE ScopeClone
(
	targetRevisionNr INT NOT NULL,
	targetName VARCHAR(16383) NOT NULL,
	sourceRevisionNr INT NOT NULL,
	sourceName VARCHAR(16383) NOT NULL,
	driftType CHAR(1) NOT NULL,
	intraType CHAR(1) NOT NULL,
	matchLines INT NOT NULL,
	isIntra BIT(1),
	CONSTRAINT ScopeClone_PK PRIMARY KEY(sourceRevisionNr, sourceName, targetRevisionNr, targetName)
);

ALTER TABLE Revision ADD CONSTRAINT Revision_FK1 FOREIGN KEY (rootFileRevisionNr, rootFileName) REFERENCES FileNode (revisionNr, fileName) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE Revision ADD CONSTRAINT Revision_FK2 FOREIGN KEY (rootScopeRevisionNr, rootScopeName) REFERENCES ScopeNode (revisionNr, scopeName) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE FileNode ADD CONSTRAINT FileNode_FK1 FOREIGN KEY (revisionNr) REFERENCES Revision (revisionNr) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE FileNode ADD CONSTRAINT FileNode_FK2 FOREIGN KEY (parentRevisionNr, parentName) REFERENCES FileNode (revisionNr, fileName) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE ScopeNode ADD CONSTRAINT ScopeNode_FK1 FOREIGN KEY (revisionNr) REFERENCES Revision (revisionNr) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE ScopeNode ADD CONSTRAINT ScopeNode_FK2 FOREIGN KEY (parentRevisionNr, parentName) REFERENCES ScopeNode (revisionNr, scopeName) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE ScopeNode ADD CONSTRAINT ScopeNode_FK3 FOREIGN KEY (fileNodeRevisionNr, fileName) REFERENCES FileNode (revisionNr, fileName) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE CodeClone ADD CONSTRAINT CodeClone_FK FOREIGN KEY (revisionNr, fileName) REFERENCES FileNode (revisionNr, fileName) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE ScopeClone ADD CONSTRAINT ScopeClone_FK1 FOREIGN KEY (targetRevisionNr, targetName) REFERENCES ScopeNode (revisionNr, scopeName) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE ScopeClone ADD CONSTRAINT ScopeClone_FK2 FOREIGN KEY (sourceRevisionNr, sourceName) REFERENCES ScopeNode (revisionNr, scopeName) ON DELETE RESTRICT ON UPDATE RESTRICT;
