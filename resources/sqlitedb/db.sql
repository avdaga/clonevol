
CREATE TABLE DataStore
(
	dataStoreName VARCHAR(16383) NOT NULL,
	URL VARCHAR(16383) NOT NULL,
	CONSTRAINT DataStore_PK PRIMARY KEY(dataStoreName),
	CONSTRAINT DataStore_UC UNIQUE(URL)
);

CREATE TABLE Revision
(
	revisionNr INT NOT NULL,
	authorName VARCHAR(16383) NOT NULL,
	logMessage VARCHAR(16383) NOT NULL,
	rootFileName VARCHAR(16383) NOT NULL,
	rootScopeName VARCHAR(16383) NOT NULL,
	isAcquired BIT(1),
	CONSTRAINT Revision_PK PRIMARY KEY(revisionNr)
);

CREATE TABLE File
(
	revisionNr INT NOT NULL,
	fileName VARCHAR(16383) NOT NULL,
	parentName VARCHAR(16383),
	FNType INT NOT NULL,
	operation CHAR(1) NOT NULL,
	CONSTRAINT File_PK PRIMARY KEY(revisionNr, fileName)
);

CREATE TABLE `Scope`
(
	revisionNr INT NOT NULL,
	scopeName VARCHAR(16383) NOT NULL,
	parentName VARCHAR(16383),
	fileName VARCHAR(16383) NOT NULL,
	SNType INT NOT NULL,
	operation CHAR(1) NOT NULL,
	CONSTRAINT Scope_PK PRIMARY KEY(revisionNr, scopeName, fileName)
);

CREATE TABLE CodeClone
(
	endLine INT NOT NULL,
	fileName VARCHAR(16383) NOT NULL,
	revisionNr INT NOT NULL,
	startLine INT NOT NULL,
	cloneSetId INT AUTO_INCREMENT NOT NULL,
	CONSTRAINT CodeClone_PK PRIMARY KEY(revisionNr, fileName, startLine, endLine)
);

CREATE TABLE ScopeClone
(
	targetRevisionNr INT NOT NULL,
	targetName VARCHAR(16383) NOT NULL,
	sourceRevisionNr INT NOT NULL,
	sourceName VARCHAR(16383) NOT NULL,
	driftType CHAR(1) NOT NULL,
	intraType CHAR(1) NOT NULL,
	matchLines INT NOT NULL,
	isIntra BIT(1),
	CONSTRAINT ScopeClone_PK PRIMARY KEY(sourceRevisionNr, sourceName, targetRevisionNr, targetName, driftType, intraType, isIntra)
);

ALTER TABLE Revision ADD CONSTRAINT Revision_FK1 FOREIGN KEY (dataStoreName) REFERENCES DataStore (dataStoreName) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE Revision ADD CONSTRAINT Revision_FK2 FOREIGN KEY (rootFileRevisionNr, rootFileName) REFERENCES File (revisionNr, fileName) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE Revision ADD CONSTRAINT Revision_FK3 FOREIGN KEY (rootScopeRevisionNr, rootScopeName) REFERENCES `Scope` (revisionNr, scopeName) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE File ADD CONSTRAINT File_FK1 FOREIGN KEY (revisionNr) REFERENCES Revision (revisionNr) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE File ADD CONSTRAINT File_FK2 FOREIGN KEY (parentRevisionNr, parentName) REFERENCES File (revisionNr, fileName) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE `Scope` ADD CONSTRAINT Scope_FK1 FOREIGN KEY (revisionNr) REFERENCES Revision (revisionNr) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE `Scope` ADD CONSTRAINT Scope_FK2 FOREIGN KEY (parentRevisionNr, parentName) REFERENCES `Scope` (revisionNr, scopeName) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE `Scope` ADD CONSTRAINT Scope_FK3 FOREIGN KEY (fileRevisionNr, fileName) REFERENCES File (revisionNr, fileName) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE CodeClone ADD CONSTRAINT CodeClone_FK FOREIGN KEY (revisionNr, fileName) REFERENCES File (revisionNr, fileName) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE ScopeClone ADD CONSTRAINT ScopeClone_FK1 FOREIGN KEY (targetRevisionNe, targetName) REFERENCES `Scope` (revisionNr, scopeName) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE ScopeClone ADD CONSTRAINT ScopeClone_FK2 FOREIGN KEY (sourceRevisionNr, sourceName) REFERENCES `Scope` (revisionNr, scopeName) ON DELETE RESTRICT ON UPDATE RESTRICT;
