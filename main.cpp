#include <QApplication>
#include <QDebug>
#include <QSurfaceFormat>

#include "gui/mainwindow.h"
#include "gui/logbrowser.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setApplicationName("ClonEvol");
    a.setApplicationVersion("v21.01");

    // Make sure antialiasing works with OpenGL (by using multisampling)
    QSurfaceFormat fmt;
    fmt.setSamples(4);
    QSurfaceFormat::setDefaultFormat(fmt);

    // Initialize log browser
    LogBrowser::Instance();

    // Start main application
    MainWindow::Instance()->show();

    return a.exec();
}
